﻿namespace WindowsFormsApplication2
{
    partial class ExpenseTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlExpenseTransaction = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkDatePicker = new System.Windows.Forms.DateTimePicker();
            this.cmbParticular = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCheckAmount = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtCheckNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCash = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtORNo = new System.Windows.Forms.TextBox();
            this.ORNo = new System.Windows.Forms.Label();
            this.txtExpenseDetails = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlExpenseTransaction.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExpenseTransaction
            // 
            this.pnlExpenseTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.pnlExpenseTransaction.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.pnlExpenseTransaction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlExpenseTransaction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlExpenseTransaction.Controls.Add(this.label1);
            this.pnlExpenseTransaction.Controls.Add(this.checkDatePicker);
            this.pnlExpenseTransaction.Controls.Add(this.cmbParticular);
            this.pnlExpenseTransaction.Controls.Add(this.btnCancel);
            this.pnlExpenseTransaction.Controls.Add(this.txtDate);
            this.pnlExpenseTransaction.Controls.Add(this.label28);
            this.pnlExpenseTransaction.Controls.Add(this.txtCheckAmount);
            this.pnlExpenseTransaction.Controls.Add(this.label29);
            this.pnlExpenseTransaction.Controls.Add(this.label23);
            this.pnlExpenseTransaction.Controls.Add(this.txtTotalAmount);
            this.pnlExpenseTransaction.Controls.Add(this.label26);
            this.pnlExpenseTransaction.Controls.Add(this.btnAdd);
            this.pnlExpenseTransaction.Controls.Add(this.txtCheckNo);
            this.pnlExpenseTransaction.Controls.Add(this.label25);
            this.pnlExpenseTransaction.Controls.Add(this.txtCash);
            this.pnlExpenseTransaction.Controls.Add(this.label24);
            this.pnlExpenseTransaction.Controls.Add(this.txtORNo);
            this.pnlExpenseTransaction.Controls.Add(this.ORNo);
            this.pnlExpenseTransaction.Controls.Add(this.txtExpenseDetails);
            this.pnlExpenseTransaction.Controls.Add(this.label16);
            this.pnlExpenseTransaction.Location = new System.Drawing.Point(8, 14);
            this.pnlExpenseTransaction.Name = "pnlExpenseTransaction";
            this.pnlExpenseTransaction.Size = new System.Drawing.Size(482, 320);
            this.pnlExpenseTransaction.TabIndex = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(120, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 168;
            this.label1.Text = "Check Date";
            // 
            // checkDatePicker
            // 
            this.checkDatePicker.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.checkDatePicker.CustomFormat = "dd/MM/yyyy";
            this.checkDatePicker.Enabled = false;
            this.checkDatePicker.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F);
            this.checkDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.checkDatePicker.Location = new System.Drawing.Point(213, 183);
            this.checkDatePicker.Name = "checkDatePicker";
            this.checkDatePicker.Size = new System.Drawing.Size(151, 22);
            this.checkDatePicker.TabIndex = 6;
            // 
            // cmbParticular
            // 
            this.cmbParticular.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParticular.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F);
            this.cmbParticular.FormattingEnabled = true;
            this.cmbParticular.ItemHeight = 16;
            this.cmbParticular.Location = new System.Drawing.Point(213, 41);
            this.cmbParticular.Name = "cmbParticular";
            this.cmbParticular.Size = new System.Drawing.Size(151, 24);
            this.cmbParticular.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(399, 283);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtDate
            // 
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(48, 12);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(86, 22);
            this.txtDate.TabIndex = 164;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(5, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(37, 16);
            this.label28.TabIndex = 165;
            this.label28.Text = "Date";
            // 
            // txtCheckAmount
            // 
            this.txtCheckAmount.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckAmount.Location = new System.Drawing.Point(213, 155);
            this.txtCheckAmount.Name = "txtCheckAmount";
            this.txtCheckAmount.Size = new System.Drawing.Size(151, 22);
            this.txtCheckAmount.TabIndex = 5;
            this.txtCheckAmount.TextChanged += new System.EventHandler(this.txtCheckAmount_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(102, 162);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(98, 16);
            this.label29.TabIndex = 163;
            this.label29.Text = "Check Amount";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(129, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 16);
            this.label23.TabIndex = 161;
            this.label23.Text = "Particular";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(213, 241);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(151, 22);
            this.txtTotalAmount.TabIndex = 8;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(108, 244);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(92, 16);
            this.label26.TabIndex = 159;
            this.label26.Text = "Total Amount";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(318, 283);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtCheckNo
            // 
            this.txtCheckNo.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckNo.Location = new System.Drawing.Point(213, 127);
            this.txtCheckNo.Name = "txtCheckNo";
            this.txtCheckNo.Size = new System.Drawing.Size(151, 22);
            this.txtCheckNo.TabIndex = 4;
            this.txtCheckNo.TextChanged += new System.EventHandler(this.txtCheckNo_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(131, 130);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 16);
            this.label25.TabIndex = 156;
            this.label25.Text = "Check No.";
            // 
            // txtCash
            // 
            this.txtCash.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCash.Location = new System.Drawing.Point(213, 211);
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(151, 22);
            this.txtCash.TabIndex = 7;
            this.txtCash.TextChanged += new System.EventHandler(this.txtCash_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(159, 214);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 16);
            this.label24.TabIndex = 154;
            this.label24.Text = "Cash";
            // 
            // txtORNo
            // 
            this.txtORNo.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtORNo.Location = new System.Drawing.Point(213, 99);
            this.txtORNo.Name = "txtORNo";
            this.txtORNo.Size = new System.Drawing.Size(151, 22);
            this.txtORNo.TabIndex = 3;
            // 
            // ORNo
            // 
            this.ORNo.AutoSize = true;
            this.ORNo.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ORNo.Location = new System.Drawing.Point(148, 102);
            this.ORNo.Name = "ORNo";
            this.ORNo.Size = new System.Drawing.Size(51, 16);
            this.ORNo.TabIndex = 152;
            this.ORNo.Text = "OR No.";
            // 
            // txtExpenseDetails
            // 
            this.txtExpenseDetails.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpenseDetails.Location = new System.Drawing.Point(213, 71);
            this.txtExpenseDetails.Name = "txtExpenseDetails";
            this.txtExpenseDetails.Size = new System.Drawing.Size(151, 22);
            this.txtExpenseDetails.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(96, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(103, 16);
            this.label16.TabIndex = 150;
            this.label16.Text = "Expense Details";
            // 
            // ExpenseTransaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 337);
            this.ControlBox = false;
            this.Controls.Add(this.pnlExpenseTransaction);
            this.Name = "ExpenseTransaction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExpenseTransaction";
            this.Load += new System.EventHandler(this.ExpenseTransaction_Load);
            this.pnlExpenseTransaction.ResumeLayout(false);
            this.pnlExpenseTransaction.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlExpenseTransaction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCheckAmount;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtCheckNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCash;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtORNo;
        private System.Windows.Forms.Label ORNo;
        private System.Windows.Forms.TextBox txtExpenseDetails;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbParticular;
        private System.Windows.Forms.DateTimePicker checkDatePicker;
        private System.Windows.Forms.Label label1;
    }
}