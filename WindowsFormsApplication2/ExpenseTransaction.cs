﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class ExpenseTransaction : Form
    {
        public ExpenseTransaction()
        {
            InitializeComponent();
            DateTime today = DateTime.Today;
            txtDate.ReadOnly = true;
            txtDate.Text = today.ToString("MM/dd/yyy");
        }
        //ADDS NEW EXPENSE
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtExpenseDetails.Text != "" || txtORNo.Text != "" || cmbParticular.SelectedIndex == -1)
            {
                if (txtTotalAmount.Text != "" || txtTotalAmount.Text != "0")
                {
                    if (txtCheckAmount.Text == "")
                    {
                        txtCheckAmount.Text = "0";
                    }
                    if (txtCheckNo.Text == "")
                    {
                        txtCheckNo.Text = "NA";
                    }
                    if (txtCash.Text == "")
                    {
                        txtCash.Text = "0";
                    }

                    if (txtCheckAmount.Text != "0" && txtCheckNo.Text != "NA")
                        Global.Add_Record(txtDate.Text, cmbParticular.Text, txtExpenseDetails.Text, txtORNo.Text, txtCheckNo.Text, double.Parse(txtCheckAmount.Text), checkDatePicker.Text, double.Parse(txtCash.Text), double.Parse(txtTotalAmount.Text));
                    else
                        Global.Add_Record(txtDate.Text, cmbParticular.Text, txtExpenseDetails.Text, txtORNo.Text, txtCheckNo.Text, double.Parse(txtCheckAmount.Text), "NA", double.Parse(txtCash.Text), double.Parse(txtTotalAmount.Text));
                    MessageBox.Show("Expense was successfully added");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please provide the amount");
                    txtCash.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please check fields");
                txtORNo.Focus();
            }
        }
        //CLOSES THE FORM
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //WHEN THE CASH FIELD HAS CHANGES, IT AUTOMATICALLY COMPUTES
        private void txtCash_TextChanged(object sender, EventArgs e)
        {
            double temp = 0, temp1 = 0, temp2 = 0;
            if (!double.TryParse(txtCash.Text, out temp1))
            {
                if (txtCash.Text != "")
                {
                    MessageBox.Show("This is a number only field");
                    txtCash.Clear();
                }
                else
                {
                    if (txtCheckAmount.Text != "")
                        txtTotalAmount.Text = txtCheckAmount.Text;
                    else
                        txtTotalAmount.Text = "0";

                }
                return;
            }
            else
            {
                if (txtCash.Text == "")
                    temp1 = 0;
                else
                    temp1 = double.Parse(txtCash.Text);

                if (txtCheckAmount.Text == "")
                    temp2 = 0;
                else
                    temp2 = double.Parse(txtCheckAmount.Text);

                temp = temp1 + temp2;

                txtTotalAmount.Text = temp.ToString();
            }
        }
        //WHEN THE CHECK AMOUNT FIELD HAS CHANGES, IT AUTOMATICALLY COMPUTES
        private void txtCheckAmount_TextChanged(object sender, EventArgs e)
        {
            if (txtCheckAmount.Text != "" && txtCheckNo.Text != "")
                checkDatePicker.Enabled = true;
            else
                checkDatePicker.Enabled = false;

            double temp = 0, temp1 = 0, temp2 = 0;
            if (!double.TryParse(txtCheckAmount.Text, out temp1))
            {
                if (txtCheckAmount.Text != "")
                {
                    MessageBox.Show("This is a number only field");
                    txtCheckAmount.Clear();
                }
                else
                {
                    if (txtCash.Text != "")
                        txtTotalAmount.Text = txtCash.Text;
                    else
                        txtTotalAmount.Text = "0";
                }
                return;
            }
            else
            {
                if (txtCash.Text == "")
                    temp1 = 0;
                else
                    temp1 = double.Parse(txtCash.Text);

                if (txtCheckAmount.Text == "")
                    temp2 = 0;
                else
                    temp2 = double.Parse(txtCheckAmount.Text);

                temp = temp1 + temp2;

                txtTotalAmount.Text = temp.ToString();
            }
        }
        //ON LOAD, POPULATES THE COMBOBOX
        private void ExpenseTransaction_Load(object sender, EventArgs e)
        {
            cmbParticular.Items.Clear();
            DataTable dt = Global.Query2("*", "ExpenseServices");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                cmbParticular.Items.Add(dr["Name"].ToString());
            }
        }
        //ENABLES/DISABLES THE CHECK DATE PICKER
        private void txtCheckNo_TextChanged(object sender, EventArgs e)
        {
            if (txtCheckAmount.Text != "" && txtCheckNo.Text != "")
                checkDatePicker.Enabled = true;
            else
                checkDatePicker.Enabled = false;
        }
    }
}
