﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class AccountSettings : Form
    {
        public AccountSettings()
        {
            InitializeComponent();
        }

        //LOADS THE CURRENT LOGGED IN USER ACCOUNT'S DETAILS
        private void AccountSettings_Load(object sender, EventArgs e)
        {
            DataTable dt = Global.AccountSettings("*", "UserAccounts",Global.username);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                txtSecurityQuestion.Text = dr["securityQuestion"].ToString();
                txtAnswer.Text = dr["securityAnswer"].ToString();
                txtPassword.Text = dr["password"].ToString();
            }
        }
        int show = 0;
        //SHOWS/HIDES THE REAL CHARACTERS IN THE PASSWORD TEXTBOX
        private void lblShow_Click(object sender, EventArgs e)
        {
            if (show == 0)
            {
                txtPassword.PasswordChar = '\0';
                lblShow.Text = "Hide";
                show = 1;
            }
            else
            {
                txtPassword.PasswordChar = '*';
                lblShow.Text = "Show";
                show = 0;
            }
        }
        //SAVES THE CHANGES IN THE DETAILS OF THE ACCOUNT
        private void btnSave_Click(object sender, EventArgs e)
        {
            if(txtSecurityQuestion.Text!="" && txtAnswer.Text!=""&& txtPassword.Text!="")
            {
                if (txtPassword.TextLength > 8)
                {
                    Global.Update_Useraccounts(txtSecurityQuestion.Text, txtAnswer.Text, txtPassword.Text);
                    MessageBox.Show("Updated successfuly");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Password must be atleast 8 characters");
                    txtPassword.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please check fields");
                if(txtSecurityQuestion.Text=="")
                {
                    txtSecurityQuestion.Focus();
                }
                if (txtAnswer.Text == "")
                {
                    txtAnswer.Focus();
                }
                if (txtPassword.Text == "")
                {
                    txtPassword.Focus();
                }
            }
            
        }
        //CLOSES THE FORM
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //VALIDATION FOR THE PASSWORD TEXTBOX
        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtPassword.TextLength < 8)
            {
                txtPassword.BackColor = Color.FromArgb(247, 122, 111);
            }
            else
            {
                txtPassword.BackColor = SystemColors.Control;
            }
        }



        //SHOWS THE CREATE ACCOUNT FORM
        private void lblCreateAccount_Click(object sender, EventArgs e)
        {
            AddNewAccount ac = new AddNewAccount();
            ac.ShowDialog();
        }
    }
}
