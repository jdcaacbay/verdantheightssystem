﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class CheckDetails : Form
    {
        public CheckDetails()
        {
            InitializeComponent();
        }
        //CHECK DETAILS ON LOAD. LOADS THE CHECK DETAILS OF THE SELECTED INCOME TRANSACTION
        private void CheckDetails_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(Global.connectionString);
            SqlCommand command = new SqlCommand("select * from IncomeLogs WHERE ORnum='" + Global.OrNo + "'", con);
            con.Open();
            SqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                txtCash.Text = (read["Cash"].ToString());
                txtCheckNo.Text = (read["Checknum"].ToString());
                txtCheckAmount.Text = (read["CheckAmount"].ToString());
              
            }
            read.Close();
            con.Close();
        
        }
        //CLOSES THE FORM
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
