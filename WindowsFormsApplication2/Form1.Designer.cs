﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Units = new System.Windows.Forms.TabPage();
            this.panel2 = new WindowsFormsApplication2.Form1.MyPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.unitInfoListView = new System.Windows.Forms.ListView();
            this.UnitID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Blk = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Lot = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OwnerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UnitType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TurnOver = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Rented = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RenterName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MonthlyDue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtSearchUnits = new System.Windows.Forms.TextBox();
            this.controlsPnl = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnAccountSettings = new System.Windows.Forms.Button();
            this.panel3 = new WindowsFormsApplication2.Form1.MyPanel();
            this.btnTransact = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.UnitInfo = new System.Windows.Forms.TabPage();
            this.unitInfoBgPanel = new System.Windows.Forms.Panel();
            this.panel4 = new WindowsFormsApplication2.Form1.MyPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.txtUnitID = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.cmbRented = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRenterName = new System.Windows.Forms.TextBox();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBlk = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbUnitType = new System.Windows.Forms.ComboBox();
            this.cmbTurnOver = new System.Windows.Forms.ComboBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblUnitID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOwnerName = new System.Windows.Forms.TextBox();
            this.lblCivil = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhoneNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.panel5 = new WindowsFormsApplication2.Form1.MyPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioParkingFee = new System.Windows.Forms.RadioButton();
            this.radioMonthlyDue = new System.Windows.Forms.RadioButton();
            this.MonthlyDueListView = new System.Windows.Forms.ListView();
            this.UnitID_MonthlyDue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Year = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.January = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.February = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.March = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.April = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.May = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.June = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.July = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.August = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.September = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.October = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.November = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.December = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnCancelUnitInfo = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.IncomeTransaction = new System.Windows.Forms.TabPage();
            this.incTransBgPanel = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lblUnitIDTransaction = new System.Windows.Forms.Label();
            this.txtUnitIDIncome = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtORNoIncome = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbParticularIncome = new System.Windows.Forms.ComboBox();
            this.pnlOthers = new System.Windows.Forms.Panel();
            this.txtSpecify = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pnlTotals = new System.Windows.Forms.Panel();
            this.checkDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtTotalAmountIncome = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtChangeIncome = new System.Windows.Forms.TextBox();
            this.lblAmountToPay = new System.Windows.Forms.Label();
            this.txtAmountToPay = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtCheckNoIncome = new System.Windows.Forms.TextBox();
            this.txtCashIncome = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCheckAmountIncome = new System.Windows.Forms.TextBox();
            this.pnlPerHead = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.txtNoOfGuests = new System.Windows.Forms.TextBox();
            this.txtNoOfResidents = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlReservation = new System.Windows.Forms.Panel();
            this.cmbReservation = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pnlHOA = new System.Windows.Forms.Panel();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPenalty = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlQuantity = new System.Windows.Forms.Panel();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pnlDates = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.todatePicker = new System.Windows.Forms.DateTimePicker();
            this.fromdatePicker = new System.Windows.Forms.DateTimePicker();
            this.panel8 = new WindowsFormsApplication2.Form1.MyPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.radioUnitTransaction = new System.Windows.Forms.RadioButton();
            this.radioAllTransaction = new System.Windows.Forms.RadioButton();
            this.IncomeTransactionsListView = new System.Windows.Forms.ListView();
            this.panel13 = new WindowsFormsApplication2.Form1.MyPanel();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel14 = new WindowsFormsApplication2.Form1.MyPanel();
            this.btnCancelIncome = new System.Windows.Forms.Button();
            this.ExpenseTransaction = new System.Windows.Forms.TabPage();
            this.panel18 = new WindowsFormsApplication2.Form1.MyPanel();
            this.radioIncome = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSearchAllTransactions = new System.Windows.Forms.TextBox();
            this.AllTransactionsListView = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel15 = new WindowsFormsApplication2.Form1.MyPanel();
            this.btnNewExpense = new System.Windows.Forms.Button();
            this.btnCancelTransaction = new System.Windows.Forms.Button();
            this.panel16 = new WindowsFormsApplication2.Form1.MyPanel();
            this.IncomeServices = new System.Windows.Forms.TabPage();
            this.pnlIncomeItems = new WindowsFormsApplication2.Form1.MyPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.txtSearchServices = new System.Windows.Forms.TextBox();
            this.radioExpenseServices = new System.Windows.Forms.RadioButton();
            this.ServicesListView = new System.Windows.Forms.ListView();
            this.radioIncomeServices = new System.Windows.Forms.RadioButton();
            this.panel19 = new WindowsFormsApplication2.Form1.MyPanel();
            this.btnDelete_Item = new System.Windows.Forms.Button();
            this.btnEdit_Item = new System.Windows.Forms.Button();
            this.btnAdd_Item = new System.Windows.Forms.Button();
            this.panel20 = new WindowsFormsApplication2.Form1.MyPanel();
            this.Reports = new System.Windows.Forms.TabPage();
            this.panel25 = new WindowsFormsApplication2.Form1.MyPanel();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel26 = new WindowsFormsApplication2.Form1.MyPanel();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.labelUnitType = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.txtFrom = new System.Windows.Forms.Label();
            this.txtTo = new System.Windows.Forms.Label();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.panel22 = new WindowsFormsApplication2.Form1.MyPanel();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnExpenseReport = new System.Windows.Forms.Button();
            this.btnMonthlyDuesReport = new System.Windows.Forms.Button();
            this.panel23 = new WindowsFormsApplication2.Form1.MyPanel();
            this.btnIncomeReport = new System.Windows.Forms.Button();
            this.btnUnitReport = new System.Windows.Forms.Button();
            this.panel24 = new WindowsFormsApplication2.Form1.MyPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblDateandTime = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel10 = new WindowsFormsApplication2.Form1.MyPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnReports = new System.Windows.Forms.Button();
            this.btnIncomeServices = new System.Windows.Forms.Button();
            this.btnTransactions = new System.Windows.Forms.Button();
            this.btnIncome = new System.Windows.Forms.Button();
            this.btnUnitInfo = new System.Windows.Forms.Button();
            this.btnUnits = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.Units.SuspendLayout();
            this.panel2.SuspendLayout();
            this.controlsPnl.SuspendLayout();
            this.UnitInfo.SuspendLayout();
            this.unitInfoBgPanel.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.IncomeTransaction.SuspendLayout();
            this.incTransBgPanel.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel21.SuspendLayout();
            this.pnlOthers.SuspendLayout();
            this.pnlTotals.SuspendLayout();
            this.pnlPerHead.SuspendLayout();
            this.pnlReservation.SuspendLayout();
            this.pnlHOA.SuspendLayout();
            this.pnlQuantity.SuspendLayout();
            this.pnlDates.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.ExpenseTransaction.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel15.SuspendLayout();
            this.IncomeServices.SuspendLayout();
            this.pnlIncomeItems.SuspendLayout();
            this.panel19.SuspendLayout();
            this.Reports.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.Units);
            this.tabControl1.Controls.Add(this.UnitInfo);
            this.tabControl1.Controls.Add(this.IncomeTransaction);
            this.tabControl1.Controls.Add(this.ExpenseTransaction);
            this.tabControl1.Controls.Add(this.IncomeServices);
            this.tabControl1.Controls.Add(this.Reports);
            this.tabControl1.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1060, 485);
            this.tabControl1.TabIndex = 0;
            // 
            // Units
            // 
            this.Units.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.Units.Controls.Add(this.panel2);
            this.Units.Controls.Add(this.controlsPnl);
            this.Units.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Units.Location = new System.Drawing.Point(4, 27);
            this.Units.Name = "Units";
            this.Units.Padding = new System.Windows.Forms.Padding(3);
            this.Units.Size = new System.Drawing.Size(1052, 454);
            this.Units.TabIndex = 1;
            this.Units.Text = "Units";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.panel2.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.unitInfoListView);
            this.panel2.Controls.Add(this.txtSearchUnits);
            this.panel2.Location = new System.Drawing.Point(150, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(900, 457);
            this.panel2.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(250, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 18);
            this.label13.TabIndex = 167;
            this.label13.Text = "Search";
            // 
            // unitInfoListView
            // 
            this.unitInfoListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unitInfoListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UnitID,
            this.Blk,
            this.Lot,
            this.OwnerName,
            this.UnitType,
            this.TurnOver,
            this.Rented,
            this.RenterName,
            this.MonthlyDue});
            this.unitInfoListView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.unitInfoListView.FullRowSelect = true;
            this.unitInfoListView.GridLines = true;
            this.unitInfoListView.Location = new System.Drawing.Point(38, 62);
            this.unitInfoListView.MultiSelect = false;
            this.unitInfoListView.Name = "unitInfoListView";
            this.unitInfoListView.Size = new System.Drawing.Size(834, 377);
            this.unitInfoListView.TabIndex = 5;
            this.unitInfoListView.UseCompatibleStateImageBehavior = false;
            this.unitInfoListView.View = System.Windows.Forms.View.Details;
            this.unitInfoListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.unitInfoListView_ColumnClick);
            // 
            // UnitID
            // 
            this.UnitID.Tag = "";
            this.UnitID.Text = "UnitID";
            this.UnitID.Width = 87;
            // 
            // Blk
            // 
            this.Blk.Text = "Blk";
            // 
            // Lot
            // 
            this.Lot.Tag = "";
            this.Lot.Text = "Lot";
            // 
            // OwnerName
            // 
            this.OwnerName.Text = "Owner\'s Name";
            // 
            // UnitType
            // 
            this.UnitType.Text = "Unit Type";
            // 
            // TurnOver
            // 
            this.TurnOver.Tag = "";
            this.TurnOver.Text = "TurnOver";
            // 
            // Rented
            // 
            this.Rented.Text = "Rented";
            this.Rented.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RenterName
            // 
            this.RenterName.Text = "Renter\'s Name";
            // 
            // MonthlyDue
            // 
            this.MonthlyDue.Text = "Monthly Due";
            this.MonthlyDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSearchUnits
            // 
            this.txtSearchUnits.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchUnits.Location = new System.Drawing.Point(38, 34);
            this.txtSearchUnits.Name = "txtSearchUnits";
            this.txtSearchUnits.Size = new System.Drawing.Size(206, 25);
            this.txtSearchUnits.TabIndex = 3;
            this.txtSearchUnits.TextChanged += new System.EventHandler(this.txtSearchUnits_TextChanged);
            // 
            // controlsPnl
            // 
            this.controlsPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlsPnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.controlsPnl.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.controlsPnl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.controlsPnl.Controls.Add(this.btnLogout);
            this.controlsPnl.Controls.Add(this.btnAccountSettings);
            this.controlsPnl.Controls.Add(this.panel3);
            this.controlsPnl.Controls.Add(this.btnTransact);
            this.controlsPnl.Controls.Add(this.btnView);
            this.controlsPnl.Controls.Add(this.btnDelete);
            this.controlsPnl.Controls.Add(this.btnEdit);
            this.controlsPnl.Controls.Add(this.btnAdd);
            this.controlsPnl.Location = new System.Drawing.Point(0, 0);
            this.controlsPnl.Name = "controlsPnl";
            this.controlsPnl.Size = new System.Drawing.Size(150, 455);
            this.controlsPnl.TabIndex = 6;
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Image = global::WindowsFormsApplication2.Properties.Resources.logout;
            this.btnLogout.Location = new System.Drawing.Point(-7, 390);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(157, 41);
            this.btnLogout.TabIndex = 16;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click_1);
            // 
            // btnAccountSettings
            // 
            this.btnAccountSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAccountSettings.BackColor = System.Drawing.Color.Transparent;
            this.btnAccountSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccountSettings.FlatAppearance.BorderSize = 0;
            this.btnAccountSettings.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnAccountSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnAccountSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccountSettings.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccountSettings.ForeColor = System.Drawing.Color.White;
            this.btnAccountSettings.Image = global::WindowsFormsApplication2.Properties.Resources.accSettings;
            this.btnAccountSettings.Location = new System.Drawing.Point(-7, 337);
            this.btnAccountSettings.Name = "btnAccountSettings";
            this.btnAccountSettings.Size = new System.Drawing.Size(157, 47);
            this.btnAccountSettings.TabIndex = 15;
            this.btnAccountSettings.Text = "Account Settings";
            this.btnAccountSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAccountSettings.UseVisualStyleBackColor = false;
            this.btnAccountSettings.Click += new System.EventHandler(this.btnAccountSettings_Click);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(158, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(783, 81);
            this.panel3.TabIndex = 10;
            // 
            // btnTransact
            // 
            this.btnTransact.BackColor = System.Drawing.Color.Transparent;
            this.btnTransact.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTransact.FlatAppearance.BorderSize = 0;
            this.btnTransact.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnTransact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransact.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransact.ForeColor = System.Drawing.Color.White;
            this.btnTransact.Image = global::WindowsFormsApplication2.Properties.Resources.Transact1;
            this.btnTransact.Location = new System.Drawing.Point(-3, 248);
            this.btnTransact.Name = "btnTransact";
            this.btnTransact.Size = new System.Drawing.Size(157, 41);
            this.btnTransact.TabIndex = 14;
            this.btnTransact.Text = "TRANSACT";
            this.btnTransact.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTransact.UseVisualStyleBackColor = false;
            this.btnTransact.Click += new System.EventHandler(this.btnTransact_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.Transparent;
            this.btnView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnView.FlatAppearance.BorderSize = 0;
            this.btnView.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.White;
            this.btnView.Image = global::WindowsFormsApplication2.Properties.Resources.View1;
            this.btnView.Location = new System.Drawing.Point(-3, 201);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(157, 41);
            this.btnView.TabIndex = 13;
            this.btnView.Text = "VIEW";
            this.btnView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Image = global::WindowsFormsApplication2.Properties.Resources.Delete1;
            this.btnDelete.Location = new System.Drawing.Point(-3, 154);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(157, 41);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.Transparent;
            this.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEdit.FlatAppearance.BorderSize = 0;
            this.btnEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.White;
            this.btnEdit.Image = global::WindowsFormsApplication2.Properties.Resources.Edit1;
            this.btnEdit.Location = new System.Drawing.Point(-3, 107);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(157, 41);
            this.btnEdit.TabIndex = 11;
            this.btnEdit.Text = "EDIT";
            this.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Image = global::WindowsFormsApplication2.Properties.Resources.Add_New_32;
            this.btnAdd.Location = new System.Drawing.Point(-3, 60);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(157, 41);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "ADD";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // UnitInfo
            // 
            this.UnitInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.UnitInfo.Controls.Add(this.unitInfoBgPanel);
            this.UnitInfo.Controls.Add(this.panel11);
            this.UnitInfo.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnitInfo.Location = new System.Drawing.Point(4, 27);
            this.UnitInfo.Name = "UnitInfo";
            this.UnitInfo.Size = new System.Drawing.Size(1052, 454);
            this.UnitInfo.TabIndex = 2;
            this.UnitInfo.Text = "Unit Information";
            // 
            // unitInfoBgPanel
            // 
            this.unitInfoBgPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unitInfoBgPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.unitInfoBgPanel.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.unitInfoBgPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.unitInfoBgPanel.Controls.Add(this.panel4);
            this.unitInfoBgPanel.Controls.Add(this.panel5);
            this.unitInfoBgPanel.Location = new System.Drawing.Point(150, 0);
            this.unitInfoBgPanel.Name = "unitInfoBgPanel";
            this.unitInfoBgPanel.Size = new System.Drawing.Size(900, 457);
            this.unitInfoBgPanel.TabIndex = 151;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.txtUnitID);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.txtMobileNo);
            this.panel4.Controls.Add(this.cmbRented);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.txtRenterName);
            this.panel4.Controls.Add(this.txtLot);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txtBlk);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.cmbUnitType);
            this.panel4.Controls.Add(this.cmbTurnOver);
            this.panel4.Controls.Add(this.lblGender);
            this.panel4.Controls.Add(this.lblUnitID);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.txtOwnerName);
            this.panel4.Controls.Add(this.lblCivil);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.txtPhoneNo);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.txtEmail);
            this.panel4.Location = new System.Drawing.Point(3, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(904, 199);
            this.panel4.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Eras Demi ITC", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(23, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 18);
            this.label12.TabIndex = 161;
            this.label12.Text = "Unit Information";
            // 
            // txtUnitID
            // 
            this.txtUnitID.AutoSize = true;
            this.txtUnitID.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnitID.Location = new System.Drawing.Point(85, 43);
            this.txtUnitID.Name = "txtUnitID";
            this.txtUnitID.Size = new System.Drawing.Size(51, 18);
            this.txtUnitID.TabIndex = 171;
            this.txtUnitID.Text = "UnitID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(271, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 18);
            this.label5.TabIndex = 170;
            this.label5.Text = "Mobile Number";
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Enabled = false;
            this.txtMobileNo.Location = new System.Drawing.Point(393, 111);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(103, 22);
            this.txtMobileNo.TabIndex = 7;
            // 
            // cmbRented
            // 
            this.cmbRented.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRented.FormattingEnabled = true;
            this.cmbRented.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbRented.Location = new System.Drawing.Point(364, 156);
            this.cmbRented.Name = "cmbRented";
            this.cmbRented.Size = new System.Drawing.Size(100, 24);
            this.cmbRented.TabIndex = 10;
            this.cmbRented.Tag = "0";
            this.cmbRented.SelectedIndexChanged += new System.EventHandler(this.cmbRented_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(300, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 169;
            this.label3.Text = "Rented";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(470, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 18);
            this.label2.TabIndex = 168;
            this.label2.Text = "Renter\'s Name";
            // 
            // txtRenterName
            // 
            this.txtRenterName.Enabled = false;
            this.txtRenterName.Location = new System.Drawing.Point(587, 156);
            this.txtRenterName.Name = "txtRenterName";
            this.txtRenterName.Size = new System.Drawing.Size(133, 22);
            this.txtRenterName.TabIndex = 11;
            // 
            // txtLot
            // 
            this.txtLot.Enabled = false;
            this.txtLot.Location = new System.Drawing.Point(145, 71);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(43, 22);
            this.txtLot.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(108, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 18);
            this.label10.TabIndex = 167;
            this.label10.Text = "Lot";
            // 
            // txtBlk
            // 
            this.txtBlk.Enabled = false;
            this.txtBlk.Location = new System.Drawing.Point(59, 72);
            this.txtBlk.Name = "txtBlk";
            this.txtBlk.Size = new System.Drawing.Size(43, 22);
            this.txtBlk.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 75);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 18);
            this.label14.TabIndex = 166;
            this.label14.Text = "Blk";
            // 
            // cmbUnitType
            // 
            this.cmbUnitType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitType.FormattingEnabled = true;
            this.cmbUnitType.Items.AddRange(new object[] {
            "SU",
            "TH"});
            this.cmbUnitType.Location = new System.Drawing.Point(278, 68);
            this.cmbUnitType.Name = "cmbUnitType";
            this.cmbUnitType.Size = new System.Drawing.Size(100, 24);
            this.cmbUnitType.TabIndex = 3;
            this.cmbUnitType.Tag = "0";
            // 
            // cmbTurnOver
            // 
            this.cmbTurnOver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTurnOver.FormattingEnabled = true;
            this.cmbTurnOver.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbTurnOver.Location = new System.Drawing.Point(478, 69);
            this.cmbTurnOver.Name = "cmbTurnOver";
            this.cmbTurnOver.Size = new System.Drawing.Size(93, 24);
            this.cmbTurnOver.TabIndex = 4;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(395, 74);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(71, 18);
            this.lblGender.TabIndex = 165;
            this.lblGender.Text = "TurnOver";
            // 
            // lblUnitID
            // 
            this.lblUnitID.AutoSize = true;
            this.lblUnitID.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnitID.Location = new System.Drawing.Point(23, 43);
            this.lblUnitID.Name = "lblUnitID";
            this.lblUnitID.Size = new System.Drawing.Size(51, 18);
            this.lblUnitID.TabIndex = 163;
            this.lblUnitID.Text = "UnitID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 18);
            this.label1.TabIndex = 147;
            this.label1.Text = "Owner\'s Name";
            // 
            // txtOwnerName
            // 
            this.txtOwnerName.Enabled = false;
            this.txtOwnerName.Location = new System.Drawing.Point(135, 111);
            this.txtOwnerName.Name = "txtOwnerName";
            this.txtOwnerName.Size = new System.Drawing.Size(133, 22);
            this.txtOwnerName.TabIndex = 5;
            // 
            // lblCivil
            // 
            this.lblCivil.AutoSize = true;
            this.lblCivil.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCivil.Location = new System.Drawing.Point(195, 74);
            this.lblCivil.Name = "lblCivil";
            this.lblCivil.Size = new System.Drawing.Size(72, 18);
            this.lblCivil.TabIndex = 160;
            this.lblCivil.Text = "Unit Type";
            this.lblCivil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(502, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 18);
            this.label4.TabIndex = 155;
            this.label4.Text = "Telephone Number";
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Enabled = false;
            this.txtPhoneNo.Location = new System.Drawing.Point(643, 111);
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(103, 22);
            this.txtPhoneNo.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 18);
            this.label6.TabIndex = 159;
            this.label6.Text = "Email Address";
            // 
            // txtEmail
            // 
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(170, 158);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(112, 22);
            this.txtEmail.TabIndex = 9;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.MonthlyDueListView);
            this.panel5.Location = new System.Drawing.Point(3, 205);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(904, 259);
            this.panel5.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.radioParkingFee);
            this.panel1.Controls.Add(this.radioMonthlyDue);
            this.panel1.Location = new System.Drawing.Point(96, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(281, 31);
            this.panel1.TabIndex = 166;
            // 
            // radioParkingFee
            // 
            this.radioParkingFee.AutoSize = true;
            this.radioParkingFee.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioParkingFee.Location = new System.Drawing.Point(143, 3);
            this.radioParkingFee.Name = "radioParkingFee";
            this.radioParkingFee.Size = new System.Drawing.Size(103, 22);
            this.radioParkingFee.TabIndex = 165;
            this.radioParkingFee.Text = "Parking Fee";
            this.radioParkingFee.UseVisualStyleBackColor = true;
            this.radioParkingFee.CheckedChanged += new System.EventHandler(this.radioParkingFee_CheckedChanged);
            // 
            // radioMonthlyDue
            // 
            this.radioMonthlyDue.AutoSize = true;
            this.radioMonthlyDue.Checked = true;
            this.radioMonthlyDue.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioMonthlyDue.Location = new System.Drawing.Point(18, 3);
            this.radioMonthlyDue.Name = "radioMonthlyDue";
            this.radioMonthlyDue.Size = new System.Drawing.Size(113, 22);
            this.radioMonthlyDue.TabIndex = 164;
            this.radioMonthlyDue.TabStop = true;
            this.radioMonthlyDue.Text = "Monthly Due";
            this.radioMonthlyDue.UseVisualStyleBackColor = true;
            this.radioMonthlyDue.CheckedChanged += new System.EventHandler(this.radioMonthlyDue_CheckedChanged);
            // 
            // MonthlyDueListView
            // 
            this.MonthlyDueListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MonthlyDueListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UnitID_MonthlyDue,
            this.Year,
            this.January,
            this.February,
            this.March,
            this.April,
            this.May,
            this.June,
            this.July,
            this.August,
            this.September,
            this.October,
            this.November,
            this.December});
            this.MonthlyDueListView.FullRowSelect = true;
            this.MonthlyDueListView.GridLines = true;
            this.MonthlyDueListView.Location = new System.Drawing.Point(96, 47);
            this.MonthlyDueListView.Name = "MonthlyDueListView";
            this.MonthlyDueListView.Size = new System.Drawing.Size(717, 157);
            this.MonthlyDueListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.MonthlyDueListView.TabIndex = 106;
            this.MonthlyDueListView.Tag = "";
            this.MonthlyDueListView.UseCompatibleStateImageBehavior = false;
            this.MonthlyDueListView.View = System.Windows.Forms.View.Details;
            // 
            // UnitID_MonthlyDue
            // 
            this.UnitID_MonthlyDue.Text = "UnitID";
            this.UnitID_MonthlyDue.Width = 80;
            // 
            // Year
            // 
            this.Year.Text = "Year";
            this.Year.Width = 80;
            // 
            // January
            // 
            this.January.Text = "January";
            this.January.Width = 80;
            // 
            // February
            // 
            this.February.Text = "February";
            this.February.Width = 80;
            // 
            // March
            // 
            this.March.Text = "March";
            this.March.Width = 80;
            // 
            // April
            // 
            this.April.Text = "April";
            this.April.Width = 80;
            // 
            // May
            // 
            this.May.Text = "May";
            this.May.Width = 80;
            // 
            // June
            // 
            this.June.Text = "June";
            this.June.Width = 80;
            // 
            // July
            // 
            this.July.Text = "July";
            this.July.Width = 80;
            // 
            // August
            // 
            this.August.Text = "August";
            this.August.Width = 80;
            // 
            // September
            // 
            this.September.Text = "September";
            this.September.Width = 80;
            // 
            // October
            // 
            this.October.Text = "October";
            this.October.Width = 80;
            // 
            // November
            // 
            this.November.Text = "November";
            this.November.Width = 80;
            // 
            // December
            // 
            this.December.Text = "December";
            this.December.Width = 80;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.panel11.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.btnCancelUnitInfo);
            this.panel11.Controls.Add(this.btnSave);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(150, 455);
            this.panel11.TabIndex = 151;
            // 
            // panel12
            // 
            this.panel12.Location = new System.Drawing.Point(158, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(783, 81);
            this.panel12.TabIndex = 10;
            // 
            // btnCancelUnitInfo
            // 
            this.btnCancelUnitInfo.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelUnitInfo.FlatAppearance.BorderSize = 0;
            this.btnCancelUnitInfo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnCancelUnitInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelUnitInfo.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelUnitInfo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancelUnitInfo.Image = global::WindowsFormsApplication2.Properties.Resources.Cancel1;
            this.btnCancelUnitInfo.Location = new System.Drawing.Point(-4, 107);
            this.btnCancelUnitInfo.Name = "btnCancelUnitInfo";
            this.btnCancelUnitInfo.Size = new System.Drawing.Size(157, 41);
            this.btnCancelUnitInfo.TabIndex = 150;
            this.btnCancelUnitInfo.Text = "CANCEL";
            this.btnCancelUnitInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelUnitInfo.UseVisualStyleBackColor = false;
            this.btnCancelUnitInfo.Click += new System.EventHandler(this.btnCancelUnitInfo_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Image = global::WindowsFormsApplication2.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(-3, 60);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(157, 41);
            this.btnSave.TabIndex = 149;
            this.btnSave.Text = "SAVE";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // IncomeTransaction
            // 
            this.IncomeTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.IncomeTransaction.Controls.Add(this.incTransBgPanel);
            this.IncomeTransaction.Controls.Add(this.panel13);
            this.IncomeTransaction.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IncomeTransaction.Location = new System.Drawing.Point(4, 27);
            this.IncomeTransaction.Name = "IncomeTransaction";
            this.IncomeTransaction.Size = new System.Drawing.Size(1052, 454);
            this.IncomeTransaction.TabIndex = 3;
            this.IncomeTransaction.Text = "Income Transactions";
            // 
            // incTransBgPanel
            // 
            this.incTransBgPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.incTransBgPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.incTransBgPanel.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.incTransBgPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.incTransBgPanel.Controls.Add(this.panel6);
            this.incTransBgPanel.Controls.Add(this.panel8);
            this.incTransBgPanel.Location = new System.Drawing.Point(150, 0);
            this.incTransBgPanel.Name = "incTransBgPanel";
            this.incTransBgPanel.Size = new System.Drawing.Size(900, 457);
            this.incTransBgPanel.TabIndex = 174;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.panel21);
            this.panel6.Controls.Add(this.pnlOthers);
            this.panel6.Controls.Add(this.pnlTotals);
            this.panel6.Controls.Add(this.pnlPerHead);
            this.panel6.Controls.Add(this.pnlReservation);
            this.panel6.Controls.Add(this.pnlHOA);
            this.panel6.Controls.Add(this.pnlQuantity);
            this.panel6.Controls.Add(this.pnlDates);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(900, 291);
            this.panel6.TabIndex = 0;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Transparent;
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.txtDate);
            this.panel21.Controls.Add(this.lblUnitIDTransaction);
            this.panel21.Controls.Add(this.txtUnitIDIncome);
            this.panel21.Controls.Add(this.label7);
            this.panel21.Controls.Add(this.label32);
            this.panel21.Controls.Add(this.txtORNoIncome);
            this.panel21.Controls.Add(this.label8);
            this.panel21.Controls.Add(this.cmbParticularIncome);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(900, 47);
            this.panel21.TabIndex = 172;
            // 
            // txtDate
            // 
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(623, 12);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(86, 22);
            this.txtDate.TabIndex = 161;
            // 
            // lblUnitIDTransaction
            // 
            this.lblUnitIDTransaction.AutoSize = true;
            this.lblUnitIDTransaction.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnitIDTransaction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblUnitIDTransaction.Location = new System.Drawing.Point(5, 14);
            this.lblUnitIDTransaction.Name = "lblUnitIDTransaction";
            this.lblUnitIDTransaction.Size = new System.Drawing.Size(55, 18);
            this.lblUnitIDTransaction.TabIndex = 147;
            this.lblUnitIDTransaction.Text = "Unit ID";
            // 
            // txtUnitIDIncome
            // 
            this.txtUnitIDIncome.Enabled = false;
            this.txtUnitIDIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnitIDIncome.Location = new System.Drawing.Point(69, 10);
            this.txtUnitIDIncome.Name = "txtUnitIDIncome";
            this.txtUnitIDIncome.Size = new System.Drawing.Size(66, 22);
            this.txtUnitIDIncome.TabIndex = 146;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(141, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 18);
            this.label7.TabIndex = 149;
            this.label7.Text = "OR #";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label32.Location = new System.Drawing.Point(575, 14);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(40, 18);
            this.label32.TabIndex = 162;
            this.label32.Text = "Date";
            // 
            // txtORNoIncome
            // 
            this.txtORNoIncome.Enabled = false;
            this.txtORNoIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtORNoIncome.Location = new System.Drawing.Point(190, 10);
            this.txtORNoIncome.Name = "txtORNoIncome";
            this.txtORNoIncome.Size = new System.Drawing.Size(66, 22);
            this.txtORNoIncome.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(262, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 18);
            this.label8.TabIndex = 151;
            this.label8.Text = "Particular";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbParticularIncome
            // 
            this.cmbParticularIncome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParticularIncome.FormattingEnabled = true;
            this.cmbParticularIncome.Location = new System.Drawing.Point(345, 10);
            this.cmbParticularIncome.Name = "cmbParticularIncome";
            this.cmbParticularIncome.Size = new System.Drawing.Size(224, 24);
            this.cmbParticularIncome.TabIndex = 2;
            this.cmbParticularIncome.Tag = "0";
            this.cmbParticularIncome.SelectedIndexChanged += new System.EventHandler(this.cmbParticularIncome_SelectedIndexChanged);
            // 
            // pnlOthers
            // 
            this.pnlOthers.BackColor = System.Drawing.SystemColors.Control;
            this.pnlOthers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOthers.Controls.Add(this.txtSpecify);
            this.pnlOthers.Controls.Add(this.label27);
            this.pnlOthers.Enabled = false;
            this.pnlOthers.Location = new System.Drawing.Point(13, 227);
            this.pnlOthers.Name = "pnlOthers";
            this.pnlOthers.Size = new System.Drawing.Size(400, 55);
            this.pnlOthers.TabIndex = 171;
            // 
            // txtSpecify
            // 
            this.txtSpecify.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtSpecify.Enabled = false;
            this.txtSpecify.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpecify.Location = new System.Drawing.Point(187, 15);
            this.txtSpecify.Name = "txtSpecify";
            this.txtSpecify.Size = new System.Drawing.Size(131, 22);
            this.txtSpecify.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(110, 15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 18);
            this.label27.TabIndex = 150;
            this.label27.Text = "Specify";
            // 
            // pnlTotals
            // 
            this.pnlTotals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTotals.BackColor = System.Drawing.SystemColors.Control;
            this.pnlTotals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotals.Controls.Add(this.checkDatePicker);
            this.pnlTotals.Controls.Add(this.label30);
            this.pnlTotals.Controls.Add(this.label34);
            this.pnlTotals.Controls.Add(this.txtTotalAmountIncome);
            this.pnlTotals.Controls.Add(this.label33);
            this.pnlTotals.Controls.Add(this.txtRemarks);
            this.pnlTotals.Controls.Add(this.label23);
            this.pnlTotals.Controls.Add(this.txtChangeIncome);
            this.pnlTotals.Controls.Add(this.lblAmountToPay);
            this.pnlTotals.Controls.Add(this.txtAmountToPay);
            this.pnlTotals.Controls.Add(this.label26);
            this.pnlTotals.Controls.Add(this.label22);
            this.pnlTotals.Controls.Add(this.button1);
            this.pnlTotals.Controls.Add(this.txtCheckNoIncome);
            this.pnlTotals.Controls.Add(this.txtCashIncome);
            this.pnlTotals.Controls.Add(this.label24);
            this.pnlTotals.Controls.Add(this.label25);
            this.pnlTotals.Controls.Add(this.txtCheckAmountIncome);
            this.pnlTotals.Enabled = false;
            this.pnlTotals.Location = new System.Drawing.Point(759, 52);
            this.pnlTotals.Name = "pnlTotals";
            this.pnlTotals.Size = new System.Drawing.Size(130, 238);
            this.pnlTotals.TabIndex = 165;
            // 
            // checkDatePicker
            // 
            this.checkDatePicker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkDatePicker.CalendarFont = new System.Drawing.Font("Eras Demi ITC", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkDatePicker.CustomFormat = "dd/MM/yyyy";
            this.checkDatePicker.Enabled = false;
            this.checkDatePicker.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.checkDatePicker.Location = new System.Drawing.Point(39, 106);
            this.checkDatePicker.Name = "checkDatePicker";
            this.checkDatePicker.Size = new System.Drawing.Size(162, 22);
            this.checkDatePicker.TabIndex = 16;
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(-49, 112);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(84, 18);
            this.label30.TabIndex = 178;
            this.label30.Text = "Check Date";
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(-24, 213);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 18);
            this.label34.TabIndex = 177;
            this.label34.Text = "Change";
            // 
            // txtTotalAmountIncome
            // 
            this.txtTotalAmountIncome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtTotalAmountIncome.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTotalAmountIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalAmountIncome.Enabled = false;
            this.txtTotalAmountIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmountIncome.Location = new System.Drawing.Point(40, 156);
            this.txtTotalAmountIncome.Name = "txtTotalAmountIncome";
            this.txtTotalAmountIncome.Size = new System.Drawing.Size(162, 22);
            this.txtTotalAmountIncome.TabIndex = 18;
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(-63, 158);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 18);
            this.label33.TabIndex = 176;
            this.label33.Text = "Total Amount";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtRemarks.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Enabled = false;
            this.txtRemarks.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.Location = new System.Drawing.Point(39, 4);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(162, 22);
            this.txtRemarks.TabIndex = 12;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(-27, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 18);
            this.label23.TabIndex = 174;
            this.label23.Text = "Remarks";
            // 
            // txtChangeIncome
            // 
            this.txtChangeIncome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtChangeIncome.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtChangeIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChangeIncome.Enabled = false;
            this.txtChangeIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChangeIncome.Location = new System.Drawing.Point(40, 211);
            this.txtChangeIncome.Name = "txtChangeIncome";
            this.txtChangeIncome.Size = new System.Drawing.Size(162, 22);
            this.txtChangeIncome.TabIndex = 171;
            // 
            // lblAmountToPay
            // 
            this.lblAmountToPay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAmountToPay.AutoSize = true;
            this.lblAmountToPay.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountToPay.Location = new System.Drawing.Point(-74, 33);
            this.lblAmountToPay.Name = "lblAmountToPay";
            this.lblAmountToPay.Size = new System.Drawing.Size(111, 18);
            this.lblAmountToPay.TabIndex = 163;
            this.lblAmountToPay.Text = "Amount To Pay";
            // 
            // txtAmountToPay
            // 
            this.txtAmountToPay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtAmountToPay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtAmountToPay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmountToPay.Enabled = false;
            this.txtAmountToPay.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountToPay.Location = new System.Drawing.Point(39, 30);
            this.txtAmountToPay.Name = "txtAmountToPay";
            this.txtAmountToPay.Size = new System.Drawing.Size(162, 22);
            this.txtAmountToPay.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(-24, 234);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 16);
            this.label26.TabIndex = 172;
            this.label26.Text = "Change";
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(-40, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 18);
            this.label22.TabIndex = 165;
            this.label22.Text = "Check No.";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(81, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 29);
            this.button1.TabIndex = 19;
            this.button1.Text = "Pay";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtCheckNoIncome
            // 
            this.txtCheckNoIncome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCheckNoIncome.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtCheckNoIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCheckNoIncome.Enabled = false;
            this.txtCheckNoIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckNoIncome.Location = new System.Drawing.Point(39, 55);
            this.txtCheckNoIncome.Name = "txtCheckNoIncome";
            this.txtCheckNoIncome.Size = new System.Drawing.Size(162, 22);
            this.txtCheckNoIncome.TabIndex = 14;
            this.txtCheckNoIncome.TextChanged += new System.EventHandler(this.txtCheckNoIncome_TextChanged);
            // 
            // txtCashIncome
            // 
            this.txtCashIncome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCashIncome.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtCashIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCashIncome.Enabled = false;
            this.txtCashIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashIncome.Location = new System.Drawing.Point(40, 131);
            this.txtCashIncome.Name = "txtCashIncome";
            this.txtCashIncome.Size = new System.Drawing.Size(162, 22);
            this.txtCashIncome.TabIndex = 17;
            this.txtCashIncome.TextChanged += new System.EventHandler(this.txtCashIncome_TextChanged);
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(-73, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 18);
            this.label24.TabIndex = 167;
            this.label24.Text = "Check Amount";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(-3, 134);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 18);
            this.label25.TabIndex = 169;
            this.label25.Text = "Cash ";
            // 
            // txtCheckAmountIncome
            // 
            this.txtCheckAmountIncome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCheckAmountIncome.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtCheckAmountIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCheckAmountIncome.Enabled = false;
            this.txtCheckAmountIncome.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckAmountIncome.Location = new System.Drawing.Point(39, 80);
            this.txtCheckAmountIncome.Name = "txtCheckAmountIncome";
            this.txtCheckAmountIncome.Size = new System.Drawing.Size(162, 22);
            this.txtCheckAmountIncome.TabIndex = 15;
            this.txtCheckAmountIncome.TextChanged += new System.EventHandler(this.txtCheckAmountIncome_TextChanged);
            // 
            // pnlPerHead
            // 
            this.pnlPerHead.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPerHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPerHead.Controls.Add(this.label19);
            this.pnlPerHead.Controls.Add(this.txtNoOfGuests);
            this.pnlPerHead.Controls.Add(this.txtNoOfResidents);
            this.pnlPerHead.Controls.Add(this.label18);
            this.pnlPerHead.Enabled = false;
            this.pnlPerHead.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPerHead.Location = new System.Drawing.Point(419, 152);
            this.pnlPerHead.Name = "pnlPerHead";
            this.pnlPerHead.Size = new System.Drawing.Size(326, 67);
            this.pnlPerHead.TabIndex = 170;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(59, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(117, 18);
            this.label19.TabIndex = 154;
            this.label19.Text = "No. Of Residents";
            // 
            // txtNoOfGuests
            // 
            this.txtNoOfGuests.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtNoOfGuests.Enabled = false;
            this.txtNoOfGuests.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoOfGuests.Location = new System.Drawing.Point(178, 36);
            this.txtNoOfGuests.Name = "txtNoOfGuests";
            this.txtNoOfGuests.Size = new System.Drawing.Size(98, 22);
            this.txtNoOfGuests.TabIndex = 11;
            this.txtNoOfGuests.TextChanged += new System.EventHandler(this.txtNoOfGuests_TextChanged);
            // 
            // txtNoOfResidents
            // 
            this.txtNoOfResidents.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtNoOfResidents.Enabled = false;
            this.txtNoOfResidents.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoOfResidents.Location = new System.Drawing.Point(178, 10);
            this.txtNoOfResidents.Name = "txtNoOfResidents";
            this.txtNoOfResidents.Size = new System.Drawing.Size(98, 22);
            this.txtNoOfResidents.TabIndex = 10;
            this.txtNoOfResidents.TextChanged += new System.EventHandler(this.txtNoOfResidents_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(75, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 18);
            this.label18.TabIndex = 156;
            this.label18.Text = "No. Of Guests";
            // 
            // pnlReservation
            // 
            this.pnlReservation.BackColor = System.Drawing.SystemColors.Control;
            this.pnlReservation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlReservation.Controls.Add(this.cmbReservation);
            this.pnlReservation.Controls.Add(this.label17);
            this.pnlReservation.Enabled = false;
            this.pnlReservation.Location = new System.Drawing.Point(13, 169);
            this.pnlReservation.Name = "pnlReservation";
            this.pnlReservation.Size = new System.Drawing.Size(400, 51);
            this.pnlReservation.TabIndex = 169;
            // 
            // cmbReservation
            // 
            this.cmbReservation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReservation.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbReservation.FormattingEnabled = true;
            this.cmbReservation.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbReservation.Location = new System.Drawing.Point(187, 13);
            this.cmbReservation.Name = "cmbReservation";
            this.cmbReservation.Size = new System.Drawing.Size(131, 23);
            this.cmbReservation.TabIndex = 6;
            this.cmbReservation.Tag = "0";
            this.cmbReservation.SelectedIndexChanged += new System.EventHandler(this.cmbReservation_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(56, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(117, 18);
            this.label17.TabIndex = 152;
            this.label17.Text = "For Reservation?";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlHOA
            // 
            this.pnlHOA.BackColor = System.Drawing.SystemColors.Control;
            this.pnlHOA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHOA.Controls.Add(this.txtDiscount);
            this.pnlHOA.Controls.Add(this.label20);
            this.pnlHOA.Controls.Add(this.txtPenalty);
            this.pnlHOA.Controls.Add(this.label21);
            this.pnlHOA.Enabled = false;
            this.pnlHOA.Location = new System.Drawing.Point(419, 79);
            this.pnlHOA.Name = "pnlHOA";
            this.pnlHOA.Size = new System.Drawing.Size(326, 62);
            this.pnlHOA.TabIndex = 168;
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtDiscount.Enabled = false;
            this.txtDiscount.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Location = new System.Drawing.Point(234, 20);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(66, 22);
            this.txtDiscount.TabIndex = 9;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(161, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 18);
            this.label20.TabIndex = 147;
            this.label20.Text = "Discount";
            // 
            // txtPenalty
            // 
            this.txtPenalty.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPenalty.Enabled = false;
            this.txtPenalty.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPenalty.Location = new System.Drawing.Point(89, 20);
            this.txtPenalty.Name = "txtPenalty";
            this.txtPenalty.Size = new System.Drawing.Size(66, 22);
            this.txtPenalty.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(27, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 18);
            this.label21.TabIndex = 145;
            this.label21.Text = "Penalty";
            // 
            // pnlQuantity
            // 
            this.pnlQuantity.BackColor = System.Drawing.SystemColors.Control;
            this.pnlQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlQuantity.Controls.Add(this.txtQuantity);
            this.pnlQuantity.Controls.Add(this.label15);
            this.pnlQuantity.Enabled = false;
            this.pnlQuantity.Location = new System.Drawing.Point(13, 108);
            this.pnlQuantity.Name = "pnlQuantity";
            this.pnlQuantity.Size = new System.Drawing.Size(400, 55);
            this.pnlQuantity.TabIndex = 167;
            // 
            // txtQuantity
            // 
            this.txtQuantity.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtQuantity.Enabled = false;
            this.txtQuantity.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantity.Location = new System.Drawing.Point(187, 16);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(131, 22);
            this.txtQuantity.TabIndex = 5;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(98, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 18);
            this.label15.TabIndex = 144;
            this.label15.Text = "Quantity";
            // 
            // pnlDates
            // 
            this.pnlDates.BackColor = System.Drawing.SystemColors.Control;
            this.pnlDates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDates.Controls.Add(this.label11);
            this.pnlDates.Controls.Add(this.label9);
            this.pnlDates.Controls.Add(this.todatePicker);
            this.pnlDates.Controls.Add(this.fromdatePicker);
            this.pnlDates.Enabled = false;
            this.pnlDates.Location = new System.Drawing.Point(13, 52);
            this.pnlDates.Name = "pnlDates";
            this.pnlDates.Size = new System.Drawing.Size(401, 50);
            this.pnlDates.TabIndex = 166;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(215, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 18);
            this.label11.TabIndex = 142;
            this.label11.Text = "To";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 18);
            this.label9.TabIndex = 141;
            this.label9.Text = "From";
            // 
            // todatePicker
            // 
            this.todatePicker.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.todatePicker.CustomFormat = "MMMM/yyyy";
            this.todatePicker.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.todatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.todatePicker.Location = new System.Drawing.Point(244, 12);
            this.todatePicker.Name = "todatePicker";
            this.todatePicker.Size = new System.Drawing.Size(144, 22);
            this.todatePicker.TabIndex = 4;
            this.todatePicker.ValueChanged += new System.EventHandler(this.todatePicker_ValueChanged);
            // 
            // fromdatePicker
            // 
            this.fromdatePicker.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.fromdatePicker.CustomFormat = "MMMM/yyyy";
            this.fromdatePicker.Font = new System.Drawing.Font("Eras Medium ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromdatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fromdatePicker.Location = new System.Drawing.Point(59, 12);
            this.fromdatePicker.Name = "fromdatePicker";
            this.fromdatePicker.Size = new System.Drawing.Size(149, 22);
            this.fromdatePicker.TabIndex = 3;
            this.fromdatePicker.ValueChanged += new System.EventHandler(this.fromdatePicker_ValueChanged);
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.IncomeTransactionsListView);
            this.panel8.Location = new System.Drawing.Point(0, 297);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(900, 158);
            this.panel8.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.radioUnitTransaction);
            this.panel9.Controls.Add(this.radioAllTransaction);
            this.panel9.Location = new System.Drawing.Point(12, 6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(327, 31);
            this.panel9.TabIndex = 168;
            // 
            // radioUnitTransaction
            // 
            this.radioUnitTransaction.AutoSize = true;
            this.radioUnitTransaction.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioUnitTransaction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioUnitTransaction.Location = new System.Drawing.Point(153, 2);
            this.radioUnitTransaction.Name = "radioUnitTransaction";
            this.radioUnitTransaction.Size = new System.Drawing.Size(150, 22);
            this.radioUnitTransaction.TabIndex = 164;
            this.radioUnitTransaction.Text = "Unit\'s Transactions";
            this.radioUnitTransaction.UseVisualStyleBackColor = true;
            this.radioUnitTransaction.CheckedChanged += new System.EventHandler(this.radioUnitTransaction_CheckedChanged);
            // 
            // radioAllTransaction
            // 
            this.radioAllTransaction.AutoSize = true;
            this.radioAllTransaction.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioAllTransaction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioAllTransaction.Location = new System.Drawing.Point(5, 2);
            this.radioAllTransaction.Name = "radioAllTransaction";
            this.radioAllTransaction.Size = new System.Drawing.Size(129, 22);
            this.radioAllTransaction.TabIndex = 163;
            this.radioAllTransaction.Text = "All Transactions";
            this.radioAllTransaction.UseVisualStyleBackColor = true;
            this.radioAllTransaction.CheckedChanged += new System.EventHandler(this.radioAllTransaction_CheckedChanged);
            // 
            // IncomeTransactionsListView
            // 
            this.IncomeTransactionsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IncomeTransactionsListView.BackColor = System.Drawing.SystemColors.Control;
            this.IncomeTransactionsListView.FullRowSelect = true;
            this.IncomeTransactionsListView.GridLines = true;
            this.IncomeTransactionsListView.Location = new System.Drawing.Point(12, 36);
            this.IncomeTransactionsListView.MultiSelect = false;
            this.IncomeTransactionsListView.Name = "IncomeTransactionsListView";
            this.IncomeTransactionsListView.Size = new System.Drawing.Size(876, 112);
            this.IncomeTransactionsListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.IncomeTransactionsListView.TabIndex = 106;
            this.IncomeTransactionsListView.Tag = "";
            this.IncomeTransactionsListView.UseCompatibleStateImageBehavior = false;
            this.IncomeTransactionsListView.View = System.Windows.Forms.View.Details;
            this.IncomeTransactionsListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.TransactionsListView_ColumnClick);
            this.IncomeTransactionsListView.DoubleClick += new System.EventHandler(this.IncomeTransactionsListView_DoubleClick);
            this.IncomeTransactionsListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.IncomeTransactionsListView_MouseClick);
            // 
            // panel13
            // 
            this.panel13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.panel13.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel13.Controls.Add(this.btnClear);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.btnCancelIncome);
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(150, 455);
            this.panel13.TabIndex = 163;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Image = global::WindowsFormsApplication2.Properties.Resources.Delete1;
            this.btnClear.Location = new System.Drawing.Point(-3, 60);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(157, 41);
            this.btnClear.TabIndex = 161;
            this.btnClear.Text = "CLEAR";
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // panel14
            // 
            this.panel14.Location = new System.Drawing.Point(158, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(783, 81);
            this.panel14.TabIndex = 10;
            // 
            // btnCancelIncome
            // 
            this.btnCancelIncome.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelIncome.FlatAppearance.BorderSize = 0;
            this.btnCancelIncome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnCancelIncome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelIncome.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelIncome.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancelIncome.Image = global::WindowsFormsApplication2.Properties.Resources.Cancel1;
            this.btnCancelIncome.Location = new System.Drawing.Point(-3, 107);
            this.btnCancelIncome.Name = "btnCancelIncome";
            this.btnCancelIncome.Size = new System.Drawing.Size(157, 41);
            this.btnCancelIncome.TabIndex = 160;
            this.btnCancelIncome.Text = "CANCEL";
            this.btnCancelIncome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelIncome.UseVisualStyleBackColor = false;
            this.btnCancelIncome.Click += new System.EventHandler(this.btnCancelIncome_Click);
            // 
            // ExpenseTransaction
            // 
            this.ExpenseTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.ExpenseTransaction.Controls.Add(this.panel18);
            this.ExpenseTransaction.Controls.Add(this.panel15);
            this.ExpenseTransaction.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpenseTransaction.Location = new System.Drawing.Point(4, 27);
            this.ExpenseTransaction.Name = "ExpenseTransaction";
            this.ExpenseTransaction.Size = new System.Drawing.Size(1052, 454);
            this.ExpenseTransaction.TabIndex = 4;
            this.ExpenseTransaction.Text = "Transactions";
            // 
            // panel18
            // 
            this.panel18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.panel18.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.panel18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel18.Controls.Add(this.radioIncome);
            this.panel18.Controls.Add(this.radioButton2);
            this.panel18.Controls.Add(this.lblTotal);
            this.panel18.Controls.Add(this.txtTotal);
            this.panel18.Controls.Add(this.label31);
            this.panel18.Controls.Add(this.txtSearchAllTransactions);
            this.panel18.Controls.Add(this.AllTransactionsListView);
            this.panel18.Location = new System.Drawing.Point(150, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(902, 460);
            this.panel18.TabIndex = 9;
            // 
            // radioIncome
            // 
            this.radioIncome.AutoSize = true;
            this.radioIncome.Checked = true;
            this.radioIncome.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioIncome.Location = new System.Drawing.Point(17, 25);
            this.radioIncome.Name = "radioIncome";
            this.radioIncome.Size = new System.Drawing.Size(76, 22);
            this.radioIncome.TabIndex = 10;
            this.radioIncome.TabStop = true;
            this.radioIncome.Text = "Income";
            this.radioIncome.UseVisualStyleBackColor = true;
            this.radioIncome.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(109, 25);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(81, 22);
            this.radioButton2.TabIndex = 11;
            this.radioButton2.Text = "Expense";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(614, 57);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(95, 18);
            this.lblTotal.TabIndex = 170;
            this.lblTotal.Text = "Total Income";
            // 
            // txtTotal
            // 
            this.txtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotal.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(723, 57);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(158, 25);
            this.txtTotal.TabIndex = 169;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(228, 59);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(52, 18);
            this.label31.TabIndex = 168;
            this.label31.Text = "Search";
            // 
            // txtSearchAllTransactions
            // 
            this.txtSearchAllTransactions.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchAllTransactions.Location = new System.Drawing.Point(16, 57);
            this.txtSearchAllTransactions.Name = "txtSearchAllTransactions";
            this.txtSearchAllTransactions.Size = new System.Drawing.Size(206, 25);
            this.txtSearchAllTransactions.TabIndex = 12;
            this.txtSearchAllTransactions.TextChanged += new System.EventHandler(this.txtSearchAllTransactions_TextChanged);
            // 
            // AllTransactionsListView
            // 
            this.AllTransactionsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AllTransactionsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.AllTransactionsListView.FullRowSelect = true;
            this.AllTransactionsListView.GridLines = true;
            this.AllTransactionsListView.Location = new System.Drawing.Point(16, 86);
            this.AllTransactionsListView.Name = "AllTransactionsListView";
            this.AllTransactionsListView.Size = new System.Drawing.Size(865, 353);
            this.AllTransactionsListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.AllTransactionsListView.TabIndex = 167;
            this.AllTransactionsListView.Tag = "";
            this.AllTransactionsListView.UseCompatibleStateImageBehavior = false;
            this.AllTransactionsListView.View = System.Windows.Forms.View.Details;
            this.AllTransactionsListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.AllTransactionsListView_ColumnClick);
            this.AllTransactionsListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AllTransactionsListView_MouseClick);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "ORnum";
            this.columnHeader11.Width = 110;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Date";
            this.columnHeader12.Width = 110;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Unit ID";
            this.columnHeader13.Width = 110;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Code";
            this.columnHeader14.Width = 110;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Tag = "Numeric";
            this.columnHeader15.Text = "Amount Paid";
            this.columnHeader15.Width = 110;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Tag = "Numeric";
            this.columnHeader16.Text = "Cash";
            this.columnHeader16.Width = 110;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Check #";
            this.columnHeader17.Width = 110;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Tag = "Numeric";
            this.columnHeader18.Text = "Check Amount";
            this.columnHeader18.Width = 110;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Tag = "Numeric";
            this.columnHeader19.Text = "Total Amount";
            this.columnHeader19.Width = 110;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Remarks";
            this.columnHeader20.Width = 110;
            // 
            // panel15
            // 
            this.panel15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.panel15.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.panel15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel15.Controls.Add(this.btnNewExpense);
            this.panel15.Controls.Add(this.btnCancelTransaction);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(150, 455);
            this.panel15.TabIndex = 7;
            // 
            // btnNewExpense
            // 
            this.btnNewExpense.BackColor = System.Drawing.Color.Transparent;
            this.btnNewExpense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewExpense.FlatAppearance.BorderSize = 0;
            this.btnNewExpense.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnNewExpense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewExpense.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewExpense.ForeColor = System.Drawing.SystemColors.Control;
            this.btnNewExpense.Image = global::WindowsFormsApplication2.Properties.Resources.Add_New_32;
            this.btnNewExpense.Location = new System.Drawing.Point(-3, 60);
            this.btnNewExpense.Name = "btnNewExpense";
            this.btnNewExpense.Size = new System.Drawing.Size(157, 57);
            this.btnNewExpense.TabIndex = 12;
            this.btnNewExpense.Text = "New Expense";
            this.btnNewExpense.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewExpense.UseVisualStyleBackColor = false;
            this.btnNewExpense.Click += new System.EventHandler(this.btnNewExpense_Click);
            // 
            // btnCancelTransaction
            // 
            this.btnCancelTransaction.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelTransaction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelTransaction.FlatAppearance.BorderSize = 0;
            this.btnCancelTransaction.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnCancelTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelTransaction.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelTransaction.ForeColor = System.Drawing.SystemColors.Control;
            this.btnCancelTransaction.Image = global::WindowsFormsApplication2.Properties.Resources.Cancel1;
            this.btnCancelTransaction.Location = new System.Drawing.Point(-3, 123);
            this.btnCancelTransaction.Name = "btnCancelTransaction";
            this.btnCancelTransaction.Size = new System.Drawing.Size(157, 54);
            this.btnCancelTransaction.TabIndex = 11;
            this.btnCancelTransaction.Text = "Cancel Transaction";
            this.btnCancelTransaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelTransaction.UseVisualStyleBackColor = false;
            this.btnCancelTransaction.Click += new System.EventHandler(this.btnCancelTransaction_Click);
            // 
            // panel16
            // 
            this.panel16.Location = new System.Drawing.Point(158, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(783, 81);
            this.panel16.TabIndex = 10;
            // 
            // IncomeServices
            // 
            this.IncomeServices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.IncomeServices.Controls.Add(this.pnlIncomeItems);
            this.IncomeServices.Controls.Add(this.panel19);
            this.IncomeServices.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IncomeServices.Location = new System.Drawing.Point(4, 27);
            this.IncomeServices.Name = "IncomeServices";
            this.IncomeServices.Size = new System.Drawing.Size(1052, 454);
            this.IncomeServices.TabIndex = 5;
            this.IncomeServices.Text = "Income Services";
            // 
            // pnlIncomeItems
            // 
            this.pnlIncomeItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIncomeItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.pnlIncomeItems.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.pnlIncomeItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlIncomeItems.Controls.Add(this.label36);
            this.pnlIncomeItems.Controls.Add(this.txtSearchServices);
            this.pnlIncomeItems.Controls.Add(this.radioExpenseServices);
            this.pnlIncomeItems.Controls.Add(this.ServicesListView);
            this.pnlIncomeItems.Controls.Add(this.radioIncomeServices);
            this.pnlIncomeItems.Location = new System.Drawing.Point(150, 0);
            this.pnlIncomeItems.Name = "pnlIncomeItems";
            this.pnlIncomeItems.Size = new System.Drawing.Size(902, 457);
            this.pnlIncomeItems.TabIndex = 10;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(241, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 18);
            this.label36.TabIndex = 178;
            this.label36.Text = "Search";
            // 
            // txtSearchServices
            // 
            this.txtSearchServices.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchServices.Location = new System.Drawing.Point(29, 46);
            this.txtSearchServices.Name = "txtSearchServices";
            this.txtSearchServices.Size = new System.Drawing.Size(206, 25);
            this.txtSearchServices.TabIndex = 177;
            this.txtSearchServices.TextChanged += new System.EventHandler(this.txtSearchServices_TextChanged);
            // 
            // radioExpenseServices
            // 
            this.radioExpenseServices.AutoSize = true;
            this.radioExpenseServices.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioExpenseServices.Location = new System.Drawing.Point(149, 13);
            this.radioExpenseServices.Name = "radioExpenseServices";
            this.radioExpenseServices.Size = new System.Drawing.Size(121, 22);
            this.radioExpenseServices.TabIndex = 176;
            this.radioExpenseServices.Text = "Expense Items";
            this.radioExpenseServices.UseVisualStyleBackColor = true;
            this.radioExpenseServices.CheckedChanged += new System.EventHandler(this.radioExpenseServices_CheckedChanged);
            // 
            // ServicesListView
            // 
            this.ServicesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ServicesListView.FullRowSelect = true;
            this.ServicesListView.GridLines = true;
            this.ServicesListView.Location = new System.Drawing.Point(29, 74);
            this.ServicesListView.Name = "ServicesListView";
            this.ServicesListView.Size = new System.Drawing.Size(849, 407);
            this.ServicesListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.ServicesListView.TabIndex = 168;
            this.ServicesListView.Tag = "";
            this.ServicesListView.UseCompatibleStateImageBehavior = false;
            this.ServicesListView.View = System.Windows.Forms.View.Details;
            this.ServicesListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ServicesListView_ColumnClick);
            // 
            // radioIncomeServices
            // 
            this.radioIncomeServices.AutoSize = true;
            this.radioIncomeServices.Checked = true;
            this.radioIncomeServices.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioIncomeServices.Location = new System.Drawing.Point(27, 13);
            this.radioIncomeServices.Name = "radioIncomeServices";
            this.radioIncomeServices.Size = new System.Drawing.Size(116, 22);
            this.radioIncomeServices.TabIndex = 175;
            this.radioIncomeServices.TabStop = true;
            this.radioIncomeServices.Text = "Income Items";
            this.radioIncomeServices.UseVisualStyleBackColor = true;
            this.radioIncomeServices.CheckedChanged += new System.EventHandler(this.radioIncomeServices_CheckedChanged);
            // 
            // panel19
            // 
            this.panel19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.panel19.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.panel19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel19.Controls.Add(this.btnDelete_Item);
            this.panel19.Controls.Add(this.btnEdit_Item);
            this.panel19.Controls.Add(this.btnAdd_Item);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(150, 455);
            this.panel19.TabIndex = 7;
            // 
            // btnDelete_Item
            // 
            this.btnDelete_Item.BackColor = System.Drawing.Color.Transparent;
            this.btnDelete_Item.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete_Item.FlatAppearance.BorderSize = 0;
            this.btnDelete_Item.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnDelete_Item.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete_Item.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete_Item.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDelete_Item.Image = global::WindowsFormsApplication2.Properties.Resources.Delete1;
            this.btnDelete_Item.Location = new System.Drawing.Point(-3, 154);
            this.btnDelete_Item.Name = "btnDelete_Item";
            this.btnDelete_Item.Size = new System.Drawing.Size(157, 41);
            this.btnDelete_Item.TabIndex = 15;
            this.btnDelete_Item.Text = "DELETE";
            this.btnDelete_Item.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete_Item.UseVisualStyleBackColor = false;
            this.btnDelete_Item.Click += new System.EventHandler(this.btnDelete_Item_Click);
            // 
            // btnEdit_Item
            // 
            this.btnEdit_Item.BackColor = System.Drawing.Color.Transparent;
            this.btnEdit_Item.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEdit_Item.FlatAppearance.BorderSize = 0;
            this.btnEdit_Item.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnEdit_Item.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit_Item.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit_Item.ForeColor = System.Drawing.SystemColors.Control;
            this.btnEdit_Item.Image = global::WindowsFormsApplication2.Properties.Resources.Edit1;
            this.btnEdit_Item.Location = new System.Drawing.Point(-3, 107);
            this.btnEdit_Item.Name = "btnEdit_Item";
            this.btnEdit_Item.Size = new System.Drawing.Size(157, 41);
            this.btnEdit_Item.TabIndex = 14;
            this.btnEdit_Item.Text = "EDIT";
            this.btnEdit_Item.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEdit_Item.UseVisualStyleBackColor = false;
            this.btnEdit_Item.Click += new System.EventHandler(this.btnEdit_Item_Click);
            // 
            // btnAdd_Item
            // 
            this.btnAdd_Item.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd_Item.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd_Item.FlatAppearance.BorderSize = 0;
            this.btnAdd_Item.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnAdd_Item.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd_Item.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd_Item.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAdd_Item.Image = global::WindowsFormsApplication2.Properties.Resources.Add_New_32;
            this.btnAdd_Item.Location = new System.Drawing.Point(-3, 60);
            this.btnAdd_Item.Name = "btnAdd_Item";
            this.btnAdd_Item.Size = new System.Drawing.Size(157, 41);
            this.btnAdd_Item.TabIndex = 13;
            this.btnAdd_Item.Text = "ADD";
            this.btnAdd_Item.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd_Item.UseVisualStyleBackColor = false;
            this.btnAdd_Item.Click += new System.EventHandler(this.btnAdd_Item_Click);
            // 
            // panel20
            // 
            this.panel20.Location = new System.Drawing.Point(158, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(783, 81);
            this.panel20.TabIndex = 10;
            // 
            // Reports
            // 
            this.Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.Reports.Controls.Add(this.panel25);
            this.Reports.Controls.Add(this.panel22);
            this.Reports.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reports.Location = new System.Drawing.Point(4, 27);
            this.Reports.Name = "Reports";
            this.Reports.Size = new System.Drawing.Size(1052, 454);
            this.Reports.TabIndex = 6;
            this.Reports.Text = "Reports";
            // 
            // panel25
            // 
            this.panel25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.panel25.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel25.Controls.Add(this.reportViewer1);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Location = new System.Drawing.Point(150, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(902, 457);
            this.panel25.TabIndex = 10;
            // 
            // reportViewer1
            // 
            this.reportViewer1.AllowDrop = true;
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportViewer1.Location = new System.Drawing.Point(38, 48);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(835, 391);
            this.reportViewer1.TabIndex = 174;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Transparent;
            this.panel26.Controls.Add(this.button8);
            this.panel26.Controls.Add(this.button7);
            this.panel26.Controls.Add(this.button4);
            this.panel26.Controls.Add(this.button3);
            this.panel26.Controls.Add(this.button2);
            this.panel26.Controls.Add(this.cmbUnit);
            this.panel26.Controls.Add(this.labelUnitType);
            this.panel26.Controls.Add(this.btnLoad);
            this.panel26.Controls.Add(this.labelYear);
            this.panel26.Controls.Add(this.txtYear);
            this.panel26.Controls.Add(this.dateTo);
            this.panel26.Controls.Add(this.txtFrom);
            this.panel26.Controls.Add(this.txtTo);
            this.panel26.Controls.Add(this.dateFrom);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(902, 56);
            this.panel26.TabIndex = 173;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Transparent;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button8.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Location = new System.Drawing.Point(361, 21);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(61, 22);
            this.button8.TabIndex = 162;
            this.button8.Text = "Load";
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Transparent;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button7.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Location = new System.Drawing.Point(361, 21);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(61, 22);
            this.button7.TabIndex = 161;
            this.button7.Text = "Load";
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button4.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(361, 20);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(61, 22);
            this.button4.TabIndex = 160;
            this.button4.Text = "Load";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button3.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(183, 20);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 22);
            this.button3.TabIndex = 159;
            this.button3.Text = "Load";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button2.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(323, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(61, 22);
            this.button2.TabIndex = 156;
            this.button2.Text = "Load";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmbUnit
            // 
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "SU",
            "TH"});
            this.cmbUnit.Location = new System.Drawing.Point(127, 20);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(50, 24);
            this.cmbUnit.TabIndex = 155;
            this.cmbUnit.Visible = false;
            // 
            // labelUnitType
            // 
            this.labelUnitType.AutoSize = true;
            this.labelUnitType.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnitType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelUnitType.Location = new System.Drawing.Point(56, 23);
            this.labelUnitType.Name = "labelUnitType";
            this.labelUnitType.Size = new System.Drawing.Size(72, 18);
            this.labelUnitType.TabIndex = 154;
            this.labelUnitType.Text = "Unit Type";
            this.labelUnitType.Visible = false;
            // 
            // btnLoad
            // 
            this.btnLoad.BackColor = System.Drawing.Color.Transparent;
            this.btnLoad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnLoad.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.ForeColor = System.Drawing.Color.Black;
            this.btnLoad.Location = new System.Drawing.Point(361, 20);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(61, 22);
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "Load";
            this.btnLoad.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnLoad.UseVisualStyleBackColor = false;
            this.btnLoad.Visible = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click_1);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelYear.Location = new System.Drawing.Point(184, 23);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(35, 16);
            this.labelYear.TabIndex = 153;
            this.labelYear.Text = "Year";
            this.labelYear.Visible = false;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(221, 20);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(100, 22);
            this.txtYear.TabIndex = 152;
            this.txtYear.Visible = false;
            // 
            // dateTo
            // 
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTo.Location = new System.Drawing.Point(241, 18);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(114, 22);
            this.dateTo.TabIndex = 151;
            this.dateTo.Value = new System.DateTime(2017, 10, 14, 0, 0, 0, 0);
            this.dateTo.Visible = false;
            // 
            // txtFrom
            // 
            this.txtFrom.AutoSize = true;
            this.txtFrom.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrom.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtFrom.Location = new System.Drawing.Point(40, 23);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(46, 18);
            this.txtFrom.TabIndex = 148;
            this.txtFrom.Text = "From:";
            this.txtFrom.Visible = false;
            // 
            // txtTo
            // 
            this.txtTo.AutoSize = true;
            this.txtTo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTo.Location = new System.Drawing.Point(209, 23);
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(26, 16);
            this.txtTo.TabIndex = 150;
            this.txtTo.Text = "To:";
            this.txtTo.Visible = false;
            // 
            // dateFrom
            // 
            this.dateFrom.CustomFormat = "";
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFrom.Location = new System.Drawing.Point(89, 18);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(114, 22);
            this.dateFrom.TabIndex = 149;
            this.dateFrom.Value = new System.DateTime(2017, 10, 14, 0, 0, 0, 0);
            this.dateFrom.Visible = false;
            // 
            // panel22
            // 
            this.panel22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.panel22.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.panel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel22.Controls.Add(this.button6);
            this.panel22.Controls.Add(this.button5);
            this.panel22.Controls.Add(this.btnExpenseReport);
            this.panel22.Controls.Add(this.btnMonthlyDuesReport);
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.btnIncomeReport);
            this.panel22.Controls.Add(this.btnUnitReport);
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(150, 455);
            this.panel22.TabIndex = 8;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(-7, 301);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(157, 41);
            this.button6.TabIndex = 15;
            this.button6.Text = "Expense Logs";
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(-7, 254);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(157, 41);
            this.button5.TabIndex = 14;
            this.button5.Text = "Income Logs";
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnExpenseReport
            // 
            this.btnExpenseReport.BackColor = System.Drawing.Color.Transparent;
            this.btnExpenseReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExpenseReport.FlatAppearance.BorderSize = 0;
            this.btnExpenseReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnExpenseReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpenseReport.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExpenseReport.ForeColor = System.Drawing.Color.White;
            this.btnExpenseReport.Location = new System.Drawing.Point(-7, 160);
            this.btnExpenseReport.Name = "btnExpenseReport";
            this.btnExpenseReport.Size = new System.Drawing.Size(157, 41);
            this.btnExpenseReport.TabIndex = 13;
            this.btnExpenseReport.Text = "Expense";
            this.btnExpenseReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExpenseReport.UseVisualStyleBackColor = false;
            this.btnExpenseReport.Click += new System.EventHandler(this.btnExpenseReport_Click);
            // 
            // btnMonthlyDuesReport
            // 
            this.btnMonthlyDuesReport.BackColor = System.Drawing.Color.Transparent;
            this.btnMonthlyDuesReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMonthlyDuesReport.FlatAppearance.BorderSize = 0;
            this.btnMonthlyDuesReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnMonthlyDuesReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMonthlyDuesReport.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonthlyDuesReport.ForeColor = System.Drawing.Color.White;
            this.btnMonthlyDuesReport.Location = new System.Drawing.Point(-7, 207);
            this.btnMonthlyDuesReport.Name = "btnMonthlyDuesReport";
            this.btnMonthlyDuesReport.Size = new System.Drawing.Size(157, 41);
            this.btnMonthlyDuesReport.TabIndex = 12;
            this.btnMonthlyDuesReport.Text = "MonthlyDues";
            this.btnMonthlyDuesReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMonthlyDuesReport.UseVisualStyleBackColor = false;
            this.btnMonthlyDuesReport.Click += new System.EventHandler(this.btnMonthlyDuesReport_Click);
            // 
            // panel23
            // 
            this.panel23.Location = new System.Drawing.Point(158, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(783, 81);
            this.panel23.TabIndex = 10;
            // 
            // btnIncomeReport
            // 
            this.btnIncomeReport.BackColor = System.Drawing.Color.Transparent;
            this.btnIncomeReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIncomeReport.FlatAppearance.BorderSize = 0;
            this.btnIncomeReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnIncomeReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncomeReport.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncomeReport.ForeColor = System.Drawing.Color.White;
            this.btnIncomeReport.Location = new System.Drawing.Point(-7, 113);
            this.btnIncomeReport.Name = "btnIncomeReport";
            this.btnIncomeReport.Size = new System.Drawing.Size(157, 41);
            this.btnIncomeReport.TabIndex = 11;
            this.btnIncomeReport.Text = "Income";
            this.btnIncomeReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIncomeReport.UseVisualStyleBackColor = false;
            this.btnIncomeReport.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnUnitReport
            // 
            this.btnUnitReport.BackColor = System.Drawing.Color.Transparent;
            this.btnUnitReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUnitReport.FlatAppearance.BorderSize = 0;
            this.btnUnitReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnUnitReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnitReport.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnitReport.ForeColor = System.Drawing.Color.White;
            this.btnUnitReport.Location = new System.Drawing.Point(-7, 60);
            this.btnUnitReport.Name = "btnUnitReport";
            this.btnUnitReport.Size = new System.Drawing.Size(157, 47);
            this.btnUnitReport.TabIndex = 10;
            this.btnUnitReport.Text = "Unit Info";
            this.btnUnitReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUnitReport.UseVisualStyleBackColor = false;
            this.btnUnitReport.Click += new System.EventHandler(this.button6_Click);
            // 
            // panel24
            // 
            this.panel24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(134)))), ((int)(((byte)(52)))));
            this.panel24.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.gradient;
            this.panel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel24.Controls.Add(this.panel7);
            this.panel24.Controls.Add(this.label16);
            this.panel24.Controls.Add(this.panel10);
            this.panel24.Controls.Add(this.btnReports);
            this.panel24.Controls.Add(this.btnIncomeServices);
            this.panel24.Controls.Add(this.btnTransactions);
            this.panel24.Controls.Add(this.btnIncome);
            this.panel24.Controls.Add(this.btnUnitInfo);
            this.panel24.Controls.Add(this.btnUnits);
            this.panel24.Location = new System.Drawing.Point(4, -19);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1053, 127);
            this.panel24.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.lblDateandTime);
            this.panel7.Location = new System.Drawing.Point(995, 49);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(145, 42);
            this.panel7.TabIndex = 11;
            // 
            // lblDateandTime
            // 
            this.lblDateandTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDateandTime.AutoSize = true;
            this.lblDateandTime.BackColor = System.Drawing.Color.Transparent;
            this.lblDateandTime.Font = new System.Drawing.Font("Eras Demi ITC", 20F);
            this.lblDateandTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDateandTime.Location = new System.Drawing.Point(10, 6);
            this.lblDateandTime.Name = "lblDateandTime";
            this.lblDateandTime.Size = new System.Drawing.Size(142, 31);
            this.lblDateandTime.TabIndex = 9;
            this.lblDateandTime.Text = "LOADING";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.LightGray;
            this.label16.Font = new System.Drawing.Font("Eras Demi ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(34, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 24);
            this.label16.TabIndex = 10;
            this.label16.Text = "VHHAI";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.pictureBox1);
            this.panel10.Location = new System.Drawing.Point(0, 19);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(150, 106);
            this.panel10.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantlogo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(18, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 80);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // btnReports
            // 
            this.btnReports.BackColor = System.Drawing.Color.Transparent;
            this.btnReports.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReports.FlatAppearance.BorderSize = 0;
            this.btnReports.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnReports.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReports.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReports.ForeColor = System.Drawing.SystemColors.Control;
            this.btnReports.Image = global::WindowsFormsApplication2.Properties.Resources.reports;
            this.btnReports.Location = new System.Drawing.Point(773, 19);
            this.btnReports.Name = "btnReports";
            this.btnReports.Size = new System.Drawing.Size(118, 119);
            this.btnReports.TabIndex = 6;
            this.btnReports.Text = "REPORTS";
            this.btnReports.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReports.UseVisualStyleBackColor = false;
            this.btnReports.Click += new System.EventHandler(this.button22_Click);
            // 
            // btnIncomeServices
            // 
            this.btnIncomeServices.BackColor = System.Drawing.Color.Transparent;
            this.btnIncomeServices.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIncomeServices.FlatAppearance.BorderSize = 0;
            this.btnIncomeServices.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnIncomeServices.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnIncomeServices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncomeServices.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncomeServices.ForeColor = System.Drawing.SystemColors.Control;
            this.btnIncomeServices.Image = global::WindowsFormsApplication2.Properties.Resources.income_services;
            this.btnIncomeServices.Location = new System.Drawing.Point(649, 19);
            this.btnIncomeServices.Name = "btnIncomeServices";
            this.btnIncomeServices.Size = new System.Drawing.Size(118, 119);
            this.btnIncomeServices.TabIndex = 5;
            this.btnIncomeServices.Text = "SERVICES";
            this.btnIncomeServices.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnIncomeServices.UseVisualStyleBackColor = false;
            this.btnIncomeServices.Click += new System.EventHandler(this.button20_Click);
            // 
            // btnTransactions
            // 
            this.btnTransactions.BackColor = System.Drawing.Color.Transparent;
            this.btnTransactions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTransactions.FlatAppearance.BorderSize = 0;
            this.btnTransactions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnTransactions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnTransactions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransactions.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransactions.ForeColor = System.Drawing.SystemColors.Control;
            this.btnTransactions.Image = global::WindowsFormsApplication2.Properties.Resources.transactions;
            this.btnTransactions.Location = new System.Drawing.Point(525, 19);
            this.btnTransactions.Name = "btnTransactions";
            this.btnTransactions.Size = new System.Drawing.Size(118, 119);
            this.btnTransactions.TabIndex = 4;
            this.btnTransactions.Text = "TRANSACTIONS";
            this.btnTransactions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnTransactions.UseVisualStyleBackColor = false;
            this.btnTransactions.Click += new System.EventHandler(this.button21_Click);
            // 
            // btnIncome
            // 
            this.btnIncome.BackColor = System.Drawing.Color.Transparent;
            this.btnIncome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIncome.FlatAppearance.BorderSize = 0;
            this.btnIncome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnIncome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnIncome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncome.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncome.ForeColor = System.Drawing.SystemColors.Control;
            this.btnIncome.Image = global::WindowsFormsApplication2.Properties.Resources.income_transaction;
            this.btnIncome.Location = new System.Drawing.Point(401, 19);
            this.btnIncome.Name = "btnIncome";
            this.btnIncome.Size = new System.Drawing.Size(118, 119);
            this.btnIncome.TabIndex = 3;
            this.btnIncome.Text = "INCOME TRANSACTIONS";
            this.btnIncome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnIncome.UseVisualStyleBackColor = false;
            this.btnIncome.Visible = false;
            this.btnIncome.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnUnitInfo
            // 
            this.btnUnitInfo.BackColor = System.Drawing.Color.Transparent;
            this.btnUnitInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUnitInfo.FlatAppearance.BorderSize = 0;
            this.btnUnitInfo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnUnitInfo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnUnitInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnitInfo.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnitInfo.ForeColor = System.Drawing.SystemColors.Control;
            this.btnUnitInfo.Image = global::WindowsFormsApplication2.Properties.Resources.Unit_information1;
            this.btnUnitInfo.Location = new System.Drawing.Point(277, 19);
            this.btnUnitInfo.Name = "btnUnitInfo";
            this.btnUnitInfo.Size = new System.Drawing.Size(118, 119);
            this.btnUnitInfo.TabIndex = 2;
            this.btnUnitInfo.Text = "UNIT INFORMATION";
            this.btnUnitInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnUnitInfo.UseVisualStyleBackColor = false;
            this.btnUnitInfo.Visible = false;
            this.btnUnitInfo.Click += new System.EventHandler(this.button19_Click);
            // 
            // btnUnits
            // 
            this.btnUnits.BackColor = System.Drawing.Color.Transparent;
            this.btnUnits.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUnits.FlatAppearance.BorderSize = 0;
            this.btnUnits.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(163)))), ((int)(((byte)(135)))));
            this.btnUnits.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(219)))), ((int)(((byte)(63)))));
            this.btnUnits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnits.Font = new System.Drawing.Font("Eras Demi ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnits.ForeColor = System.Drawing.SystemColors.Control;
            this.btnUnits.Image = global::WindowsFormsApplication2.Properties.Resources.Units;
            this.btnUnits.Location = new System.Drawing.Point(153, 19);
            this.btnUnits.Name = "btnUnits";
            this.btnUnits.Size = new System.Drawing.Size(118, 119);
            this.btnUnits.TabIndex = 1;
            this.btnUnits.Text = "UNITS";
            this.btnUnits.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnUnits.UseVisualStyleBackColor = false;
            this.btnUnits.Click += new System.EventHandler(this.button17_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detailsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(110, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // detailsToolStripMenuItem
            // 
            this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
            this.detailsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.detailsToolStripMenuItem.Text = "Details";
            this.detailsToolStripMenuItem.Click += new System.EventHandler(this.detailsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 558);
            this.ControlBox = false;
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.tabControl1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.Units.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.controlsPnl.ResumeLayout(false);
            this.UnitInfo.ResumeLayout(false);
            this.unitInfoBgPanel.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.IncomeTransaction.ResumeLayout(false);
            this.incTransBgPanel.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.pnlOthers.ResumeLayout(false);
            this.pnlOthers.PerformLayout();
            this.pnlTotals.ResumeLayout(false);
            this.pnlTotals.PerformLayout();
            this.pnlPerHead.ResumeLayout(false);
            this.pnlPerHead.PerformLayout();
            this.pnlReservation.ResumeLayout(false);
            this.pnlReservation.PerformLayout();
            this.pnlHOA.ResumeLayout(false);
            this.pnlHOA.PerformLayout();
            this.pnlQuantity.ResumeLayout(false);
            this.pnlQuantity.PerformLayout();
            this.pnlDates.ResumeLayout(false);
            this.pnlDates.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.ExpenseTransaction.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.IncomeServices.ResumeLayout(false);
            this.pnlIncomeItems.ResumeLayout(false);
            this.pnlIncomeItems.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.Reports.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Units;
        private System.Windows.Forms.TabPage UnitInfo;
        private System.Windows.Forms.TabPage IncomeTransaction;
        private System.Windows.Forms.Panel controlsPnl;
        private System.Windows.Forms.Button btnTransact;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtSearchUnits;
        private System.Windows.Forms.ListView MonthlyDueListView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.ComboBox cmbRented;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRenterName;
        private System.Windows.Forms.TextBox txtLot;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBlk;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbUnitType;
        private System.Windows.Forms.ComboBox cmbTurnOver;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblUnitID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOwnerName;
        private System.Windows.Forms.Label lblCivil;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPhoneNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button btnCancelUnitInfo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ListView IncomeTransactionsListView;
        private System.Windows.Forms.ComboBox cmbParticularIncome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtORNoIncome;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUnitIDIncome;
        private System.Windows.Forms.Label lblUnitIDTransaction;
        private System.Windows.Forms.Button btnCancelIncome;
        private System.Windows.Forms.TabPage ExpenseTransaction;
        private System.Windows.Forms.TabPage IncomeServices;
        private System.Windows.Forms.TabPage Reports;
        private System.Windows.Forms.Button btnCancelTransaction;
        private System.Windows.Forms.Button btnNewExpense;
        private System.Windows.Forms.ListView AllTransactionsListView;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.TextBox txtSearchAllTransactions;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioIncome;
        private System.Windows.Forms.ListView ServicesListView;
        private System.Windows.Forms.Button btnDelete_Item;
        private System.Windows.Forms.Button btnEdit_Item;
        private System.Windows.Forms.Button btnAdd_Item;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnReports;
        private System.Windows.Forms.Button btnIncomeServices;
        private System.Windows.Forms.Button btnTransactions;
        private System.Windows.Forms.Button btnIncome;
        private System.Windows.Forms.Button btnUnitInfo;
        private System.Windows.Forms.Button btnUnits;
        private System.Windows.Forms.Panel pnlTotals;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblAmountToPay;
        private System.Windows.Forms.TextBox txtChangeIncome;
        private System.Windows.Forms.TextBox txtAmountToPay;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtCheckNoIncome;
        private System.Windows.Forms.TextBox txtCashIncome;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCheckAmountIncome;
        private System.Windows.Forms.Panel pnlOthers;
        private System.Windows.Forms.TextBox txtSpecify;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel pnlPerHead;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtNoOfGuests;
        private System.Windows.Forms.TextBox txtNoOfResidents;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pnlReservation;
        private System.Windows.Forms.ComboBox cmbReservation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel pnlHOA;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPenalty;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel pnlQuantity;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pnlDates;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker todatePicker;
        private System.Windows.Forms.DateTimePicker fromdatePicker;
        public System.Windows.Forms.ListView unitInfoListView;
        private System.Windows.Forms.ColumnHeader UnitID;
        private System.Windows.Forms.ColumnHeader Blk;
        private System.Windows.Forms.ColumnHeader Lot;
        private System.Windows.Forms.ColumnHeader OwnerName;
        private System.Windows.Forms.ColumnHeader UnitType;
        private System.Windows.Forms.ColumnHeader TurnOver;
        private System.Windows.Forms.ColumnHeader Rented;
        private System.Windows.Forms.ColumnHeader RenterName;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtTotalAmountIncome;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.RadioButton radioUnitTransaction;
        private System.Windows.Forms.RadioButton radioAllTransaction;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.RadioButton radioExpenseServices;
        private System.Windows.Forms.RadioButton radioIncomeServices;
        private System.Windows.Forms.Button btnIncomeReport;
        private System.Windows.Forms.Button btnUnitReport;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.Label txtTo;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.Label txtFrom;
        private System.Windows.Forms.ColumnHeader UnitID_MonthlyDue;
        private System.Windows.Forms.ColumnHeader Year;
        private System.Windows.Forms.ColumnHeader January;
        private System.Windows.Forms.ColumnHeader February;
        private System.Windows.Forms.ColumnHeader March;
        private System.Windows.Forms.ColumnHeader April;
        private System.Windows.Forms.ColumnHeader May;
        private System.Windows.Forms.ColumnHeader June;
        private System.Windows.Forms.ColumnHeader July;
        private System.Windows.Forms.ColumnHeader August;
        private System.Windows.Forms.ColumnHeader September;
        private System.Windows.Forms.ColumnHeader October;
        private System.Windows.Forms.ColumnHeader November;
        private System.Windows.Forms.ColumnHeader December;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Label txtUnitID;
        private System.Windows.Forms.Panel unitInfoBgPanel;
        private System.Windows.Forms.Panel incTransBgPanel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DateTimePicker checkDatePicker;
        private System.Windows.Forms.Label lblDateandTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ColumnHeader MonthlyDue;
        private MyPanel panel2;
        private MyPanel panel3;
        private MyPanel panel4;
        private MyPanel panel8;
        private MyPanel panel15;
        private MyPanel panel16;
        private MyPanel panel18;
        private MyPanel panel19;
        private MyPanel panel20;
        private MyPanel pnlIncomeItems;
        private MyPanel panel24;
        private MyPanel panel10;
        private MyPanel panel13;
        private MyPanel panel14;
        private MyPanel panel22;
        private MyPanel panel23;
        private MyPanel panel25;
        private MyPanel panel26;
        private System.Windows.Forms.RadioButton radioParkingFee;
        private System.Windows.Forms.RadioButton radioMonthlyDue;
        private MyPanel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtSearchServices;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnAccountSettings;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnMonthlyDuesReport;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label labelUnitType;
        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnExpenseReport;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
    }
}

