﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        //THIS IS FOR THE REPORT VIEWER IN REPORTS PAGE
        private void Form2_Load(object sender, EventArgs e)
        {
            VerdantHeightsDataSetTableAdapters.MonthlyDuesTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.MonthlyDuesTableAdapter();
            VerdantHeightsDataSet.MonthlyDuesDataTable ds = new VerdantHeightsDataSet.MonthlyDuesDataTable();
            tbl.FillBy1(ds,Global.UnitID);

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "IndividualMD";
            reportDataSource1.Value = ds;

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../IndividualMD.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Landscape = true;
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);
            
            this.reportViewer1.RefreshReport();
        }
    }
}
