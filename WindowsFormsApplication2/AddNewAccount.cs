﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class AddNewAccount : Form
    {
        public AddNewAccount()
        {
            InitializeComponent();
            txtUsername.Focus();
        }


        //CREATES NEW ACCOUNT. COMPLETE WITH ALL THE VALIDATIONS.
        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text != "" && txtPassword.Text != "" && txtConfirmPassword.Text != "" && txtSecurityQuestion.Text != "" && txtAnswer_.Text != "")
            {
                if (txtPassword.Text != "" || txtConfirmPassword.Text != "")
                {
                    if (txtPassword.Text == txtConfirmPassword.Text)
                    {
                        if (txtPassword.TextLength >= 8)
                        {
                            var ds = Global.CheckPrimaryKey("*", "UserAccounts", "username", txtUsername.Text);
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                Global.Add_Account(txtUsername.Text, txtPassword.Text, txtSecurityQuestion.Text, txtAnswer_.Text);
                                MessageBox.Show("Account successfully created!");
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("User already exists");
                            }

                        }
                        else
                        {
                            MessageBox.Show("Password must be atleast 8 characters");
                            txtPassword.Focus();
                        }
                    }
                    else
                        MessageBox.Show("Passwords do not match");
                }
            }

            else
            {
                MessageBox.Show("Please complete all the fields");
            }
        }
        //CLOSES THE FORM
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //ON LOAD
        private void AddNewAccount_Load(object sender, EventArgs e)
        {
            txtUsername.Focus();
        }
        int show = 0, show1 = 0;
        //SHOWS/HIDES THE CHARACTERS IN THE CONFIRM PASSWORD TEXTBOX
        private void lblShow1_Click(object sender, EventArgs e)
        {
            if (show1 == 0)
            {
                txtConfirmPassword.PasswordChar = '\0';
                lblShow.Text = "Hide";
                show1 = 1;
            }
            else
            {
                txtConfirmPassword.PasswordChar = '*';
                lblShow.Text = "Show";
                show1 = 0;
            }
        }
        //SHOWS/HIDES THE CHARACTERS IN THE PASSWORD TEXTBOX
        private void lblShow_Click(object sender, EventArgs e)
        {
            if (show == 0)
            {
                txtPassword.PasswordChar = '\0';
                lblShow.Text = "Hide";
                show = 1;
            }
            else
            {
                txtPassword.PasswordChar = '*';
                lblShow.Text = "Show";
                show = 0;
            }
        }
    }
}
