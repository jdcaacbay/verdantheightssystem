﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    //ALL THE NEEDED VARIABLES ARE DECLARED
    public partial class Form1 : Form
    {
        double total_res = 0, total_guest = 0;
        private int save, UnitInfoOpened = 0, IncomeTransactionOpened = 0;
        private int to, rented;
        private string renterName;
        String particular;
        int iecounter = 0;
        double months;
        double price = 0;
        int incometrans = 0;
        int monthlypayment = 0;
        int allOrIncome = 0;
        String transtype;
    //ALL THE INITIALIZATIONS NEEDED IN THE START
        public Form1()
        {
            InitializeComponent();

            unitInfoListView.ListViewItemSorter = new Sorter();
            IncomeTransactionsListView.ListViewItemSorter = new Sorter();
            AllTransactionsListView.ListViewItemSorter = new Sorter();
            ServicesListView.ListViewItemSorter = new Sorter();


            tabControl1.TabPages.Remove(UnitInfo);
            tabControl1.TabPages.Remove(IncomeTransaction);

            txtUnitID.Text = String.Empty;
            txtUnitID.Visible = false;
            lblUnitID.Visible = false;
        }
        //SAVE BUTTON IN UNIT INFO PAGE. - FOR SAVE,EDIT,AND VIEW
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (save == 1)
            {
                if (validateUnitInfo())
                {
                    getDatafromFields();
                    var ds = Global.CheckPrimaryKey("*", "UnitInfo", "UnitID", txtUnitID.Text);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        Global.Add_Record(txtUnitID.Text, txtBlk.Text, txtLot.Text, txtOwnerName.Text, cmbUnitType.Text, to, rented, renterName, txtMobileNo.Text, txtPhoneNo.Text, txtEmail.Text);
                        String year = DateTime.Now.Year.ToString();
                        Global.Add_MonthlyDue(txtUnitID.Text, year);
                        Global.Add_ParkingFee(txtUnitID.Text, year);
                        MessageBox.Show("Unit added successfully");
                        tabControl1.TabPages.Remove(UnitInfo);
                        tabControl1.SelectedTab = Units;
                        Reload();
                        closeUnitInfoTab();
                    }
                    else
                    {
                        MessageBox.Show("Unit already exists");
                    }
                }
                else
                    MessageBox.Show("Please check fields");
            }
            else if (save == 2)
            {
                if (validateUnitInfo())
                {
                    getDatafromFields();
                    Global.Edit_Record(txtUnitID.Text, txtBlk.Text, txtLot.Text, txtOwnerName.Text, cmbUnitType.Text, to, rented, renterName, txtMobileNo.Text, txtPhoneNo.Text, txtEmail.Text);
                    MessageBox.Show("Unit information edited successfully");
                    String year = DateTime.Now.Year.ToString();
                    tabControl1.TabPages.Remove(UnitInfo);
                    tabControl1.SelectedTab = Units;
                    Reload();
                    closeUnitInfoTab();
                }
                else
                    MessageBox.Show("Please check fields");
            }
            else
            {
                Global.UnitID = txtUnitID.Text;
                Form2 f = new Form2();
                f.ShowDialog();
            }

        }
        //FUNCTION FOR VALIDATING THE UNITINFO PAGE BEFORE ADDING,EDITING UNITS
        private bool validateUnitInfo()
        {
            if (txtBlk.Text == "" || txtLot.Text == "" || cmbUnitType.SelectedIndex == -1 || cmbTurnOver.SelectedIndex == -1 || txtOwnerName.Text == "" ||txtMobileNo.Text ==""||txtPhoneNo.Text==""||txtEmail.Text==""||cmbRented.SelectedIndex==-1)
            {
                return false;
            }
            else
            {
                if(cmbRented.SelectedIndex==0)
                {
                    if (txtRenterName.Text == "")
                        return false;
                }
                return true;
            }
        }
        //FUNCTION FOR VALIDATNG THE INCOME TRANSACTION PAGE
        private bool validateIncomeTransaction1()
        {
            if (txtORNoIncome.Text==""||txtAmountToPay.Text=="")
            {
                return false;
            }
            else
            {
                return true;

            }
        }
        //FUNCTION FOR GETTING THE RIGHT DATA FOR THE FIELDS IN UNIT INFO
        private void getDatafromFields()
        {
            if (cmbTurnOver.Text == "Yes")
                to = 1;
            else
                to = 0;
            if (cmbRented.Text == "Yes")
                rented = 1;
            else
                rented = 0;

            if (string.IsNullOrWhiteSpace(txtRenterName.Text))
                renterName = "NA";
            else
                renterName = txtRenterName.Text;

            txtUnitID.Text = cmbUnitType.Text + txtBlk.Text + txtLot.Text;
        }
        //ON LOAD OF THE FORM
        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
            btnReports.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
            btnTransactions.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);
            btnIncomeServices.Location = new Point(btnIncome.Location.X, btnIncome.Location.Y);
            btnUnits.BackColor = Color.FromArgb(115, 163, 135);

            Load_All();
            this.reportViewer1.RefreshReport();
        }
        //FUNCTION FOR LOADING ALL THE NEEDED DATA FOR THE WHOLE SYSTEM
        private void Load_All()
        {
            //FOR TRANSACTIONS TAB
            LoadIncomeView();
            radioIncome.Checked = true;
            btnNewExpense.Enabled = false;
            //FOR INCOME ITEMS
            Global.typeofservice = 1;
            ServicesListView.Clear();
            IncomeServicesInitializeView();
            DataTable dt = Global.Query2("*", "IncomeServices");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["Code"].ToString());
                listitem.SubItems.Add(dr["Name"].ToString());
                listitem.SubItems.Add(dr["Price"].ToString());
                listitem.SubItems.Add(dr["TransactionType"].ToString());
                ServicesListView.Items.Add(listitem);
            }
            Reload();

            //TOTALS
            dt = Global.Query2("SUM(Price) AS 'Total'", "IncomeLogs");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                txtTotal.Text = dr["Total"].ToString();

            }
        }
        //RELOADS THE UNITS LISTVIEW
        private void Reload()
        {
            unitInfoListView.Items.Clear();
            unitInfoListView.View = View.Details;
            UnitInitializeView();
            var dt = new DataTable();
            dt = Global.Query2("*", "UnitInfo");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["UnitID"].ToString());
                listitem.SubItems.Add(dr["Blk"].ToString());
                listitem.SubItems.Add(dr["Lot"].ToString());
                listitem.SubItems.Add(dr["OwnerName"].ToString());
                listitem.SubItems.Add(dr["UnitType"].ToString());
                if (dr["TurnOver"].ToString() == "True")
                    listitem.SubItems.Add("Yes".ToString());
                else
                    listitem.SubItems.Add("No".ToString());
                if (dr["Rented"].ToString() == "True")
                    listitem.SubItems.Add("Yes".ToString());
                else
                    listitem.SubItems.Add("No".ToString());
                listitem.SubItems.Add(dr["RenterName"].ToString());
                DateTime today = DateTime.Today;
                int month = today.Month;
                String month_ = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                int year = today.Year;
                var dt1 = new DataTable();
                dt1 = Global.Query2(month_, "MonthlyDue WHERE UnitID='" + dr["UnitID"].ToString() + "' AND Year=" + year);
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    DataRow dr1 = dt1.Rows[j];
                    String paid = dr1[month_].ToString();

                    if (paid != "")
                    {
                        listitem.SubItems.Add("Paid");
                        listitem.SubItems[8].BackColor = Color.FromArgb(76, 255, 130);
                        listitem.UseItemStyleForSubItems = false;
                    }
                    else
                    {
                        listitem.SubItems.Add("Unpaid");
                        listitem.SubItems[8].BackColor = Color.FromArgb(247, 122, 111);
                        listitem.UseItemStyleForSubItems = false;
                    }
                }


                unitInfoListView.Items.Add(listitem);
            }

        }
        //FUNCTION FOR THE SEARCHING IN THE UNITS LISTVIEW
        private void SearchUnits(DataTable dt)
        {
   
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["UnitID"].ToString());
                listitem.SubItems.Add(dr["Blk"].ToString());
                listitem.SubItems.Add(dr["Lot"].ToString());
                listitem.SubItems.Add(dr["OwnerName"].ToString());
                listitem.SubItems.Add(dr["UnitType"].ToString());
                if (dr["TurnOver"].ToString() == "True")
                    listitem.SubItems.Add("Yes".ToString());
                else
                    listitem.SubItems.Add("No".ToString());
                if (dr["Rented"].ToString() == "True")
                    listitem.SubItems.Add("Yes".ToString());
                else
                    listitem.SubItems.Add("No".ToString());
                listitem.SubItems.Add(dr["RenterName"].ToString());
                DateTime today = DateTime.Today;
                int month = today.Month;
                String month_ = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                int year = today.Year;
                var dt1 = new DataTable();
                dt1 = Global.Query2(month_, "MonthlyDue WHERE UnitID='" + dr["UnitID"].ToString() + "' AND Year=" + year);
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    DataRow dr1 = dt1.Rows[j];
                    String paid = dr1[month_].ToString();

                    if (paid != "")
                    {
                        listitem.SubItems.Add("Paid");
                        listitem.SubItems[8].BackColor = Color.FromArgb(76, 255, 130);
                        listitem.UseItemStyleForSubItems = false;
                    }
                    else
                    {
                        listitem.SubItems.Add("Unpaid");
                        listitem.SubItems[8].BackColor = Color.FromArgb(247, 122, 111);
                        listitem.UseItemStyleForSubItems = false;
                    }
                }


                unitInfoListView.Items.Add(listitem);
            }
        }
        //FUNCTION FOR THE RELOADING OF THE INCOME SERVICES LIST VIEW
        private void ReloadIncomeServices()
        {
            Global.typeofservice = 1;
                ColumnClickEventArgs args = new ColumnClickEventArgs(0);
                ServicesListView_ColumnClick(this, args);
                args = new ColumnClickEventArgs(0);
                ServicesListView_ColumnClick(this, args);
            ServicesListView.Clear();
            IncomeServicesInitializeView();
            DataTable dt = Global.Query2("*", "IncomeServices");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["Code"].ToString());
                listitem.SubItems.Add(dr["Name"].ToString());
                listitem.SubItems.Add(dr["Price"].ToString());
                listitem.SubItems.Add(dr["TransactionType"].ToString());
                ServicesListView.Items.Add(listitem);
            }
        }
         //FUNCTION FOR THE RELOADING OF THE EXPENSE SERVICES LIST VIEW
        private void ReloadExpenseServices()
        {
            Global.typeofservice = 2;
                ColumnClickEventArgs args = new ColumnClickEventArgs(0);
                ServicesListView_ColumnClick(this, args);
                args = new ColumnClickEventArgs(0);
                ServicesListView_ColumnClick(this, args);
            ServicesListView.Clear();
            ExpenseServicesInitializeView();
            DataTable dt = Global.Query2("*", "ExpenseServices");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["Code"].ToString());
                listitem.SubItems.Add(dr["Name"].ToString());
                ServicesListView.Items.Add(listitem);
            }
        }
        //FUNCTION FOR THE SEARCHING IN THE SERVICES LISTVIEW
        private void SearchServices(DataTable dt)
        {
            if (Global.typeofservice == 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["Code"].ToString());
                    listitem.SubItems.Add(dr["Name"].ToString());
                    listitem.SubItems.Add(dr["Price"].ToString());
                    listitem.SubItems.Add(dr["TransactionType"].ToString());
                    ServicesListView.Items.Add(listitem);
                }
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["Code"].ToString());
                    listitem.SubItems.Add(dr["Name"].ToString());
                    ServicesListView.Items.Add(listitem);
                }
            }
        }
        //INITIALIZATION FOR THE INCOME SERVICES LIST VIEW IN SERVICES LISTVIEW
        private void IncomeServicesInitializeView()
        {
            ServicesListView.Columns.Add("Code", 200, HorizontalAlignment.Left);
            ServicesListView.Columns.Add("Name", 200, HorizontalAlignment.Left);
            ServicesListView.Columns.Add("Price", 200, HorizontalAlignment.Left);
            ServicesListView.Columns.Add("Transaction Type", 200, HorizontalAlignment.Left);
        }
        //INITIALIZATION FOR THE EXPENSE SERVICES LIST VIEW IN SERVICES LISTVIEW
        private void ExpenseServicesInitializeView()
        {
            ServicesListView.Columns.Add("Code", 250, HorizontalAlignment.Left);
            ServicesListView.Columns.Add("Name", 250, HorizontalAlignment.Left);
            
        }
        //INITIALIZATION FOR INCOME TRANSACTION LIST VIEW
        private void IncomeTransactionInitializeView()
        {
            AllTransactionsListView.Columns.Add("OR#", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Date", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Unit ID", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Owner's Name", 120, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Code", 60, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Particular", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Amount to Pay", 120, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Mode of Payment", 120, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Amount Paid", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Remarks", 150, HorizontalAlignment.Left);

        }
        //INITIALIZATION FOR THE UNITS LISTVIEW
        private void UnitInitializeView()
        {
            UnitID.Width = 100;
            Blk.Width = 70;
            Lot.Width = 70;
            OwnerName.Width = 200;
            UnitType.Width = 100;
            TurnOver.Width = 100;
            Rented.Width = 100;
            RenterName.Width = 200;
            MonthlyDue.Width = 100;
        }
        //NAVIGATION BUTTONS FOR THE WHOLE SYSTEM
        private void button17_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = Units;
            btnUnits.BackColor = Color.FromArgb(115, 163, 135);
          
            btnIncome.BackColor = Color.Transparent;
            btnUnitInfo.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;
        }
        private void button19_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = UnitInfo;
            btnUnitInfo.BackColor = Color.FromArgb(115, 163, 135);
          
            btnUnits.BackColor = Color.Transparent;
            btnIncome.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;
        }
        private void button18_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = IncomeTransaction;
            btnIncome.BackColor = Color.FromArgb(115, 163, 135);
          
            btnUnits.BackColor = Color.Transparent;
            btnUnitInfo.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;

        }
        private void button21_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = ExpenseTransaction;
            btnTransactions.BackColor = Color.FromArgb(115, 163, 135);
          
            btnUnits.BackColor = Color.Transparent;
            btnIncome.BackColor = Color.Transparent;
            btnUnitInfo.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;
        }

        private void button20_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = IncomeServices;
            btnIncomeServices.BackColor = Color.FromArgb(115, 163, 135);
          
            btnUnits.BackColor = Color.Transparent;
            btnIncome.BackColor = Color.Transparent;
            btnUnitInfo.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;
        }

        private void button22_Click(object sender, EventArgs e)
        {
         
            txtYear.Text = DateTime.Now.Year.ToString();

            tabControl1.SelectedTab = Reports;
            btnReports.BackColor = Color.FromArgb(115, 163, 135);
          
            btnUnits.BackColor = Color.Transparent;
            btnIncome.BackColor = Color.Transparent;
            btnUnitInfo.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
        }

        //SHOWS UNITINFO PAGE
        private void showUnitInfoTab()
        {
            if (UnitInfoOpened != 1)
            {
                if (IncomeTransactionOpened == 0)
                {
                    btnUnitInfo.Visible = true;
                    btnTransactions.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                    btnIncomeServices.Location = new Point(btnReports.Location.X, btnReports.Location.Y);
                    btnReports.Location = new Point(btnReports.Location.X + 124, btnReports.Location.Y);
                    btnIncome.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);

                }
                else
                {

                    btnUnitInfo.Visible = true;
                    btnIncome.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
                    btnTransactions.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                    btnIncomeServices.Location = new Point(btnReports.Location.X, btnReports.Location.Y);
                    btnReports.Location = new Point(btnReports.Location.X + 124, btnReports.Location.Y);
                }
            }

            //COLOR OF THE BUTTON
            btnUnitInfo.BackColor = Color.FromArgb(115, 163, 135);
           
            btnUnits.BackColor = Color.Transparent;
            btnIncome.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;

            UnitInfoOpened = 1;
        }
        //CLOSES UNIT INFO PAGE
        private void closeUnitInfoTab()
        {
            if (IncomeTransactionOpened == 0)
            {
                btnUnitInfo.Visible = false;
                btnReports.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                btnIncomeServices.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
                btnTransactions.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);
            }
            else
            {
                btnUnitInfo.Visible = false;
                btnReports.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                btnIncomeServices.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
                btnTransactions.Location = new Point(btnIncome.Location.X, btnIncome.Location.Y);
                btnIncome.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);
            }
            //COLOR OF THE BUTTON
            btnUnitInfo.BackColor = Color.Transparent;
          
            btnUnits.BackColor = Color.FromArgb(115, 163, 135);
            btnIncome.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;


            UnitInfoOpened = 0;
        }
        //EDIT/UPDATES THE UNIT INFO
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (unitInfoListView.SelectedItems.Count != 0)
            {
                tabControl1.TabPages.Remove(UnitInfo);
                tabControl1.TabPages.Insert(2, UnitInfo);

                showUnitInfoTab();
                
                save = 2;
                btnSave.Text = "Save";
                ListViewItem item = unitInfoListView.SelectedItems[0];
                lblUnitID.Visible = true;
                txtUnitID.Visible = true;
                txtUnitID.Text = item.Text;
                tabControl1.SelectedTab = UnitInfo;
                Global.Enable(UnitInfo);
                Global.Clear(UnitInfo, 2);
                Global.RemoveReadOnly(UnitInfo);
                radioMonthlyDue.Checked = true;
                txtBlk.Enabled = false;
                txtLot.Enabled = false;
                cmbUnitType.Enabled = false;
                SqlConnection con = new SqlConnection(Global.connectionString);
                SqlCommand command = new SqlCommand("select * from UnitInfo WHERE UnitID='" + txtUnitID.Text + "'", con);
                con.Open();
                SqlDataReader read = command.ExecuteReader();
                while (read.Read())
                {
                    txtOwnerName.Text = (read["OwnerName"].ToString());
                    txtBlk.Text = (read["Blk"].ToString());
                    txtLot.Text = (read["Lot"].ToString());
                    if ((read["UnitType"].ToString() == "SU"))
                        cmbUnitType.SelectedIndex = 0;
                    else
                        cmbUnitType.SelectedIndex = 1;
                    if ((read["TurnOver"].ToString() == "True"))
                        cmbTurnOver.SelectedIndex = 0;
                    else
                        cmbTurnOver.SelectedIndex = 1;
                    if ((read["Rented"].ToString() == "True"))
                        cmbRented.SelectedIndex = 0;
                    else
                        cmbRented.SelectedIndex = 1;
                    if ((read["RenterName"].ToString() == "NA"))
                        txtRenterName.Clear();
                    else
                        txtRenterName.Text = (read["RenterName"].ToString());

                    txtMobileNo.Text = (read["MobileNo"].ToString());
                    txtPhoneNo.Text = (read["PhoneNo"].ToString());
                    txtEmail.Text = (read["Email"].ToString());


                }
                read.Close();
                con.Close();

                ReloadMonthlyView();
            }
            else
                MessageBox.Show("Select from the list");
        }
        //RELOADS THE MONTHLY DUE VIEW
        private void ReloadMonthlyView()
        {
            MonthlyDueListView.Items.Clear();
            unitInfoListView.View = View.Details;
            UnitInitializeView();
            var dt = new DataTable();
            dt = Global.Query2("*", "MonthlyDue WHERE UnitID='" + txtUnitID.Text + "'");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["UnitID"].ToString());
                listitem.SubItems.Add(dr["Year"].ToString());
                if (dr["January"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[2].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["January"].ToString());
                if (dr["February"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[3].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["February"].ToString());
                if (dr["March"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[4].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["March"].ToString());
                if (dr["April"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[5].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["April"].ToString());
                if (dr["May"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[6].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["May"].ToString());
                if (dr["June"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[7].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["June"].ToString());
                if (dr["July"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[8].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["July"].ToString());
                if (dr["August"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[9].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["August"].ToString());

                if (dr["September"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[10].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["September"].ToString());
                if (dr["October"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[11].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["October"].ToString());
                if (dr["November"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[12].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["November"].ToString());
                if (dr["December"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[13].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["December"].ToString());

                MonthlyDueListView.Items.Add(listitem);
            }
        }
        //RELOADS THE PARKING FEE LISTVIEW
        private void ReloadParkingFee()
        {
            MonthlyDueListView.Items.Clear();
            MonthlyDueListView.View = View.Details;
   
            var dt = new DataTable();
            dt = Global.Query2("*", "ParkingFee WHERE UnitID='" + txtUnitID.Text + "'");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["UnitID"].ToString());
                listitem.SubItems.Add(dr["Year"].ToString());
                if (dr["January"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[2].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["January"].ToString());
                if (dr["February"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[3].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["February"].ToString());
                if (dr["March"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[4].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["March"].ToString());
                if (dr["April"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[5].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["April"].ToString());
                if (dr["May"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[6].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["May"].ToString());
                if (dr["June"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[7].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["June"].ToString());
                if (dr["July"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[8].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["July"].ToString());
                if (dr["August"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[9].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["August"].ToString());

                if (dr["September"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[10].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["September"].ToString());
                if (dr["October"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[11].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["October"].ToString());
                if (dr["November"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[12].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["November"].ToString());
                if (dr["December"].ToString() == "")
                {
                    listitem.SubItems.Add(" ");
                    listitem.SubItems[13].BackColor = Color.FromArgb(192, 192, 192);
                    listitem.UseItemStyleForSubItems = false;
                }
                else
                    listitem.SubItems.Add(dr["December"].ToString());

                MonthlyDueListView.Items.Add(listitem);
            }
        }
        //DELETE BUTTON
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (unitInfoListView.SelectedItems.Count != 0)
            {

                DialogResult dialogResult = MessageBox.Show("Do you really want to delete this record?", "Warning", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ListViewItem item = unitInfoListView.SelectedItems[0];
                    var unitID = item.Text;
                    Global.Delete_Unit(unitID, "UnitInfo");
                    Global.Delete_Unit(unitID, "MonthlyDue");
                    Global.Delete_Unit(unitID, "ParkingFee");
                    Global.Delete_Unit(unitID, "IncomeLogs");
                    MessageBox.Show("Record was successfully deleted");
                    Load_All();     
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }

            }
            else
                MessageBox.Show("Select from the list");
        }
        //VIEW BUTTON
        private void btnView_Click(object sender, EventArgs e)
        {
            if (unitInfoListView.SelectedItems.Count != 0)
            {
                tabControl1.TabPages.Remove(UnitInfo);
                tabControl1.TabPages.Insert(2, UnitInfo);

                showUnitInfoTab();

                save = 3;

                ListViewItem item = unitInfoListView.SelectedItems[0];
                lblUnitID.Visible = true;
                txtUnitID.Visible = true;
                txtUnitID.Text = item.Text;
                tabControl1.SelectedTab = UnitInfo;
                Global.Enable(UnitInfo);
                Global.Clear(UnitInfo, 2);
                Global.ReadOnly(UnitInfo);
                MonthlyDueListView.Enabled = true;
                radioMonthlyDue.Checked = true;
              
                UnitInfo.Text = "View Unit Information";
                btnSave.Text = "Print";
                ReloadMonthlyView();
                SqlConnection con = new SqlConnection(Global.connectionString);
                SqlCommand command = new SqlCommand("select * from UnitInfo WHERE UnitID='" + txtUnitID.Text + "'", con);
                con.Open();
                SqlDataReader read = command.ExecuteReader();
                while (read.Read())
                {
                    txtOwnerName.Text = (read["OwnerName"].ToString());
                    txtBlk.Text = (read["Blk"].ToString());
                    txtLot.Text = (read["Lot"].ToString());
                    if ((read["UnitType"].ToString() == "SU"))
                        cmbUnitType.SelectedIndex = 0;
                    else
                        cmbUnitType.SelectedIndex = 1;
                    if ((read["TurnOver"].ToString() == "True"))
                        cmbTurnOver.SelectedIndex = 0;
                    else
                        cmbTurnOver.SelectedIndex = 1;
                    if ((read["Rented"].ToString() == "True"))
                        cmbRented.SelectedIndex = 0;
                    else
                        cmbRented.SelectedIndex = 1;
                    if ((read["RenterName"].ToString() == "NA"))
                        txtRenterName.Clear();
                    else
                        txtRenterName.Text = (read["RenterName"].ToString());
                    txtMobileNo.Text = (read["MobileNo"].ToString());
                    txtPhoneNo.Text = (read["PhoneNo"].ToString());
                    txtEmail.Text = (read["Email"].ToString());

                }
                read.Close();
                con.Close();
                ReloadMonthlyView();
            }
            else
                MessageBox.Show("Select from the list");
        }
        //TRANSACT BUTTON
        private void btnTransact_Click(object sender, EventArgs e)
        {
            Global.Clear(IncomeTransaction, 2);
            String unitTypeIncome = "";

            if (unitInfoListView.SelectedItems.Count != 0)
            {

                showIncomeTransactionTab();
                Global.Clear(IncomeTransaction, 2);
                cmbParticularIncome.Enabled = true;
                btnClear.Enabled = true;
                tabControl1.TabPages.Remove(IncomeTransaction);
                tabControl1.TabPages.Insert(2, IncomeTransaction);
                tabControl1.SelectedTab = IncomeTransaction;
                ListViewItem item = unitInfoListView.SelectedItems[0];
                txtUnitIDIncome.Text = item.Text;
                Global.UnitID = txtUnitIDIncome.Text;
                DateTime today = DateTime.Today;
                txtDate.ReadOnly = true;
                txtDate.Text = today.ToString("MM/dd/yyy");
                DataTable dt1 = Global.Query2("*", "UnitInfo Where UnitID='" + txtUnitIDIncome.Text + "'");
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataRow dr = dt1.Rows[i];
                    unitTypeIncome = dr["UnitType"].ToString();
                }
                txtORNoIncome.Enabled = true;
                cmbParticularIncome.Items.Clear();
                DataTable dt = Global.Query2("*", "IncomeServices");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    cmbParticularIncome.Items.Add(dr["Name"].ToString());
                }

                DataTable dt2 = Global.Query2("*", "IncomeServices WHERE Name LIKE 'Swimming Pool%'");
                if (dt2.Rows.Count != 0)
                {
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        DataRow dr = dt2.Rows[i];
                        cmbParticularIncome.Items.Remove(dr["Name"].ToString());
                    }
                    cmbParticularIncome.Items.Insert(2, "Swimming Pool");
                }

                dt2 = Global.Query2("*", "IncomeServices WHERE TransactionType = 'Discount/Penalty'");
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow dr = dt2.Rows[i];
                    cmbParticularIncome.Items.Remove(dr["Name"].ToString());
                }

                dt2 = Global.Query2("*", "IncomeServices WHERE Name LIKE 'Sticker Fee (S%' OR Name LIKE 'Sticker Fee (F%'");
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow dr = dt2.Rows[i];
                    cmbParticularIncome.Items.Remove(dr["Name"].ToString());
                }

                dt2 = Global.Query2("*", "IncomeLogs WHERE UnitID = '" + txtUnitIDIncome.Text + "' AND Code='011'");
                if (dt2.Rows.Count != 0)
                {
                    cmbParticularIncome.Items.Insert(8, "Sticker Fee (Succeeding)");
                }
                else
                    cmbParticularIncome.Items.Insert(8, "Sticker Fee (First)");

                if (unitTypeIncome == "SU")
                    cmbParticularIncome.Items.Remove("HOA Due (TH)");
                if (unitTypeIncome == "TH")
                    cmbParticularIncome.Items.Remove("HOA Due (SU)");

                txtORNoIncome.Focus();
                cmbParticularIncome.Enabled = true;
                radioUnitTransaction.Checked = true;
                radioAllTransaction.Enabled = true;
                radioUnitTransaction.Enabled = true;

                Reload_IncomeTransactions(2);

            }
            else
                MessageBox.Show("Select from the list");

        }
        //SHOWS THE INCOME TRANSACTION TAB
        private void showIncomeTransactionTab()
        {
            if (IncomeTransactionOpened != 1)
            {
                if (UnitInfoOpened == 0)
                {

                    btnIncome.Visible = true;
                    btnTransactions.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                    btnIncomeServices.Location = new Point(btnReports.Location.X, btnReports.Location.Y);
                    btnReports.Location = new Point(btnReports.Location.X + 124, btnReports.Location.Y);
                    btnIncome.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);

                }
                else
                {
                    btnIncome.Visible = true;
                    btnIncome.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);
                    btnUnitInfo.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
                    btnTransactions.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                    btnIncomeServices.Location = new Point(btnReports.Location.X, btnReports.Location.Y);
                    btnReports.Location = new Point(btnReports.Location.X + 124, btnReports.Location.Y);

                }
            }

            //COLOR OF THE BUTTON
            btnUnitInfo.BackColor = Color.Transparent;
          
            btnUnits.BackColor = Color.Transparent;
            btnIncome.BackColor = Color.FromArgb(115, 163, 135);
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;

            IncomeTransactionOpened = 1;
        }
        //CLOSES THE INCOME TRANSACTION TAB
        private void closeIncomeTransactionTab()
        {
            if (UnitInfoOpened == 0)
            {

                btnIncome.Visible = false;
                btnReports.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                btnIncomeServices.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
                btnTransactions.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);
            }
            else
            {
                btnIncome.Visible = false;
                btnReports.Location = new Point(btnIncomeServices.Location.X, btnIncomeServices.Location.Y);
                btnIncomeServices.Location = new Point(btnTransactions.Location.X, btnTransactions.Location.Y);
                btnTransactions.Location = new Point(btnUnitInfo.Location.X, btnUnitInfo.Location.Y);
                btnUnitInfo.Location = new Point(btnIncome.Location.X, btnIncome.Location.Y);

            }
            //COLOR OF THE BUTTON
            btnUnitInfo.BackColor = Color.Transparent;
         
            btnUnits.BackColor = Color.FromArgb(115, 163, 135);
            btnIncome.BackColor = Color.Transparent;
            btnIncomeServices.BackColor = Color.Transparent;
            btnTransactions.BackColor = Color.Transparent;
            btnReports.BackColor = Color.Transparent;


            IncomeTransactionOpened = 0;
        }
        //RELOADS THE INCOME TRANSACTION LIST VIEW IN INCOME TRANS PAGE
        private void Reload_IncomeTransactions(int a)
        {
            
            IncomeTransactionsListView.Clear();
            IncomeTransactionsListView.View = View.Details;
            TransactionInitializeView();
            if (a == 1)
            {
                DataTable dt = Global.Query2("*", "IncomeLogs");
                Reload_Transactions(dt);
            }
            else
            {
                DataTable dt = Global.Query2("*", "IncomeLogs WHERE UnitID='" + txtUnitIDIncome.Text + "'");
                Reload_Transactions(dt);
            }

        }
        //RELOAD TRANSACTIONS IN INCOME TRANSACTION TAB
        private void Reload_Transactions(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["ORnum"].ToString());
                listitem.SubItems.Add(dr["CurrDate"].ToString());
                listitem.SubItems.Add(dr["UnitID"].ToString());
                DataTable dt1 = Global.Query2("*", "UnitInfo WHERE UnitID='" + dr["UnitID"].ToString() + "'");
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    DataRow dr1 = dt1.Rows[j];
                    listitem.SubItems.Add(dr1["OwnerName"].ToString());
                }
                listitem.SubItems.Add(dr["Code"].ToString());
                listitem.SubItems.Add(dr["Particular"].ToString());
                if (double.Parse(dr["Price"].ToString()) == 0)
                {
                    listitem.SubItems.Add("Cancelled");
                    listitem.SubItems[0].BackColor = Color.FromArgb(247, 122, 111);

                }
                else
                {
                    listitem.SubItems.Add(dr["Price"].ToString());
                }

                if(dr["Cash"].ToString()!="0.00"&& dr["Checknum"].ToString() == "NA" && dr["CheckAmount"].ToString() == "0.00")
                {
                    listitem.SubItems.Add("Cash");
                }
                if(dr["Checknum"].ToString()!="NA"&&dr["CheckAmount"].ToString()!="0.00"&&dr["Cash"].ToString()=="0.00")
                {
                    listitem.SubItems.Add("Check");
                }
                if (dr["Checknum"].ToString() != "NA" && dr["CheckAmount"].ToString() != "0.00" && dr["Cash"].ToString() != "0.00")
                {
                    listitem.SubItems.Add("Cash/Check");
                }
                listitem.SubItems.Add(dr["TotalAmount"].ToString());
                listitem.SubItems.Add(dr["Remarks"].ToString());
                IncomeTransactionsListView.Items.Add(listitem);

            }
        }
        //CLOSES THE UNITINFO TAB
        private void btnCancelUnitInfo_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = Units;
            tabControl1.TabPages.Remove(UnitInfo);
            Global.Clear(IncomeTransaction,2);
            closeUnitInfoTab();

        }
        //LOADS THE NEEDED DATA AND PANELS IN INCOME TRANSACTION
        private void cmbParticularIncome_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbParticularIncome.SelectedIndex != -1)
            {
                DataTable dt = Global.Query2("*", "IncomeServices Where Name LIKE '" + cmbParticularIncome.Text + "%'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    transtype = dr["TransactionType"].ToString();
                    if (transtype != "Others")
                        price = double.Parse(dr["Price"].ToString());
                    Global.code = dr["Code"].ToString();
                }

                var temp = txtORNoIncome.Text;
                Global.Clear(IncomeTransaction, 1);
                txtORNoIncome.Text = temp;
                DateTime today = DateTime.Today;
                txtDate.ReadOnly = true;
                txtDate.Text = today.ToString("MM/dd/yyy");
                txtUnitIDIncome.Text = Global.UnitID;

                DisableKeyFields();
                if (transtype == "Monthly Due") //HOA DUE
                {
                   
                    DisablePanels();
                    pnlDates.Enabled = true;
                    pnlDates.BackColor = Color.FromArgb(76, 255, 130);
                    pnlHOA.Enabled = true;
                    pnlHOA.BackColor = Color.FromArgb(76, 255, 130);
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    DisableKeyFields();
                    fromdatePicker.Focus();
                    pnlDates.TabIndex = 1;
                    pnlHOA.TabIndex = 2;
                    pnlTotals.TabIndex = 3;
                    txtORNoIncome.TabIndex = 4;
                    cmbParticularIncome.TabIndex = 5;

                    txtDiscount.Enabled = false;
                    txtPenalty.Enabled = false;
                  
                    GetPriceHOA();


                }
                if (transtype == "Swimming Pool") //POOL
                {
                    DisablePanels();
                    pnlReservation.Enabled = true;
                    pnlReservation.BackColor = Color.FromArgb(76, 255, 130);
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    DisableKeyFields();
                    cmbReservation.Focus();

                }
                if (transtype == "Reservation") //CLUBHOUSE
                {
                    DisablePanels();
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    DisableKeyFields();
                    txtRemarks.Focus();
                    txtAmountToPay.Text = price.ToString();
                }
                if (transtype == "Monthly Payment") //PARKING
                {
                    DisablePanels();
                    pnlDates.Enabled = true;
                    pnlDates.BackColor = Color.FromArgb(76, 255, 130);
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    DisableKeyFields();
                    fromdatePicker.Focus();
                    GetMonthlyFee();

                    if (Global.code == "007")
                    {
                        monthlypayment = 1;
                    }

                }
                if (transtype == "Rentals") //TABLE,CHAIR,SET RENTALS
                {
                    DisablePanels();
                    pnlQuantity.Enabled = true;
                    pnlQuantity.BackColor = Color.FromArgb(76, 255, 130);
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    DisableKeyFields();
                    txtQuantity.Focus();
                }
                if (transtype == "Sticker Fee") //STICKER
                {
                    DisablePanels();
                    pnlQuantity.Enabled = true;
                    pnlQuantity.BackColor = Color.FromArgb(76, 255, 130);
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    DisableKeyFields();
                    txtQuantity.Focus();
                }
                if (transtype == "Others") //OTHERS
                {
                    DisablePanels();
                    pnlOthers.Enabled = true;
                    pnlOthers.BackColor = Color.FromArgb(76, 255, 130);
                    pnlTotals.Enabled = true;
                    pnlTotals.BackColor = Color.FromArgb(76, 255, 130);
                    Global.Enable(IncomeTransaction);
                    txtUnitIDIncome.Enabled = false;
                    txtSpecify.Focus();
                    txtAmountToPay.ReadOnly = false;

                }
            }
        }
        //GETS THE MONTHLY FEE
        private void GetMonthlyFee()
        {
            String from = fromdatePicker.Text;
            String to = todatePicker.Text;
            DateTime fromDate = Convert.ToDateTime(from);
            DateTime toDate = Convert.ToDateTime(to);
            MonthDifference(toDate, fromDate);
            double temp = months * price;
            txtAmountToPay.Text = temp.ToString();
        }
        //DISABLES THE KEY FIELDS IN INCOMETRANSACTION PAGE
        private void DisableKeyFields()
        {
            txtUnitIDIncome.ReadOnly = true;
            txtAmountToPay.ReadOnly = true;
            txtChangeIncome.ReadOnly = true;
            txtTotalAmountIncome.ReadOnly = true;

        }
        //DISABLES THE PANELS IN INCOME TRANSACTION
        private void DisablePanels()
        {

            pnlDates.Enabled = false;
            pnlDates.BackColor = SystemColors.Control;
            pnlHOA.Enabled = false;
            pnlHOA.BackColor = SystemColors.Control;
            pnlOthers.Enabled = false;
            pnlOthers.BackColor = SystemColors.Control;
            pnlQuantity.Enabled = false;
            pnlQuantity.BackColor = SystemColors.Control;
            pnlReservation.Enabled = false;
            pnlReservation.BackColor = SystemColors.Control;
            pnlTotals.Enabled = false;
            pnlTotals.BackColor = SystemColors.Control;
            pnlPerHead.Enabled = false;
            pnlPerHead.BackColor = SystemColors.Control;
            txtORNoIncome.Enabled = true;
            btnCancelIncome.Enabled = true;
            txtSearchAllTransactions.Enabled = true;

        }
        //WHEN PICKING DATES IN INCOME TRANSACTION
        private void fromdatePicker_ValueChanged(object sender, EventArgs e)
        {
            fromdatePicker.Value = new DateTime(fromdatePicker.Value.Year, fromdatePicker.Value.Month, 1);
            todatePicker.MinDate = fromdatePicker.Value;
            if (transtype == "Monthly Due")
                GetPriceHOA();
            if (transtype == "Monthly Payment")
                GetMonthlyFee();
        }
        private void MonthDifference(DateTime lValue, DateTime rValue)
        {
            months = (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
            months++;
        }
        //GETTING THE PRICE OF THE MONTHLY DUE
        private void GetPriceHOA()
        {
            DateTime today = DateTime.Today;
            int daytoday = (int)System.DateTime.Now.Day;
            String from = fromdatePicker.Text;
            String to = todatePicker.Text;
            DateTime fromDate = Convert.ToDateTime(from);
            DateTime toDate = Convert.ToDateTime(to);
            MonthDifference(toDate, fromDate);
            double temp = months * price;
            double temp1 = price;
            double temp_months = months;
            
     

            double discount = 0, penalty = 0, totalpenalty = 0;
            if (daytoday <= 10)
            {
                DataTable dt = Global.Query2("*", "IncomeServices Where Code ='015'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    discount = double.Parse(dr["Price"].ToString());
                    temp_months--;
                }
                temp -= discount;

            }
            else if (daytoday <= 16)
            {
                DataTable dt = Global.Query2("*", "IncomeServices Where Code ='016'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    discount = double.Parse(dr["Price"].ToString());
                    temp_months--;
                }
                temp -= discount;
            }
            else if (daytoday <= 24)
            {
                discount = 0;
                penalty = 0;
                temp_months--;
            }
            else if (daytoday <= 31)
            {
                DataTable dt = Global.Query2("*", "IncomeServices Where Code ='017'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    penalty = double.Parse(dr["Price"].ToString());
                    temp_months--;
                }
                totalpenalty = temp1 * penalty;
                txtPenalty.Text = totalpenalty.ToString();
                temp += totalpenalty;
            }

            if(temp_months>0)
            {
                DataTable dt = Global.Query2("*", "IncomeServices Where Code ='015'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    discount = double.Parse(dr["Price"].ToString());
                }
                double temp_discount = temp_months * discount;
                temp -= temp_discount;
                discount = temp_discount;
            }

            txtDiscount.Text = discount.ToString();
            txtPenalty.Text = totalpenalty.ToString();
            txtAmountToPay.Text = temp.ToString();
        }
        //TO DATE PICKER CHANGED
        private void todatePicker_ValueChanged(object sender, EventArgs e)
        {

            todatePicker.MinDate = fromdatePicker.Value;
            if (transtype == "Monthly Due")
                GetPriceHOA();
            if (transtype == "Monthly Payment")
                GetMonthlyFee();
        }
        //COMPUTING THE QUANTITY
        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            if (Global.code == "011")
            {
                double temp1, temp = 0; ;
                if (!double.TryParse(txtQuantity.Text, out temp1))
                {
                    if (txtQuantity.Text != "")
                    {
                        MessageBox.Show("Invalid input. Only numbers are allowed");
                        txtQuantity.Clear();
                    }
                    else
                        txtAmountToPay.Text = "0";
                    return;
                }
                else
                {
                    if (temp1 <= 1)
                    {
                        temp = temp1 * price;
                        txtAmountToPay.Text = temp.ToString();
                    }
                    else
                    {
                        double a = price;
                        DataTable dt = Global.Query2("*", "IncomeServices Where Code = '012'");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dr = dt.Rows[i];
                            price = double.Parse(dr["Price"].ToString());
                        }
                        temp1 -= 1;
                        temp = temp1 * price;
                        price = a;
                        temp += price;
                        txtAmountToPay.Text = temp.ToString();
                    }
                }


            }
            else
            {
                double temp1, temp = 0;
                if (!double.TryParse(txtQuantity.Text, out temp1))
                {
                    if (txtQuantity.Text != "")
                    {
                        MessageBox.Show("Invalid input. Only numbers are allowed");
                        txtQuantity.Clear();
                    }
                    else
                        txtAmountToPay.Text = "0";
                    return;
                }
                else
                {
                    temp = temp1 * price;
                    txtAmountToPay.Text = temp.ToString();
                }
            }
        }

        private void cmbReservation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbReservation.Text == "No")
            {
                pnlPerHead.Enabled = true;
                pnlPerHead.BackColor = Color.FromArgb(76, 255, 130);
                txtNoOfResidents.Focus();
                txtAmountToPay.Clear();
            }
            else
            {
                pnlPerHead.Enabled = false;
                pnlPerHead.BackColor = SystemColors.Control;
                txtRemarks.Focus();
                DataTable dt = Global.Query2("*", "IncomeServices Where Code = '003'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    price = double.Parse(dr["Price"].ToString());
                }
                txtAmountToPay.Text = price.ToString();
            }
        }

        private void txtCashIncome_TextChanged(object sender, EventArgs e)
        {
            double temp = 0, temp1 = 0, temp2 = 0;
            if (!double.TryParse(txtCashIncome.Text, out temp1))
            {
                if (txtCashIncome.Text != "")
                {
                    MessageBox.Show("Invalid input. Only numbers are allowed");
                    txtCashIncome.Clear();
                }
                else
                {
                    if (txtCheckAmountIncome.Text != "")
                        txtTotalAmountIncome.Text = txtCheckAmountIncome.Text;
                    else
                        txtTotalAmountIncome.Text = "0";

                }
                return;
            }
            else
            {
                if (txtCashIncome.Text == "")
                    temp1 = 0;
                else
                    temp1 = double.Parse(txtCashIncome.Text);

                if (txtCheckAmountIncome.Text == "")
                    temp2 = 0;
                else
                    temp2 = double.Parse(txtCheckAmountIncome.Text);

                temp = temp1 + temp2;

                txtTotalAmountIncome.Text = temp.ToString();
            }
        }
        //FOR THE PAY BUTTON
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtORNoIncome.Text != "")
            {
                if (txtAmountToPay.Text != "")
                {
                    if (txtTotalAmountIncome.Text != "")
                    {
                        if (double.Parse(txtAmountToPay.Text) <= double.Parse(txtTotalAmountIncome.Text))
                        {
                            var ds = Global.CheckPrimaryKey("*", "IncomeLogs", "ORNum", txtORNoIncome.Text);
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                if (transtype == "Monthly Due")//HOADUE
                                {
                                    int fromMonth = int.Parse(fromdatePicker.Value.ToString("MM"));
                                    int fromyear = int.Parse(fromdatePicker.Value.ToString("yyyy"));
                                    int toMonth = int.Parse(todatePicker.Value.ToString("MM"));
                                    int ctr = 1;
                                    while (months != 0)
                                    {
                                        for (int x = fromMonth; x <= 12; x++)
                                        {
                                            if (months != 0)
                                            {
                                                String month_ = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(x);
                                                var dt1 = Global.Query2(month_, "MonthlyDue WHERE UnitID='" + txtUnitIDIncome.Text + "' AND Year=" + fromyear);
                                                for (int j = 0; j < dt1.Rows.Count; j++)
                                                {
                                                    DataRow dr1 = dt1.Rows[j];
                                                    String paid = dr1[month_].ToString();

                                                    if (paid == "")
                                                    {
                                                        if (ctr == 1)
                                                            Global.Update_MonthlyDue(txtUnitIDIncome.Text, fromyear.ToString(), month_, double.Parse(txtAmountToPay.Text));
                                                        else
                                                            Global.Update_MonthlyDue(txtUnitIDIncome.Text, fromyear.ToString(), month_, 0);
                                                        months--;
                                                        ctr++;
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("One of the months are already paid");
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                        fromyear++;
                                        if (months != 0)
                                        {
                                            Global.Add_MonthlyDue(txtUnitIDIncome.Text, fromyear.ToString());
                                            fromMonth = 1;
                                        }

                                    }
                                }

                                if (transtype == "Monthly Payment" && monthlypayment == 1)//PARKING
                                {
                                    int fromMonth = int.Parse(fromdatePicker.Value.ToString("MM"));
                                    int fromyear = int.Parse(fromdatePicker.Value.ToString("yyyy"));
                                    int toMonth = int.Parse(todatePicker.Value.ToString("MM"));
                                    int ctr = 1;
                                    while (months != 0)
                                    {
                                        for (int x = fromMonth; x <= 12; x++)
                                        {
                                            if (months != 0)
                                            {
                                                String month_ = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(x);
                                                var dt1 = Global.Query2(month_, "ParkingFee WHERE UnitID='" + txtUnitIDIncome.Text + "' AND Year=" + fromyear);
                                                for (int j = 0; j < dt1.Rows.Count; j++)
                                                {
                                                    DataRow dr1 = dt1.Rows[j];
                                                    String paid = dr1[month_].ToString();

                                                    if (paid == "")
                                                    {

                                                        if (ctr == 1)
                                                            Global.Update_ParkingFee(txtUnitIDIncome.Text, fromyear.ToString(), month_, double.Parse(txtAmountToPay.Text));
                                                        else
                                                            Global.Update_ParkingFee(txtUnitIDIncome.Text, fromyear.ToString(), month_, 0);
                                                        months--;
                                                        ctr++;
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("One of the months are already paid");
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                        fromyear++;
                                        if (months != 0)
                                        {
                                            Global.Add_ParkingFee(txtUnitIDIncome.Text, fromyear.ToString());
                                        }

                                    }
                                    monthlypayment = 0;
                                }


                                Global.OrNo = txtORNoIncome.Text;
                                if (txtCheckAmountIncome.Text == "")
                                {
                                    txtCheckAmountIncome.Text = "0";
                                }
                                if (txtCheckNoIncome.Text == "")
                                {
                                    txtCheckNoIncome.Text = "NA";
                                }
                                if (txtCashIncome.Text == "")
                                {
                                    txtCashIncome.Text = "0";
                                }

                                if (transtype == "Others")
                                    txtRemarks.Text = txtSpecify.Text + "/" + txtRemarks.Text;


                                DataTable dt = Global.Query2("*", "IncomeServices Where Code ='" + Global.code + "'");
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    DataRow dr = dt.Rows[i];
                                    particular = dr["Name"].ToString();

                                }

                                txtChangeIncome.Text = (double.Parse(txtTotalAmountIncome.Text) - double.Parse(txtAmountToPay.Text)).ToString();

                                if (txtCheckAmountIncome.Text != "0" && txtCheckNoIncome.Text != "NA")
                                    Global.Add_Record(txtORNoIncome.Text, txtDate.Text, txtUnitIDIncome.Text, Global.code, double.Parse(txtAmountToPay.Text), double.Parse(txtCashIncome.Text), txtCheckNoIncome.Text, double.Parse(txtCheckAmountIncome.Text), checkDatePicker.Text, double.Parse(txtTotalAmountIncome.Text), txtRemarks.Text, particular);
                                else
                                    Global.Add_Record(txtORNoIncome.Text, txtDate.Text, txtUnitIDIncome.Text, Global.code, double.Parse(txtAmountToPay.Text), double.Parse(txtCashIncome.Text), txtCheckNoIncome.Text, double.Parse(txtCheckAmountIncome.Text), "NA", double.Parse(txtTotalAmountIncome.Text), txtRemarks.Text, particular);

                                MessageBox.Show("Transaction Complete");
                                Receipt r = new Receipt();
                                r.ShowDialog();

                                transtype = "";
                                txtORNoIncome.Clear();
                                DisablePanels();
                                Load_All();
                                radioAllTransaction.Checked = true;
                                Reload_IncomeTransactions(1);
                                Clear();
                                txtUnitIDIncome.Clear();

                            }
                            else
                            {
                                MessageBox.Show("OR# already exists");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Insufficient payment");
                            txtCashIncome.Focus();

                        }
                    }
                    else
                    {
                        MessageBox.Show("Please input payment");
                        txtCashIncome.Focus();

                    }
                }
                else
                {
                    MessageBox.Show("Please check fields");
                }
            }
            else
            {
                MessageBox.Show("Please Input OR#");
                txtORNoIncome.Focus();
            }


        }

        private void btnCancelIncome_Click(object sender, EventArgs e)
        {
            DisablePanels();
            transtype = "";
            txtORNoIncome.Clear();
            tabControl1.SelectedTab = Units;
            tabControl1.TabPages.Remove(IncomeTransaction);

            closeIncomeTransactionTab();
        }
        private void radioAllTransaction_CheckedChanged(object sender, EventArgs e)
        {
            incometrans = 1;
            Reload_IncomeTransactions(1);
            
        }
        private void radioUnitTransaction_CheckedChanged(object sender, EventArgs e)
        {
            incometrans = 2;
            Reload_IncomeTransactions(2);
            
        }
 
        private void LoadIncomeView()
        {
            iecounter = 1;
            ColumnClickEventArgs args = new ColumnClickEventArgs(0);
            AllTransactionsListView_ColumnClick(this, args);
            args = new ColumnClickEventArgs(0);
            AllTransactionsListView_ColumnClick(this, args);
            AllTransactionsListView.Clear();
            IncomeTransactionInitializeView();
            DataTable dt = Global.Query2("*", "IncomeLogs");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["ORnum"].ToString());
                listitem.SubItems.Add(dr["CurrDate"].ToString());
                listitem.SubItems.Add(dr["UnitID"].ToString());
                DataTable dt1 = Global.Query2("*", "UnitInfo WHERE UnitID='" + dr["UnitID"].ToString() + "'");
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    DataRow dr1 = dt1.Rows[j];
                    listitem.SubItems.Add(dr1["OwnerName"].ToString());
                }
                listitem.SubItems.Add(dr["Code"].ToString());
                listitem.SubItems.Add(dr["Particular"].ToString());
                if (double.Parse(dr["Price"].ToString()) == 0)
                {
                    listitem.SubItems.Add("Cancelled");
                    listitem.SubItems[0].BackColor = Color.FromArgb(247, 122, 111);

                }
                else
                {
                    listitem.SubItems.Add(dr["Price"].ToString());
                }

                if (dr["Cash"].ToString() != "0.00" && dr["Checknum"].ToString() == "NA" && dr["CheckAmount"].ToString() == "0.00")
                {
                    listitem.SubItems.Add("Cash");
                }
                else if (dr["Checknum"].ToString() != "NA" && dr["CheckAmount"].ToString() != "0.00")
                {
                    listitem.SubItems.Add("Check");
                }
                else
                {
                    listitem.SubItems.Add("Cash/Check");
                }
                listitem.SubItems.Add(dr["TotalAmount"].ToString());
                listitem.SubItems.Add(dr["Remarks"].ToString());
                AllTransactionsListView.Items.Add(listitem);

            }
        }

        private void SearchAllTransactions(DataTable dt)
        {
            if (iecounter == 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["ORnum"].ToString());
                    listitem.SubItems.Add(dr["CurrDate"].ToString());
                    listitem.SubItems.Add(dr["UnitID"].ToString());
                    DataTable dt1 = Global.Query2("*", "UnitInfo WHERE UnitID='" + dr["UnitID"].ToString() + "'");
                    for (int j = 0; j < dt1.Rows.Count; j++)
                    {
                        DataRow dr1 = dt1.Rows[j];
                        listitem.SubItems.Add(dr1["OwnerName"].ToString());
                    }
                    listitem.SubItems.Add(dr["Code"].ToString());
                    listitem.SubItems.Add(dr["Particular"].ToString());
                    if (double.Parse(dr["Price"].ToString()) == 0)
                    {
                        listitem.SubItems.Add("Cancelled");
                        listitem.SubItems[0].BackColor = Color.FromArgb(247, 122, 111);

                    }
                    else
                    {
                        listitem.SubItems.Add(dr["Price"].ToString());
                    }
                    listitem.SubItems.Add(dr["Cash"].ToString());
                    listitem.SubItems.Add(dr["Checknum"].ToString());
                    listitem.SubItems.Add(dr["CheckAmount"].ToString());
                    listitem.SubItems.Add(dr["CheckDate"].ToString());
                    listitem.SubItems.Add(dr["TotalAmount"].ToString());
                    listitem.SubItems.Add(dr["Remarks"].ToString());
                    AllTransactionsListView.Items.Add(listitem);

                }
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["CurrDate"].ToString());



                    listitem.SubItems.Add(dr["Particular"].ToString());
                    listitem.SubItems.Add(dr["ExpenseDetails"].ToString());
                    listitem.SubItems.Add(dr["OrNum"].ToString());
                    listitem.SubItems.Add(dr["Checknum"].ToString());
                    listitem.SubItems.Add(dr["CheckAmount"].ToString());
                    listitem.SubItems.Add(dr["CheckDate"].ToString());
                    listitem.SubItems.Add(dr["Cash"].ToString());
                    if (double.Parse(dr["TotalAmount"].ToString()) == 0)
                    {
                        listitem.SubItems.Add("Cancelled");
                        listitem.SubItems[0].BackColor = Color.FromArgb(247, 122, 111);
                    }
                    else
                        listitem.SubItems.Add(dr["TotalAmount"].ToString());
                    AllTransactionsListView.Items.Add(listitem);
                }
            }
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            btnNewExpense.Enabled = false;
            LoadIncomeView();
            lblTotal.Text = "Total Income";
            DataTable dt = Global.Query2("SUM(Price) AS 'Total'", "IncomeLogs");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                txtTotal.Text = dr["Total"].ToString();

            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            LoadExpenseView();
            lblTotal.Text = "Total Expense";
            DataTable dt = Global.Query2("SUM(TotalAmount) AS 'Total'", "ExpenseLogs");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                txtTotal.Text = dr["Total"].ToString();

            }


        }
        private void LoadExpenseView()
        {
            iecounter = 2;
            ColumnClickEventArgs args = new ColumnClickEventArgs(0);
            AllTransactionsListView_ColumnClick(this, args);
            args = new ColumnClickEventArgs(0);
            AllTransactionsListView_ColumnClick(this, args);
            AllTransactionsListView.Clear();
            ExpenseTransactionInitializeView();
            btnNewExpense.Enabled = true;

            DataTable dt = Global.Query2("*", "ExpenseLogs");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["CurrDate"].ToString());
                listitem.SubItems.Add(dr["Particular"].ToString());
                listitem.SubItems.Add(dr["ExpenseDetails"].ToString());
                listitem.SubItems.Add(dr["OrNum"].ToString());
                listitem.SubItems.Add(dr["Checknum"].ToString());
                listitem.SubItems.Add(dr["CheckAmount"].ToString());
                listitem.SubItems.Add(dr["CheckDate"].ToString());
                listitem.SubItems.Add(dr["Cash"].ToString());
                if (double.Parse(dr["TotalAmount"].ToString()) == 0)
                {
                    listitem.SubItems.Add("Cancelled");
                    listitem.SubItems[0].BackColor = Color.FromArgb(247, 122, 111);
                }
                else
                    listitem.SubItems.Add(dr["TotalAmount"].ToString());
                AllTransactionsListView.Items.Add(listitem);
            }
        }
        private void ExpenseTransactionInitializeView()
        {
            AllTransactionsListView.Columns.Add("Date", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Particular", 150, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Expense Details", 150, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("OR#", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Check#", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Check Amount", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Check Date", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Cash", 100, HorizontalAlignment.Left);
            AllTransactionsListView.Columns.Add("Total Amount", 100, HorizontalAlignment.Left);

        }

        private void btnCancelTransaction_Click(object sender, EventArgs e)
        {

            if (AllTransactionsListView.SelectedItems.Count != 0)
            {
                if (iecounter == 2)
                {

                    DialogResult dialogResult = MessageBox.Show("Do you really want to cancel this transaction?", "Warning", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ListViewItem item = AllTransactionsListView.SelectedItems[0];
                        String ORNo = item.Text;
                        Global.Edit_Record(ORNo);
                      
                        MessageBox.Show("Transaction was successfully cancelled");
                        LoadExpenseView();

                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        return;
                    }

                }
                if (iecounter == 1)
                {

                    DialogResult dialogResult = MessageBox.Show("Do you really want to cancel this transaction?", "Warning", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ListViewItem item = AllTransactionsListView.SelectedItems[0];
                        String ORNo = item.Text;
                        CancelMonthlyDue();
                        Global.Edit_Record_(ORNo);
                        MessageBox.Show("Transaction was successfully cancelled");
                       
                        LoadIncomeView();
                        Reload_IncomeTransactions(1);

                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        return;
                    }

                }

            }
            else
  
                MessageBox.Show("Select from the list");
     }
        private void CancelMonthlyDue()
        {
            String month = "", UnitID = "", code = "";
            double amountpaid = 0;
            ListViewItem item = AllTransactionsListView.SelectedItems[0];
            String ORNo = item.Text;
            SqlConnection con = new SqlConnection(Global.connectionString);
            SqlCommand command = new SqlCommand("select * from IncomeLogs WHERE ORnum='" + ORNo + "'", con);
            con.Open();
            SqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                month = (read["CurrDate"].ToString());
                UnitID = (read["UnitID"].ToString());
                code = (read["Code"].ToString());
                amountpaid = double.Parse((read["Price"].ToString()));

                
            }
            read.Close();
            con.Close();
            MessageBox.Show(code);

            if (code == "001" || code == "002")
            {
                DateTime x = DateTime.Parse(month);
                int temp = int.Parse(x.ToString("MM"));
                int temp2 = int.Parse(x.ToString("yyyy"));
 
                DataTable dt = Global.Query2("*", "IncomeServices Where Code = '" + code + "'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    price = double.Parse(dr["Price"].ToString());
              
                }
                double temp3 = Math.Ceiling(amountpaid / price);
                int temp4 = int.Parse(temp3.ToString());
                MessageBox.Show(temp4.ToString());
                while (temp4 != 0)
                {
                    for (int a = temp; a <= 12; a++)
                    {
                        if (temp4 != 0)
                        {
                            String month_ = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(a);
                            Global.Update_MonthlyDue(UnitID, temp2.ToString(), month_);
                            temp4--;
                        }
                    }
                    temp2++;
                    if (temp4 != 0)
                    {
                        temp = 1;
                    }
                }
             
                Reload();
            }


        }

        private void btnNewExpense_Click(object sender, EventArgs e)
        {
            ExpenseTransaction expense = new ExpenseTransaction();

            expense.ShowDialog();
            LoadExpenseView();
            lblTotal.Text = "Total Expense";
            DataTable dt = Global.Query2("SUM(TotalAmount) AS 'Total'", "ExpenseLogs");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                txtTotal.Text = dr["Total"].ToString();

            }

        }

        private void btnAdd_Item_Click(object sender, EventArgs e)
        {
            Global.save_item = 0;
            Services service = new Services();
            if (Global.typeofservice == 1)
                service.Text = "Add Income Service";
            else
                service.Text = "Add Expense Service"; 
                service.ShowDialog();
            if (Global.typeofservice == 1)
                ReloadIncomeServices();
            if (Global.typeofservice == 2)
                ReloadExpenseServices();

        }

        private void btnEdit_Item_Click(object sender, EventArgs e)
        {
            if (ServicesListView.SelectedItems.Count != 0)
            {
                Global.save_item = 1;
                ListViewItem item = ServicesListView.SelectedItems[0];
                Global.code = item.Text;
                Services service = new Services();
                if (Global.typeofservice == 1)
                    service.Text = "Edit Income Service";
                else
                    service.Text = "Edit Expense Service";

                service.ShowDialog();
                if (Global.typeofservice == 1)
                    ReloadIncomeServices();
                if (Global.typeofservice == 2)
                    ReloadExpenseServices();
            }
            else
            {
                MessageBox.Show("Select from the list");
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCheckAmountIncome_TextChanged(object sender, EventArgs e)
        {
            if (txtCheckAmountIncome.Text != "" && txtCheckNoIncome.Text != "")
                checkDatePicker.Enabled = true;
            else
                checkDatePicker.Enabled = false;



            double temp = 0, temp1 = 0, temp2 = 0;
            if (!double.TryParse(txtCheckAmountIncome.Text, out temp1))
            {
                if (txtCheckAmountIncome.Text != "")
                {
                    MessageBox.Show("Invalid input. Only numbers are allowed");
                    txtCheckAmountIncome.Clear();
                }
                else
                {
                    if (txtCashIncome.Text != "")
                        txtTotalAmountIncome.Text = txtCashIncome.Text;
                    else
                        txtTotalAmountIncome.Text = "0";
                }
                return;
            }
            else
            {
                if (txtCashIncome.Text == "")
                    temp1 = 0;
                else
                    temp1 = double.Parse(txtCashIncome.Text);

                if (txtCheckAmountIncome.Text == "")
                    temp2 = 0;
                else
                    temp2 = double.Parse(txtCheckAmountIncome.Text);

                temp = temp1 + temp2;

                txtTotalAmountIncome.Text = temp.ToString();
            }

        }
        private void AllTransactionsListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Sorter s = (Sorter)AllTransactionsListView.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == System.Windows.Forms.SortOrder.Ascending)
            {
                s.Order = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                s.Order = System.Windows.Forms.SortOrder.Ascending;
            }
            AllTransactionsListView.Sort();

        }

        private void unitInfoListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Sorter s = (Sorter)unitInfoListView.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == System.Windows.Forms.SortOrder.Ascending)
            {
                s.Order = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                s.Order = System.Windows.Forms.SortOrder.Ascending;
            }
            unitInfoListView.Sort();

        }

        private void TransactionsListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Sorter s = (Sorter)IncomeTransactionsListView.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == System.Windows.Forms.SortOrder.Ascending)
            {
                s.Order = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                s.Order = System.Windows.Forms.SortOrder.Ascending;
            }
            IncomeTransactionsListView.Sort();

        }

        private void btnDelete_Item_Click(object sender, EventArgs e)
        {
            if (ServicesListView.SelectedItems.Count != 0)
            {

                DialogResult dialogResult = MessageBox.Show("Do you really want to delete this record?", "Warning", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ListViewItem item = ServicesListView.SelectedItems[0];
                    var code = item.Text;
                    if (Global.typeofservice == 1)
                        Global.Delete_Record(code, "IncomeServices");
                    else
                        Global.Delete_Record(code, "ExpenseServices");
                    MessageBox.Show("Record was successfully deleted");
                     if (Global.typeofservice == 1)
                    ReloadIncomeServices();
                if (Global.typeofservice == 2)
                    ReloadExpenseServices();
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }

            }
            else
                MessageBox.Show("Select from the list");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button7.Visible = false;
            button8.Visible = false;
            button4.Visible = false;
            button2.Visible = false;
            labelYear.Visible = false;
            txtYear.Visible = false;

            btnLoad.Visible = false;
            txtFrom.Visible = false;
            txtTo.Visible = false;
            dateFrom.Visible = false;
            dateTo.Visible = false;

            button3.Visible = true;
            cmbUnit.Visible = true;
            labelUnitType.Visible = true;

            VerdantHeightsDataSetTableAdapters.UnitInfoTableAdapter data_ad = new VerdantHeightsDataSetTableAdapters.UnitInfoTableAdapter();
            VerdantHeightsDataSet.UnitInfoDataTable edt = new VerdantHeightsDataSet.UnitInfoDataTable();
            data_ad.Fill(edt);

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "UnitInfo";
            reportDataSource1.Value = edt;

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../UnitInfo.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            this.reportViewer1.RefreshReport();

            //color or button
            btnUnitReport.BackColor = Color.FromArgb(115, 163, 135);
            btnIncomeReport.BackColor = Color.Transparent;
            btnExpenseReport.BackColor = Color.Transparent;
            btnMonthlyDuesReport.BackColor = Color.Transparent;
            button5.BackColor = Color.Transparent;
            button6.BackColor = Color.Transparent;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button7.Visible = false;
            button8.Visible = false;
            button4.Visible = false;
            button2.Visible = false;
            labelYear.Visible = false;
            txtYear.Visible = false;

            button3.Visible = false;
            cmbUnit.Visible = false;
            labelUnitType.Visible = false;

            btnLoad.Visible = true;
            txtFrom.Visible = true;
            txtTo.Visible = true;
            dateFrom.Visible = true;
            dateTo.Visible = true;
            Global.dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, 1);
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.IncomeTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.IncomeTableAdapter();
            VerdantHeightsDataSet.IncomeDataTable ds = new VerdantHeightsDataSet.IncomeDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "Income";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../Income.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();

            //color or button
            btnUnitReport.BackColor = Color.Transparent;
            btnIncomeReport.BackColor = Color.FromArgb(115, 163, 135);
            btnExpenseReport.BackColor = Color.Transparent;
            btnMonthlyDuesReport.BackColor = Color.Transparent;
            button5.BackColor = Color.Transparent;
            button6.BackColor = Color.Transparent;
        }

        private void cmbRented_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbRented.SelectedIndex == 0)
            {
                txtRenterName.Enabled = true;
                txtRenterName.Clear();
            }
            else
            {
                txtRenterName.Enabled = false;
                txtRenterName.Clear();
            }
        }

        private void txtNoOfGuests_TextChanged(object sender, EventArgs e)
        {
            double price_guest = 0;
            DataTable dt = Global.Query2("*", "IncomeServices Where Code ='004'");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                price_guest = double.Parse(dr["Price"].ToString());

            }
            double temp1, temp = 0, temp2 = 0;
            if (!double.TryParse(txtNoOfGuests.Text, out temp1))
            {
                if (txtNoOfGuests.Text != "")
                {
                    MessageBox.Show("Invalid input. Only numbers are allowed");
                    txtNoOfGuests.Clear();
                }
                else if (txtNoOfResidents.Text == "")
                {
                    txtAmountToPay.Text = "0";
                }
                else
                    txtAmountToPay.Text = total_res.ToString();
                return;
            }
            else
            {
                if (txtNoOfResidents.Text == "")
                {

                    total_guest = temp1 * price_guest;
                    txtAmountToPay.Text = total_guest.ToString();
                }
                else
                {
                    temp = 0;
                    total_guest = temp1 * price_guest;

                    temp = total_guest + total_res;
                    txtAmountToPay.Text = temp.ToString();
                }

            }
        }

        private void txtNoOfResidents_TextChanged(object sender, EventArgs e)
        {
            double price_res = 0;
            DataTable dt = Global.Query2("*", "IncomeServices Where Code ='005'");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                price_res = double.Parse(dr["Price"].ToString());

            }

            double temp1, temp = 0, temp2 = 0;
            if (!double.TryParse(txtNoOfResidents.Text, out temp1))
            {
                if (txtNoOfResidents.Text != "")
                {
                    MessageBox.Show("Invalid input. Only numbers are allowed");
                    txtNoOfResidents.Clear();
                }
                else if (txtNoOfGuests.Text == "")
                {
                    txtAmountToPay.Text = "0";
                }
                else
                    txtAmountToPay.Text = total_guest.ToString();

                return;
            }
            else
            {
                if (txtNoOfGuests.Text == "")
                {

                    total_res = temp1 * price_res;
                    txtAmountToPay.Text = total_res.ToString();
                }
                else
                {
                    temp = 0;
                    total_res = temp1 * price_res;
                    temp2 = total_guest;
                    temp = total_res + total_guest;
                    txtAmountToPay.Text = temp.ToString();
                }
            }
        }

        private void TransactionInitializeView()
        {
            IncomeTransactionsListView.Columns.Add("OR#", 100, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Date", 100, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Unit ID", 100, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Owner's Name", 120, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Code", 60, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Particular", 100, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Amount to Pay", 120, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Mode of Payment", 150, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Amount Paid", 100, HorizontalAlignment.Left);
            IncomeTransactionsListView.Columns.Add("Remarks", 150, HorizontalAlignment.Left);

        }

        private void txtCheckNoIncome_TextChanged(object sender, EventArgs e)
        {
            if (txtCheckAmountIncome.Text != "" && txtCheckNoIncome.Text != "")
                checkDatePicker.Enabled = true;
            else
                checkDatePicker.Enabled = false;
            
        }

        private void radioIncomeServices_CheckedChanged(object sender, EventArgs e)
        {
            ReloadIncomeServices();
        }

        private void ServicesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Sorter s = (Sorter)ServicesListView.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == System.Windows.Forms.SortOrder.Ascending)
            {
                s.Order = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                s.Order = System.Windows.Forms.SortOrder.Ascending;
            }
            ServicesListView.Sort();
        }

        private void radioExpenseServices_CheckedChanged(object sender, EventArgs e)
        {
            ReloadExpenseServices();
        }

        private void radioMonthlyDue_CheckedChanged(object sender, EventArgs e)
        {
            ReloadMonthlyView();
        }

        private void radioParkingFee_CheckedChanged(object sender, EventArgs e)
        {
            ReloadParkingFee();
        }

        private void txtSearchUnits_TextChanged(object sender, EventArgs e)
        {
            unitInfoListView.Items.Clear();
            unitInfoListView.View = View.Details;
            UnitInitializeView();
            var dt = new DataTable();

            if (String.IsNullOrWhiteSpace(txtSearchUnits.Text))
            {
                dt = Global.Query2("*", "UnitInfo");
                SearchUnits(dt);
            }
            else
            {
                dt = Global.SearchUnits("*", "UnitInfo",txtSearchUnits.Text);
                SearchUnits(dt);
            }
        }

        private void txtSearchAllTransactions_TextChanged(object sender, EventArgs e)
        {
            AllTransactionsListView.Clear();
            AllTransactionsListView.View = View.Details;
            var dt = new DataTable();
            if (iecounter == 1)
            {
                IncomeTransactionInitializeView();
                if (String.IsNullOrWhiteSpace(txtSearchAllTransactions.Text))
                {
                    dt = Global.Query2("*", "IncomeLogs");
                    SearchAllTransactions(dt);
                }
                else
                {
                     dt = Global.SearchIncomeLogs("*", "IncomeLogs",txtSearchAllTransactions.Text);
                    SearchAllTransactions(dt);
                }
            }
            else
            {
                ExpenseTransactionInitializeView();
                if (String.IsNullOrWhiteSpace(txtSearchAllTransactions.Text))
                {
                    dt = Global.Query2("*", "ExpenseLogs");
                    SearchAllTransactions(dt);
                }
                else
                {
                    dt = Global.SearchExpenseLogs("*", "ExpenseLogs", txtSearchAllTransactions.Text);
                    SearchAllTransactions(dt);
                }
            }
        }

        private void txtSearchServices_TextChanged(object sender, EventArgs e)
        {
            ServicesListView.Clear();
            ServicesListView.View = View.Details;
            var dt = new DataTable();
            if (Global.typeofservice==1)
            {
                IncomeServicesInitializeView();
                if (String.IsNullOrWhiteSpace(txtSearchServices.Text))
                {
                    dt = Global.Query2("*", "IncomeServices");
                    SearchServices(dt);
                }
                else
                {
                    dt = Global.SearchIncomeServices("*", "IncomeServices", txtSearchServices.Text);
                    SearchServices(dt);
                }
            }
            else
            {
                ExpenseServicesInitializeView();
                if (String.IsNullOrWhiteSpace(txtSearchServices.Text))
                {
                    dt = Global.Query2("*", "ExpenseServices");
                    SearchServices(dt);
                }
                else
                {
                    dt = Global.SearchExpenseServices("*", "ExpenseServices", txtSearchServices.Text);
                    SearchServices(dt);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
        private void Clear()
        {
            DisablePanels();
            Global.Clear(pnlQuantity, 2);
            Global.Clear(pnlDates, 2);
            Global.Clear(pnlHOA, 2);
            Global.Clear(pnlOthers, 2);
            Global.Clear(pnlPerHead, 2);
            Global.Clear(pnlReservation, 2);
            Global.Clear(pnlTotals, 2);
            cmbParticularIncome.SelectedIndex = -1;
            txtORNoIncome.Clear();
            txtORNoIncome.Focus();
        }
       private void txtSearchIncomeTransactions_TextChanged(object sender, EventArgs e)
        {
            IncomeTransactionsListView.Items.Clear();
            IncomeTransactionsListView.View = View.Details;
            var dt = new DataTable();
            if (incometrans==1)
            {
       
                if (String.IsNullOrWhiteSpace(txtSearchAllTransactions.Text))
                {
                    dt = Global.Query2("*", "IncomeLogs");
                    Reload_Transactions(dt);
                }
                else
                {
                    dt = Global.SearchIncomeLogs("*", "IncomeLogs", txtSearchAllTransactions.Text);
                    Reload_Transactions(dt);
                }
            }
            else
            {
   
                if (String.IsNullOrWhiteSpace(txtSearchAllTransactions.Text))
                {
                    dt = Global.Query2("*", "IncomeLogs WHERE UnitID ='"+txtUnitIDIncome.Text+"'");
                    Reload_Transactions(dt);
                }
                else
                {
                    dt = Global.SearchIncomeLogs("*", "IncomeLogs WHERE UnitID ='"+txtUnitID.Text+"' AND ", txtSearchAllTransactions.Text);
                    Reload_Transactions(dt);
                }
            }
        }
        private void btnLogout_Click_1(object sender, EventArgs e)
        {
            
            LoginForm lf = new LoginForm();
            lf.Show();
            this.Close();


        }
        private void btnAccountSettings_Click(object sender, EventArgs e)
        {
            
            AccountSettings account = new AccountSettings();
            account.ShowDialog();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {

        }

        private void btnMonthlyDuesReport_Click(object sender, EventArgs e)
        {
            button7.Visible = false;
            button8.Visible = false;
            button4.Visible = false;
            button3.Visible = false;
            cmbUnit.Visible = false;
            labelUnitType.Visible = false;

            btnLoad.Visible = false;
            txtFrom.Visible = false;
            txtTo.Visible = false;
            dateFrom.Visible = false;
            dateTo.Visible = false;

            button2.Visible = true;
            cmbUnit.Visible = true;
            labelUnitType.Visible = true;
            labelYear.Visible = true;
            txtYear.Visible = true;
            Global.Year = DateTime.Now.Year.ToString();

            VerdantHeightsDataSetTableAdapters.MonthlyDuesTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.MonthlyDuesTableAdapter();
            VerdantHeightsDataSet.MonthlyDuesDataTable ds = new VerdantHeightsDataSet.MonthlyDuesDataTable();
            tbl.FillBy(ds, Global.Year);

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "MonthlyDues";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../SUMonthlyDues.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();

            //color or button
            btnUnitReport.BackColor = Color.Transparent;
            btnIncomeReport.BackColor = Color.Transparent;
            btnExpenseReport.BackColor = Color.Transparent;
            btnMonthlyDuesReport.BackColor = Color.FromArgb(115, 163, 135);
            button5.BackColor = Color.Transparent;
            button6.BackColor = Color.Transparent;
        }

        private void btnLoad_Click_1(object sender, EventArgs e)
        {
            Global.dateFrom = dateFrom.Value;
            Global.dateTo = dateTo.Value;
            VerdantHeightsDataSetTableAdapters.IncomeTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.IncomeTableAdapter();
            VerdantHeightsDataSet.IncomeDataTable ds = new VerdantHeightsDataSet.IncomeDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "Income";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../Income.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            this.reportViewer1.RefreshReport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Global.Year = txtYear.Text;
            Global.UnitType = cmbUnit.Text;

            VerdantHeightsDataSetTableAdapters.MonthlyDuesTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.MonthlyDuesTableAdapter();
            VerdantHeightsDataSet.MonthlyDuesDataTable ds = new VerdantHeightsDataSet.MonthlyDuesDataTable();
            tbl.Fill(ds, Global.Year, Global.UnitType);

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "MonthlyDues";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../SUMonthlyDues.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Global.UnitType = cmbUnit.Text;

            VerdantHeightsDataSetTableAdapters.UnitInfoTableAdapter data_ad = new VerdantHeightsDataSetTableAdapters.UnitInfoTableAdapter();
            VerdantHeightsDataSet.UnitInfoDataTable edt = new VerdantHeightsDataSet.UnitInfoDataTable();
            data_ad.FillBy(edt, Global.UnitType);

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "UnitInfo";
            reportDataSource1.Value = edt;

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../UnitInfo.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            this.reportViewer1.RefreshReport();
        }
        private void btnExpenseReport_Click(object sender, EventArgs e)
        {
            button7.Visible = false;
            button8.Visible = false;
            button2.Visible = false;
            labelYear.Visible = false;
            txtYear.Visible = false;

            button3.Visible = false;
            cmbUnit.Visible = false;
            labelUnitType.Visible = false;

            btnLoad.Visible = false;

            button4.Visible = true;
            txtFrom.Visible = true;
            txtTo.Visible = true;
            dateFrom.Visible = true;
            dateTo.Visible = true;
            Global.dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, 1);
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter();
            VerdantHeightsDataSet.ExpenseLogsDataTable ds = new VerdantHeightsDataSet.ExpenseLogsDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "Expense";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../Expense.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();

            //color or button
            btnUnitReport.BackColor = Color.Transparent;
            btnIncomeReport.BackColor = Color.Transparent;
            btnExpenseReport.BackColor = Color.FromArgb(115, 163, 135);
            btnMonthlyDuesReport.BackColor = Color.Transparent;
            button5.BackColor = Color.Transparent;
            button6.BackColor = Color.Transparent;
        }
        private void button4_Click_1(object sender, EventArgs e)
        {
            Global.dateFrom = dateFrom.Value;
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter();
            VerdantHeightsDataSet.ExpenseLogsDataTable ds = new VerdantHeightsDataSet.ExpenseLogsDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "Expense";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../Expense.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(UnitInfo);
            tabControl1.TabPages.Insert(2, UnitInfo);
            save = 1;

            showUnitInfoTab();

            btnSave.Text = "Save";
            tabControl1.SelectedTab = UnitInfo;
            Global.Enable(UnitInfo);
            Global.RemoveReadOnly(UnitInfo);
            Global.Clear(UnitInfo, 2);
            txtUnitID.Text = String.Empty;
            lblUnitID.Visible = false;
            txtUnitID.Visible = false;
            txtRenterName.Enabled = false;
            txtBlk.Focus();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            button7.Visible = true;
            button8.Visible = false;
            button4.Visible = false;
            button2.Visible = false;
            labelYear.Visible = false;
            txtYear.Visible = false;

            button3.Visible = false;
            cmbUnit.Visible = false;
            labelUnitType.Visible = false;

            btnLoad.Visible = false;
            txtFrom.Visible = true;
            txtTo.Visible = true;
            dateFrom.Visible = true;
            dateTo.Visible = true;
            Global.dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, 1);
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.IncomeLogsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.IncomeLogsTableAdapter();
            VerdantHeightsDataSet.IncomeLogsDataTable ds = new VerdantHeightsDataSet.IncomeLogsDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "IncomeLogs";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../IncomeLogs.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();

            //color or button
            btnUnitReport.BackColor = Color.Transparent;
            btnIncomeReport.BackColor = Color.Transparent;
            btnExpenseReport.BackColor = Color.Transparent;
            btnMonthlyDuesReport.BackColor = Color.Transparent;
            button5.BackColor = Color.FromArgb(115, 163, 135);
            button6.BackColor = Color.Transparent;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Global.dateFrom = dateFrom.Value;
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.IncomeLogsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.IncomeLogsTableAdapter();
            VerdantHeightsDataSet.IncomeLogsDataTable ds = new VerdantHeightsDataSet.IncomeLogsDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "IncomeLogs";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../IncomeLogs.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Global.dateFrom = dateFrom.Value;
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter();
            VerdantHeightsDataSet.ExpenseLogsDataTable ds = new VerdantHeightsDataSet.ExpenseLogsDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "ExpenseLogs";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../ExpenseLogs.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            button7.Visible = false;
            button8.Visible = true;
            button4.Visible = false;
            button2.Visible = false;
            labelYear.Visible = false;
            txtYear.Visible = false;

            button3.Visible = false;
            cmbUnit.Visible = false;
            labelUnitType.Visible = false;

            btnLoad.Visible = false;
            txtFrom.Visible = true;
            txtTo.Visible = true;
            dateFrom.Visible = true;
            dateTo.Visible = true;
            Global.dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, 1);
            Global.dateTo = dateTo.Value;

            VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.ExpenseLogsTableAdapter();
            VerdantHeightsDataSet.ExpenseLogsDataTable ds = new VerdantHeightsDataSet.ExpenseLogsDataTable();
            tbl.Fill(ds, Global.dateFrom.ToString(), Global.dateTo.ToString());

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "ExpenseLogs";
            reportDataSource1.Value = ds;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../ExpenseLogs.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            reportViewer1.RefreshReport();

            //color or button
            btnUnitReport.BackColor = Color.Transparent;
            btnIncomeReport.BackColor = Color.Transparent;
            btnExpenseReport.BackColor = Color.Transparent;
            btnMonthlyDuesReport.BackColor = Color.Transparent;
            button5.BackColor = Color.Transparent;
            button6.BackColor = Color.FromArgb(115, 163, 135);
        }

        class Sorter : System.Collections.IComparer
        {
            public int Column = 0;
            public System.Windows.Forms.SortOrder Order = System.Windows.Forms.SortOrder.Ascending;
            public int Compare(object x, object y) // IComparer Member
            {
                if (!(x is ListViewItem))
                    return (0);
                if (!(y is ListViewItem))
                    return (0);

                ListViewItem l1 = (ListViewItem)x;
                ListViewItem l2 = (ListViewItem)y;

                if (l1.ListView.Columns[Column].Tag == null)
                {
                    l1.ListView.Columns[Column].Tag = "Text";
                }

                if (l1.ListView.Columns[Column].Tag.ToString() == "numeric")
                {
                    string newString, newString2, temp, temp1;
                    if (l1.SubItems[Column].Text == "NA")
                        temp = "0";
                    else
                        temp = l1.SubItems[Column].Text;
                    if (l2.SubItems[Column].Text == "NA")
                        temp1 = "0";
                    else
                        temp1 = l2.SubItems[Column].Text;

                    char[] remove = { 'd', 'a', 'y', 's', ' ' };
                    newString = temp.TrimEnd(remove);
                    newString2 = temp1.TrimEnd(remove);
                    float fl1 = float.Parse(newString);
                    float fl2 = float.Parse(newString2);

                    if (Order == System.Windows.Forms.SortOrder.Ascending)
                    {
                        return fl1.CompareTo(fl2);
                    }
                    else
                    {
                        return fl2.CompareTo(fl1);
                    }
                }
                else
                {
                    string str1 = l1.SubItems[Column].Text;
                    string str2 = l2.SubItems[Column].Text;

                    if (Order == System.Windows.Forms.SortOrder.Ascending)
                    {
                        return str1.CompareTo(str2);
                    }
                    else
                    {
                        return str2.CompareTo(str1);
                    }
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void IncomeTransactionsListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (IncomeTransactionsListView.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    String Check="";
                    ListViewItem item = IncomeTransactionsListView.SelectedItems[0];
                    Global.OrNo = item.Text;
                    SqlConnection con = new SqlConnection(Global.connectionString);
                    SqlCommand command = new SqlCommand("select * from IncomeLogs WHERE ORnum='" + Global.OrNo + "'", con);
                    con.Open();
                    SqlDataReader read = command.ExecuteReader();
                    while (read.Read())
                    {
                        Check = (read["Checknum"].ToString());
                    }
                    read.Close();
                    con.Close();

                    if (Check != "NA")
                    {
                        contextMenuStrip1.Items[0].Enabled = true;
                        Check = "";
                    }
                    else
                    {
                        contextMenuStrip1.Items[0].Enabled = false;
                        Check = "";
                    }
                    allOrIncome = 1;
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void detailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            
            CheckDetails cd = new CheckDetails();
            ListViewItem item;
            if (allOrIncome ==1)
             item = IncomeTransactionsListView.SelectedItems[0];
            else
             item = AllTransactionsListView.SelectedItems[0];
            Global.OrNo = item.Text;
            cd.StartPosition = FormStartPosition.Manual;
            cd.Location = new Point(Cursor.Position.X, Cursor.Position.Y);
            cd.ShowDialog();
        }

        private void AllTransactionsListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (AllTransactionsListView.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    String Check = "";
                    ListViewItem item = AllTransactionsListView.SelectedItems[0];
                    Global.OrNo = item.Text;
                    SqlConnection con = new SqlConnection(Global.connectionString);
                    SqlCommand command = new SqlCommand("select * from IncomeLogs WHERE ORnum='" + Global.OrNo + "'", con);
                    con.Open();
                    SqlDataReader read = command.ExecuteReader();
                    while (read.Read())
                    {
                        Check = (read["Checknum"].ToString());
                    }
                    read.Close();
                    con.Close();

                    if (Check != "NA")
                    {
                        contextMenuStrip1.Items[0].Enabled = true;
                        Check = "";
                    }
                    else
                    {
                        contextMenuStrip1.Items[0].Enabled = false;
                        Check = "";
                    }
                    allOrIncome = 2;
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void IncomeTransactionsListView_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem item = IncomeTransactionsListView.SelectedItems[0];
            String ORNo = item.Text;
            SqlConnection con = new SqlConnection(Global.connectionString);
            SqlCommand command = new SqlCommand("select * from IncomeLogs WHERE ORnum='" + ORNo + "'", con);
            con.Open();
            SqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                txtUnitIDIncome.Text = (read["UnitID"].ToString());
            }
            read.Close();
            con.Close();
            txtORNoIncome.Focus();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDateandTime.Text = DateTime.Now.ToString("MM/dd/yy hh:mm:ss:tt");
        }
        private void ResetDTP()
        {
            todatePicker.Value = DateTime.Now;
            fromdatePicker.Value = DateTime.Now;
            checkDatePicker.Value = DateTime.Now;
        }

        public class MyPanel : System.Windows.Forms.Panel
        {
            public MyPanel()
            {
                this.SetStyle(
                    System.Windows.Forms.ControlStyles.UserPaint |
                    System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                    System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer,
                    true);
            }
        }


    }
}