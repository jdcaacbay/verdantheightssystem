﻿namespace WindowsFormsApplication2
{
    partial class Services
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel_Item = new System.Windows.Forms.Button();
            this.btnSave_Item = new System.Windows.Forms.Button();
            this.cmbTransactionType = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPrice_Item = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtName_Item = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel_Item
            // 
            this.btnCancel_Item.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel_Item.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel_Item.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel_Item.Location = new System.Drawing.Point(256, 178);
            this.btnCancel_Item.Name = "btnCancel_Item";
            this.btnCancel_Item.Size = new System.Drawing.Size(75, 23);
            this.btnCancel_Item.TabIndex = 174;
            this.btnCancel_Item.Text = "Cancel";
            this.btnCancel_Item.UseVisualStyleBackColor = true;
            this.btnCancel_Item.Click += new System.EventHandler(this.btnCancel_Item_Click);
            // 
            // btnSave_Item
            // 
            this.btnSave_Item.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave_Item.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave_Item.Location = new System.Drawing.Point(164, 178);
            this.btnSave_Item.Name = "btnSave_Item";
            this.btnSave_Item.Size = new System.Drawing.Size(75, 23);
            this.btnSave_Item.TabIndex = 173;
            this.btnSave_Item.Text = "Save";
            this.btnSave_Item.UseVisualStyleBackColor = true;
            this.btnSave_Item.Click += new System.EventHandler(this.btnSave_Item_Click);
            // 
            // cmbTransactionType
            // 
            this.cmbTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTransactionType.FormattingEnabled = true;
            this.cmbTransactionType.Items.AddRange(new object[] {
            "Reservation",
            "Monthly Payment",
            "Rentals",
            "Sticker Fee",
            "Monthly Due",
            "Swimming Pool",
            "Others"});
            this.cmbTransactionType.Location = new System.Drawing.Point(145, 122);
            this.cmbTransactionType.Name = "cmbTransactionType";
            this.cmbTransactionType.Size = new System.Drawing.Size(121, 21);
            this.cmbTransactionType.TabIndex = 172;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(11, 125);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(128, 18);
            this.label31.TabIndex = 171;
            this.label31.Text = "Transaction Type: ";
            // 
            // txtPrice_Item
            // 
            this.txtPrice_Item.Location = new System.Drawing.Point(145, 76);
            this.txtPrice_Item.Name = "txtPrice_Item";
            this.txtPrice_Item.Size = new System.Drawing.Size(78, 20);
            this.txtPrice_Item.TabIndex = 169;
            this.txtPrice_Item.TextChanged += new System.EventHandler(this.txtPrice_Item_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(93, 76);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 18);
            this.label30.TabIndex = 170;
            this.label30.Text = "Price: ";
            // 
            // txtName_Item
            // 
            this.txtName_Item.Location = new System.Drawing.Point(145, 36);
            this.txtName_Item.Name = "txtName_Item";
            this.txtName_Item.Size = new System.Drawing.Size(158, 20);
            this.txtName_Item.TabIndex = 167;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(39, 39);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(104, 18);
            this.label29.TabIndex = 168;
            this.label29.Text = "Service Name: ";
            // 
            // Services
            // 
            this.AcceptButton = this.btnSave_Item;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(216)))), ((int)(((byte)(203)))));
            this.BackgroundImage = global::WindowsFormsApplication2.Properties.Resources.verdantbgline;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.btnCancel_Item;
            this.ClientSize = new System.Drawing.Size(343, 213);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel_Item);
            this.Controls.Add(this.btnSave_Item);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.cmbTransactionType);
            this.Controls.Add(this.txtName_Item);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.txtPrice_Item);
            this.Controls.Add(this.label30);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Services";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IncomeServices";
            this.Load += new System.EventHandler(this.IncomeServices_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancel_Item;
        private System.Windows.Forms.Button btnSave_Item;
        private System.Windows.Forms.ComboBox cmbTransactionType;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtPrice_Item;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtName_Item;
        private System.Windows.Forms.Label label29;
    }
}