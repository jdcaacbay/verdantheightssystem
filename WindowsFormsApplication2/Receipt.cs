﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Receipt : Form
    {
        //CONTAINS THE REPORT VIEWER WHEN TRANSACTING AND PRINTING THE RECEIPT
        public Receipt()
        {
            InitializeComponent();
        }

        private void Receipt_Load(object sender, EventArgs e)
        {
            VerdantHeightsDataSetTableAdapters.ReceiptsTableAdapter tbl = new VerdantHeightsDataSetTableAdapters.ReceiptsTableAdapter();
            VerdantHeightsDataSet.ReceiptsDataTable ds = new VerdantHeightsDataSet.ReceiptsDataTable();
            tbl.Fill(ds,Global.OrNo);

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            reportDataSource1.Name = "Receipts";
            reportDataSource1.Value = ds;

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportPath = @"../../Receipt.rdlc";

            System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            reportViewer1.SetPageSettings(pg);

            this.reportViewer1.RefreshReport();
        }
    }
}
