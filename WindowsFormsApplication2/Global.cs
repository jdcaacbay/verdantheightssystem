﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    //HERE CONTAINS ALL THE NEEDED FUNCTIONS GLOBALLY THROUGHOUT THE WHOLE SYSTEM. THIS CONSISTS OF FUNCTIONS FOR ADDING,UPDATING,DELETING,VALIDATIONS, AND ETC IN THE WHOLE SYSTEM. AND ALSO THE GLOBAL VARIABLES.
    class Global
    {
        public static String connectionString = "Data Source=LAPTOP-0IR0M9R9;Initial Catalog=VerdantHeights;Persist Security Info=True;User ID=sa;Password=jaranilla01";
        public static int ctr = 1;
        public static int save_item = 0;
        public static int code_item = 0;
        public static int typeofservice = 0;
        public static String code;
        public static int reload = 0;
        public static DateTime dateFrom, dateTo;
        public static String UnitID, Year, UnitType, OrNo;     //for global variable of unitID
        public static int validate = 0;   //For the validation of fields with parsing of int and double
        public static String username;
        public static void Add_Record(String a, String b, String c, String d, String e, int f, int g, String h, String i, String j, String k)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT UnitInfo VALUES(@UnitID,@Blk,@Lot,@OwnerName,@UnitType,@TurnOver,@Rented,@RenterName,@MobileNo,@PhoneNo,@Email)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Blk", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@Lot", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@OwnerName", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@UnitType", SqlDbType.VarChar).Value = e;
            cmd.Parameters.Add("@TurnOver", SqlDbType.Bit).Value = f;
            cmd.Parameters.Add("@Rented", SqlDbType.Bit).Value = g;
            cmd.Parameters.Add("@RenterName", SqlDbType.VarChar).Value = h;
            cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = i;
            cmd.Parameters.Add("@PhoneNo", SqlDbType.VarChar).Value = j;
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = k;


            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Add_Record(String a, String b, String c, String d, double e, double f, String g, double h,String i, double j, String k, String l)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT IncomeLogs VALUES(@ORnum,@CurrDate,@UnitID,@Code,@Price,@Cash,@Checknum,@CheckAmount,@CheckDate,@TotalAmount,@Remarks,@Particular)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@ORnum", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@CurrDate", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@Price", SqlDbType.Float).Value = e;
            cmd.Parameters.Add("@Cash", SqlDbType.Float).Value = f;
            cmd.Parameters.Add("@Checknum", SqlDbType.VarChar).Value = g;
            cmd.Parameters.Add("@CheckAmount", SqlDbType.Float).Value = h;
            cmd.Parameters.Add("@CheckDate", SqlDbType.VarChar).Value = i;
            cmd.Parameters.Add("@TotalAmount", SqlDbType.Float).Value = j;
            cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = k;
            cmd.Parameters.Add("@Particular", SqlDbType.VarChar).Value = l;



            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static void Add_Record(String a, String b, double c, String d)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT IncomeServices VALUES(@Code,@Name,@Price,@TransactionType)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@Price", SqlDbType.Float).Value = c;
            cmd.Parameters.Add("@TransactionType", SqlDbType.VarChar).Value = d;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static void Add_Record(String a, String b)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT ExpenseServices VALUES(@Code,@Name)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = b;


            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static void Edit_Record(String a, String b, String c, String d, String e, int f, int g, String h, String i, String j, String k)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE UnitInfo SET UnitID = @UnitID,Blk = @Blk,Lot = @Lot,OwnerName = @OwnerName,UnitType = @UnitType,TurnOver = @TurnOver,Rented = @Rented,RenterName = @RenterName, MobileNo = @MobileNo, PhoneNo = @PhoneNo, Email = @Email WHERE UnitID = @UnitID";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Blk", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@Lot", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@OwnerName", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@UnitType", SqlDbType.VarChar).Value = e;
            cmd.Parameters.Add("@TurnOver", SqlDbType.Bit).Value = f;
            cmd.Parameters.Add("@Rented", SqlDbType.Bit).Value = g;
            cmd.Parameters.Add("@RenterName", SqlDbType.VarChar).Value = h;
            cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = i;
            cmd.Parameters.Add("@PhoneNo", SqlDbType.VarChar).Value = j;
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = k;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Add_Record(String a, String b, String c, String d, String e, double f, String g, double h, double i)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT ExpenseLogs VALUES(@CurrDate,@Particular,@ExpenseDetails,@ORnum,@Checknum,@CheckAmount,@CheckDate,@Cash,@TotalAmount)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@CurrDate", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Particular", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@ExpenseDetails", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@ORnum", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@Checknum", SqlDbType.VarChar).Value = e;
            cmd.Parameters.Add("@CheckAmount", SqlDbType.Float).Value = f;
            cmd.Parameters.Add("@CheckDate", SqlDbType.VarChar).Value = g;
            cmd.Parameters.Add("@Cash", SqlDbType.Float).Value = h;
            cmd.Parameters.Add("@TotalAmount", SqlDbType.Float).Value = i;


            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Edit_Record(String a, String b, double c, String d)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE IncomeServices SET Code = @Code,Name = @Name,Price = @Price,TransactionType = @TransactionType  WHERE Code = @Code";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@Price", SqlDbType.Float).Value = c;
            cmd.Parameters.Add("@TransactionType", SqlDbType.VarChar).Value = d;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static void Edit_Record(String a, String b)
        {

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE ExpenseServices SET Code = @Code,Name = @Name WHERE Code = @Code";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = b;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Edit_Record(String a)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE ExpenseLogs SET Price = @AmountPaid WHERE ORnum = @ORnum";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@AmountPaid", SqlDbType.Float).Value = 0;
            cmd.Parameters.Add("@ORnum", SqlDbType.VarChar).Value = a;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Edit_Record_(String a)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE IncomeLogs SET Price = @AmountPaid WHERE ORnum = @ORnum";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@AmountPaid", SqlDbType.Float).Value = 0;
            cmd.Parameters.Add("@ORnum", SqlDbType.VarChar).Value = a;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_MonthlyDue(String a, String b, String c, double d)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE MonthlyDue SET " + c + " = @AmountPaid WHERE UnitID = @UnitID AND Year = @Year";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Year", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@AmountPaid", SqlDbType.Float).Value = d;


            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_MonthlyDue(String a, String b, String c)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE MonthlyDue SET " + c + " = @AmountPaid WHERE UnitID = @UnitID AND Year = @Year";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Year", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@AmountPaid", SqlDbType.Float).Value = DBNull.Value;


            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static void Add_MonthlyDue(String a, String b)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT MonthlyDue VALUES(@UnitID,@Year,@January,@February,@March,@April,@May,@June,@July,@August,@September,@October,@November,@December)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Year", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@January", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@February", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@March", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@April", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@May", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@June", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@July", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@August", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@September", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@October", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@November", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@December", SqlDbType.Float).Value = DBNull.Value;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Add_ParkingFee(String a, String b)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT ParkingFee VALUES(@UnitID,@Year,@January,@February,@March,@April,@May,@June,@July,@August,@September,@October,@November,@December)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Year", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@January", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@February", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@March", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@April", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@May", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@June", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@July", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@August", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@September", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@October", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@November", SqlDbType.Float).Value = DBNull.Value;
            cmd.Parameters.Add("@December", SqlDbType.Float).Value = DBNull.Value;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_ParkingFee(String a, String b, String c, double d)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE ParkingFee SET " + c + " = @AmountPaid WHERE UnitID = @UnitID AND Year = @Year";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@Year", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@AmountPaid", SqlDbType.Float).Value = d;


            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static DataSet Query(String columns, String tables)
        {
            var select = "SELECT " + columns + " FROM " + tables;
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }
        public static DataSet Login(String username, String password)
        {
            var select = "SELECT * FROM UserAccounts WHERE username = @username AND password = @password";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@username", username);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@password", password);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }
        public static DataSet CheckPrimaryKey(String columns, String tables, String where, String condition)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE "+where+" = @condition"; 
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@condition",condition);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }
        public static DataSet Query(String columns, String tables, String conditions)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE " + conditions;
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }
        public static DataTable Query2(String columns, String tables)
        {
            var select = "SELECT " + columns + " FROM " + tables;
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static DataTable AccountSettings(String columns, String tables, String username)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE username = @username";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@username", username);
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static DataTable SearchUnits(String columns,String tables, String searchTerm)
        {
            var select = "SELECT " + columns + " FROM " + tables+ " WHERE UnitID LIKE @searchterm OR Blk LIKE @searchTerm OR Lot LIKE @searchTerm or OwnerName LIKE @searchTerm OR UnitType LIKE @searchTerm OR RenterName LIKE @searchTerm OR MobileNo LIKE @searchTerm OR PhoneNo LIKE @searchTerm OR Email LIKE @searchTerm";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@searchTerm", "%" + searchTerm + "%");
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static DataTable SearchIncomeLogs(String columns, String tables, String searchTerm)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE UnitID LIKE @searchterm OR ORNum LIKE @searchTerm OR CurrDate LIKE @searchTerm or Code LIKE @searchTerm OR CheckNum LIKE @searchTerm OR CheckDate LIKE @searchTerm OR Remarks LIKE @searchTerm OR Particular LIKE @searchTerm";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@searchTerm", "%" + searchTerm + "%");
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static DataTable SearchExpenseLogs(String columns, String tables, String searchTerm)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE CurrDate LIKE @searchterm OR Particular LIKE @searchTerm OR ExpenseDetails LIKE @searchTerm or ORNum LIKE @searchTerm OR CheckNum LIKE @searchTerm OR CheckDate LIKE @searchTerm ";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@searchTerm", "%" + searchTerm + "%");
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static DataTable SearchIncomeServices(String columns, String tables, String searchTerm)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE Code LIKE @searchterm OR Name LIKE @searchTerm OR TransactionType LIKE @searchTerm";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@searchTerm", "%" + searchTerm + "%");
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static DataTable SearchExpenseServices(String columns, String tables, String searchTerm)
        {
            var select = "SELECT " + columns + " FROM " + tables + " WHERE Code LIKE @searchterm OR Name LIKE @searchTerm";
            var c = new SqlConnection(connectionString);
            var dataAdapter = new SqlDataAdapter(select, c);
            dataAdapter.SelectCommand.Parameters.AddWithValue("@searchTerm", "%" + searchTerm + "%");
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            return dt;
        }
        public static void Delete_Record(String a, String table)
        {
            var c = new SqlConnection(connectionString);
            var cmd = new SqlCommand();

            cmd = new SqlCommand("DELETE " + table + " WHERE Code= @Code", c);

            cmd.Parameters.Add("@Code", SqlDbType.Int).Value = a;

            c.Open();
            cmd.ExecuteNonQuery();
            c.Close();
        }
        public static void Delete_Unit(String a, String table)
        {
            var c = new SqlConnection(connectionString);
            var cmd = new SqlCommand();

            cmd = new SqlCommand("DELETE " + table + " WHERE UnitID= @UnitID", c);

            cmd.Parameters.Add("@UnitID", SqlDbType.VarChar).Value = a;

            c.Open();
            cmd.ExecuteNonQuery();
            c.Close();
        }
        public static void Clear(Control con, int type)
        {
            if (type == 2)
            {
                foreach (Control c in con.Controls)
                {
                    if (c is TextBox)
                        ((TextBox)c).Clear();
                    else if (c is ComboBox)
                        ((ComboBox)c).SelectedIndex = -1;
                    else if (c is CheckBox)
                        ((CheckBox)c).Checked = false;
                    else if (c is RichTextBox)
                        ((RichTextBox)c).Clear();
                    else
                        Clear(c, 2);
                }
            }
            else
            {
                foreach (Control c in con.Controls)
                {
                    if (c is TextBox)
                        ((TextBox)c).Clear();
                    else if (c is CheckBox)
                        ((CheckBox)c).Checked = false;
                    else if (c is RichTextBox)
                        ((RichTextBox)c).Clear();

                    Clear(c, 1);
                }
            }

        }
        public static void Disable(Control con)
        {
            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).Enabled = false;
                else if (c is ComboBox)
                    ((ComboBox)c).Enabled = false;
                else if (c is CheckBox)
                    ((CheckBox)c).Enabled = false;
                else if (c is RichTextBox)
                    ((RichTextBox)c).Enabled = false;
                else if (c is Button)
                    ((Button)c).Enabled = false;
                else
                    Disable(c);
            }

        }
        public static void Enable(Control con)
        {
            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).Enabled = true;
                else if (c is ComboBox)
                    ((ComboBox)c).Enabled = true;
                else if (c is CheckBox)
                    ((CheckBox)c).Enabled = true;
                else if (c is RadioButton)
                    ((RadioButton)c).Enabled = true;
                else if (c is RichTextBox)
                    ((RichTextBox)c).Enabled = true;
                else if (c is Button)
                    ((Button)c).Enabled = true;
                else
                    Enable(c);
            }

        }
        public static void ReadOnly(Control con)
        {
            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).ReadOnly = true;
                else if (c is RichTextBox)
                    ((RichTextBox)c).ReadOnly = true;
                else if (c is ListView)
                    ((ListView)c).Enabled = false;
                else if (c is ComboBox)
                    ((ComboBox)c).Enabled = false;
                else
                    ReadOnly(c);
            }
        }
        public static void RemoveReadOnly(Control con)
        {
            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).ReadOnly = false;
                else if (c is RichTextBox)
                    ((RichTextBox)c).ReadOnly = false;
                else if (c is ListView)
                    ((ListView)c).Enabled = true;
                else if (c is ComboBox)
                    ((ComboBox)c).Enabled = true;
                else
                    RemoveReadOnly(c);
            }
        }
        public static void Update_Record(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, String k, String l, String m, String n, String o)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE PERSONALINFO SET LName = @LName, FName = @FName, MName = @MName, PreAddress = @PreAddress, PerAddress = @PerAddress, HomePhone = @HomePhone, MobilePhone = @MobilePhone, Email = @Email, BirthDate = @BirthDate, SSSNo=@SSSNo, PhilHealthNo = @PhilHealthNo, CivilStatus = @CivilStatus, TINNo = @TINNo, Gender = @Gender WHERE EmployeeNo = @EmployeeNo";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.Int).Value = a;
            cmd.Parameters.Add("@LName", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@FName", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@MName", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@PreAddress", SqlDbType.VarChar).Value = e;
            cmd.Parameters.Add("@PerAddress", SqlDbType.VarChar).Value = f;
            cmd.Parameters.Add("@HomePhone", SqlDbType.VarChar).Value = g;
            cmd.Parameters.Add("@MobilePhone", SqlDbType.VarChar).Value = h;
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = i;
            cmd.Parameters.Add("@BirthDate", SqlDbType.VarChar).Value = j;
            cmd.Parameters.Add("@SSSNo", SqlDbType.VarChar).Value = k;
            cmd.Parameters.Add("@PhilHealthNo", SqlDbType.VarChar).Value = l;
            cmd.Parameters.Add("@CivilStatus", SqlDbType.VarChar).Value = m;
            cmd.Parameters.Add("@TINNo", SqlDbType.VarChar).Value = n;
            cmd.Parameters.Add("@Gender", SqlDbType.VarChar).Value = o;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_Record(String a, String b, String c, String d, String e, String f)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE EDUCATION SET SchoolName=@SchoolName ,Address=@Address, YearsAttended=@YearsAttended, Degree=@Degree WHERE EmployeeNo = @EmployeeNo AND IdNo = @IdNo";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.Int).Value = a;
            cmd.Parameters.Add("@SchoolName", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@YearsAttended", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@Degree", SqlDbType.VarChar).Value = e;
            cmd.Parameters.Add("@IdNo", SqlDbType.VarChar).Value = f;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_Record(String a, String b, String c, String d, double e, String f, String g, String h)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE EMPLOYMENTRECORD SET Employer=@Employer, DatesEmployed=@DatesEmployed, Position=@Position, Salary=@Salary, ImmediateSupervisor=@ImmediateSupervisor,ReasonForLeaving=@ReasonForLeaving WHERE EmployeeNo = @EmployeeNo AND IdNo = @IdNo ";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.Int).Value = a;
            cmd.Parameters.Add("@Employer", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@DatesEmployed", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@Salary", SqlDbType.Float).Value = e;
            cmd.Parameters.Add("@ImmediateSupervisor", SqlDbType.VarChar).Value = f;
            cmd.Parameters.Add("@ReasonForLeaving", SqlDbType.VarChar).Value = g;
            cmd.Parameters.Add("@IdNo", SqlDbType.VarChar).Value = h;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_Record(String a, String b, String c, String d, String e, String f, String g, String h)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE FAMILYBACKGROUND SET Relationship=@Relationship,Name=@Name, Age=@Age, Occupation=@Occupation, Employer=@Employer, ContactNo=@ContactNo WHERE EmployeeNo = @EmployeeNo AND IdNo = @IdNo";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.Int).Value = a;
            cmd.Parameters.Add("@Relationship", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@Age", SqlDbType.VarChar).Value = d;
            cmd.Parameters.Add("@Occupation", SqlDbType.VarChar).Value = e;
            cmd.Parameters.Add("@Employer", SqlDbType.VarChar).Value = f;
            cmd.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = g;
            cmd.Parameters.Add("@IdNo", SqlDbType.VarChar).Value = h;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_Record(String a, int b, int c)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "UPDATE QSearchClient SET NoOfMale=@NoOfMale,NoOfFemale=@NoOffemale WHERE ClientName = @ClientName";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@ClientName", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@NoOfMale", SqlDbType.Int).Value = b;
            cmd.Parameters.Add("@NoOfFemale", SqlDbType.Int).Value = c;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Add_Record(int a, String b, String c, int d, int e)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT QSearchClient VALUES(@ClientNo, @ClientName, @ClientAddress,@NoOfMale,@NoOfFemale)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@ClientNo", SqlDbType.Int).Value = a;
            cmd.Parameters.Add("@ClientName", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@ClientAddress", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@NoOfMale", SqlDbType.Int).Value = d;
            cmd.Parameters.Add("@NoOfFemale", SqlDbType.Int).Value = e;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Delete_Record(int a)
        {
            SqlConnection con = new SqlConnection(connectionString);
            var cmd = new SqlCommand();

            String add = "DELETE EDUCATION WHERE EmployeeNo = @EmployeeNo";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.Int).Value = a;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

        }
        public static void Security(String table, String set, String column, String condition)
        {
            var c = new SqlConnection(connectionString);
            var cmd = new SqlCommand("UPDATE " + table + " SET " + set + " WHERE " + column + "= " + "'" + condition + "'", c);
            c.Open();
            cmd.ExecuteNonQuery();
            c.Close();
        }
        public static void Add_Account(String a, String b, String c, String d)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd;
            String add = "INSERT UserAccounts VALUES(@username, @password, @failedAttempts,@securityQuestion,@securityAnswer)";
            cmd = new SqlCommand(add, con);
            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@failedAttempts", SqlDbType.Float).Value = 0;
            cmd.Parameters.Add("@securityQuestion", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@securityAnswer", SqlDbType.VarChar).Value = d;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void ChangePassword(int a, String b, String c)
        {
            var con = new SqlConnection(connectionString);
            var select = "Update UserAccounts SET failedattempts=@failedattempts,password=@password WHERE username=@username";
            var cmd = new SqlCommand(select, con);
            cmd.Parameters.Add("@failedattempts", SqlDbType.Int).Value = a;
            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = c;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void Update_Useraccounts(String a,String b,String c)
        {
            var con = new SqlConnection(connectionString);
            var select = "Update UserAccounts SET securityQuestion=@securityQuestion,securityAnswer=@securityAnswer, password=@password WHERE username=@username";
            var cmd = new SqlCommand(select, con);
            cmd.Parameters.Add("@securityQuestion", SqlDbType.VarChar).Value = a;
            cmd.Parameters.Add("@securityAnswer", SqlDbType.VarChar).Value = b;
            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = c;
            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = "verdant_admin";
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}