﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Services : Form
    {
        //HERE CONTAINS THE CODES WHEN ADDING,EDITING NEW SERVICES IN THE SYSTEM
        public Services()
        {
            InitializeComponent();
          
         
        }

        private void btnSave_Item_Click(object sender, EventArgs e)
        {
            if (Global.typeofservice == 1)
            {
                if (txtName_Item.Text != "" && txtPrice_Item.Text != "" && cmbTransactionType.SelectedIndex!=-1)
                {
                    if (Global.save_item == 0)
                    {
                        var ds = Global.CheckPrimaryKey("*", "IncomeServices", "Name", txtName_Item.Text);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            String newCode = "";
                            DataTable dt = Global.Query2("TOP 1 *", "IncomeServices ORDER BY Code DESC"); //SELECT TOP 1 * FROM Table ORDER BY ID DESC
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dr = dt.Rows[i];
                                Global.code_item = int.Parse(dr["Code"].ToString());
                                Global.code_item++;

                            }
                            if (Global.code_item < 100)
                                newCode = "0" + Global.code_item;
                            else
                                newCode = Global.code_item.ToString();

                            Global.Add_Record(newCode, txtName_Item.Text, double.Parse(txtPrice_Item.Text), cmbTransactionType.Text);
                            MessageBox.Show("Service added!");

                            txtPrice_Item.Clear();
                            txtName_Item.Clear();
                            cmbTransactionType.SelectedIndex = -1;
                            this.Close();

                        }
                        else
                        {
                            MessageBox.Show("Service already exists");
                        }
                    }
                    else
                    {
                        var ds = Global.CheckPrimaryKey("*", "IncomeServices", "Name", txtName_Item.Text);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            Global.Edit_Record(Global.code, txtName_Item.Text, double.Parse(txtPrice_Item.Text), cmbTransactionType.Text);
                            MessageBox.Show("Service successfuly edited");

                            txtPrice_Item.Clear();
                            txtName_Item.Clear();
                            cmbTransactionType.SelectedIndex = -1;
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Service already exists");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please check fields");
                    if (txtName_Item.Text == "")
                        txtName_Item.Focus();
                    if (txtPrice_Item.Text == "")
                        txtPrice_Item.Focus();
                }
            }
            else
            {
                if (txtName_Item.Text != "")
                {
                        if (Global.save_item == 0)
                        {

                            var ds = Global.CheckPrimaryKey("*", "ExpenseServices", "Name", txtName_Item.Text);
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                    String newCode = "";
                                    DataTable dt = Global.Query2("TOP 1 *", "ExpenseServices ORDER BY Code DESC"); //SELECT TOP 1 * FROM Table ORDER BY ID DESC
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        DataRow dr = dt.Rows[i];
                                        Global.code_item = int.Parse(dr["Code"].ToString());
                                        Global.code_item++;

                                    }
                                    if (Global.code_item < 100)
                                        newCode = "0" + Global.code_item;
                                    else
                                        newCode = Global.code_item.ToString();

                                    Global.Add_Record(newCode, txtName_Item.Text);
                                    MessageBox.Show("Service added!");

                                    txtPrice_Item.Clear();
                                    txtName_Item.Clear();
                                    cmbTransactionType.SelectedIndex = -1;
                                    this.Close();
                                }
                            else
                            {
                            MessageBox.Show("Service already exists");
                            }

                        }
                
                    else
                    {
                        Global.Edit_Record(Global.code, txtName_Item.Text);
                        MessageBox.Show("Service successfuly edited");

                        txtPrice_Item.Clear();
                        txtName_Item.Clear();
                        cmbTransactionType.SelectedIndex = -1;
                        this.Close();
                    }
                }
                
                else
                {
                    MessageBox.Show("Please enter a service name");
                    txtName_Item.Focus();
                }


            }
        }

        private void IncomeServices_Load(object sender, EventArgs e)
        {
            cmbTransactionType.Enabled = true;
            txtPrice_Item.Enabled = true;
            if (Global.typeofservice == 1)
            {
               
                if (Global.save_item == 1)
                {
                    SqlConnection con = new SqlConnection(Global.connectionString);
                    SqlCommand command = new SqlCommand("select * from IncomeServices WHERE Code='" + Global.code + "'", con);
                    con.Open();
                    SqlDataReader read = command.ExecuteReader();
                    while (read.Read())
                    {
                        txtName_Item.Text = (read["Name"].ToString());
                        txtPrice_Item.Text = (read["Price"].ToString());
                        if ((read["TransactionType"].ToString() == "Reservation"))
                            cmbTransactionType.SelectedIndex = 0;
                        else if ((read["TransactionType"].ToString() == "Monthly Payment"))
                            cmbTransactionType.SelectedIndex = 1;
                        else if ((read["TransactionType"].ToString() == "Rentals"))
                            cmbTransactionType.SelectedIndex = 2;
                        else if ((read["TransactionType"].ToString() == "Sticker Fee"))
                            cmbTransactionType.SelectedIndex = 3;
                        else if ((read["TransactionType"].ToString() == "Monthly Due"))
                            cmbTransactionType.SelectedIndex = 4;
                        else if ((read["TransactionType"].ToString() == "Swimming Pool"))
                            cmbTransactionType.SelectedIndex = 5;
                        else if ((read["TransactionType"].ToString() == "Others"))
                        {
                            cmbTransactionType.SelectedIndex = 6;
                            txtPrice_Item.Enabled = false;
                        }
                        else if ((read["TransactionType"].ToString() == "Discount/Penalty"))
                        {
                            cmbTransactionType.Enabled = false;
                        }
                    }
                    read.Close();
                    con.Close();
                }
            }
            else
            {
                txtPrice_Item.Enabled = false;
                cmbTransactionType.Enabled = false;
                if (Global.save_item == 1)
                {
                    SqlConnection con = new SqlConnection(Global.connectionString);
                    SqlCommand command = new SqlCommand("select * from ExpenseServices WHERE Code='" + Global.code + "'", con);
                    con.Open();
                    SqlDataReader read = command.ExecuteReader();
                    while (read.Read())
                    {
                        txtName_Item.Text = (read["Name"].ToString());
                    }
                    read.Close();
                    con.Close();

                    txtPrice_Item.Enabled = false;
                    cmbTransactionType.Enabled = false;
                }
            }
        }

        private void btnCancel_Item_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPrice_Item_TextChanged(object sender, EventArgs e)
        {
            double temp1;
            if (!double.TryParse(txtPrice_Item.Text, out temp1))
            {
                if (txtPrice_Item.Text != "")
                {
                    MessageBox.Show("Invalid input. Only numbers are allowed");
                    txtPrice_Item.Clear();
                }
            }
        }
    }
}
