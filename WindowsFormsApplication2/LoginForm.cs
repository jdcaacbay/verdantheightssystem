﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class LoginForm : Form
    {
        // CONTAINS THE CODES WHEN LOGGING-IN IN THE SYSTEM
        public LoginForm()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        int failed_counter;

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text != "" && txtPassword.Text != "")
            {
                var ds = Global.Login(txtUserName.Text,txtPassword.Text);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter();
                failed_counter = 0;

                if (ds.Tables[0].Rows.Count != 0)
                {
                    var security = Global.Query("failedAttempts", "UserAccounts", "username='" + txtUserName.Text + "'");
                    dt = security.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        failed_counter = int.Parse(dr[0].ToString());
                    }

                    if (failed_counter < 3)
                    {

                        Global.username = txtUserName.Text;
                        Global.ChangePassword(0, txtPassword.Text, txtUserName.Text);
                        Global.Clear(this, 1);
                        this.Visible = false;
                        Form1 frm = new WindowsFormsApplication2.Form1();
                        frm.Show();



                    }
                    else
                    {
                        MessageBox.Show("Failed attempts exceeded! Please reset password");
                        lblResetPassword.Visible = true;
                    }
                }
                else
                {
                    var security = Global.Query("failedAttempts", "UserAccounts", "username='" + txtUserName.Text + "'");
                    dt = security.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        failed_counter = int.Parse(dr[0].ToString());
                    }

                    if (failed_counter != 3)
                    {
                        MessageBox.Show("Invalid Login");
                        failed_counter++;
                        Global.Security("UserAccounts", "failedAttempts=" + failed_counter, "username", txtUserName.Text);
                        failed_counter = 0;
                        txtPassword.Clear();
                        txtPassword.Focus();

                    }
                    else
                    {
                        MessageBox.Show("Failed attempts exceeded! Please reset password");
                        lblResetPassword.Visible = true;
                    }
                }
            }
            else
            {
                if(txtUserName.Text=="")
                {
                    MessageBox.Show("Please input username");
                    txtUserName.Focus();
                }
                if(txtPassword.Text=="")
                {
                    MessageBox.Show("Please input password");
                    txtPassword.Focus();
                }
            }
               
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblForgetPassword_Click(object sender, EventArgs e)
        {
            Global.username = txtUserName.Text;
            ResetPassword fp = new ResetPassword();
            fp.ShowDialog();
            txtPassword.Clear();
            txtPassword.Focus();
            lblResetPassword.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtPassword_KeyPress_1(object sender, KeyPressEventArgs e)
        {
         
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
