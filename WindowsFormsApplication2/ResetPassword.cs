﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class ResetPassword : Form
    {
        //HERE CONTAINS THE CODES WHEN RESETTING THE PASSWORD AFTER FAILING 3 TIMES WHEN LOGGING IN
        public ResetPassword()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ResetPassword_Load(object sender, EventArgs e)
        {
            DataTable dt = Global.AccountSettings("*", "UserAccounts", Global.username);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                txtSecurityQuestion.Text = dr["securityQuestion"].ToString();
            }
            
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (txtNewPassword.Text != "" || txtConfirmPassword.Text != "")
            {
                if (txtNewPassword.Text == txtConfirmPassword.Text)
                {
                    if (txtNewPassword.TextLength >= 8)
                    {
                        Global.ChangePassword(0, txtNewPassword.Text, Global.username);
                        MessageBox.Show("Success! Your Password has been changed!");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Password must be atleast 8 characters");
                        txtNewPassword.Focus();
                    }
                }
                else
                    MessageBox.Show("Passwords do not match");
            }
            else
                MessageBox.Show("Incomplete input");
        }
        private void CheckSecurity()
        {
            var ds = Global.Query("*", "UserAccounts", "securityAnswer='" + txtAnswer.Text + "'");
            DataTable dt = new DataTable();
            if (ds.Tables[0].Rows.Count != 0)
            {
                pnlChangePassword.Enabled = true;
                pnlSecurity.Enabled = false;
                MessageBox.Show("Password was reset. Please enter new password");
                txtNewPassword.Focus();
            }
            else
                MessageBox.Show("Incorrect answer");
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            CheckSecurity();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNewPassword_TextChanged(object sender, EventArgs e)
        {
            if(txtNewPassword.TextLength<8)
            {
                txtNewPassword.BackColor = Color.FromArgb(247, 122, 111);
            }
            else
            {
                txtNewPassword.BackColor = SystemColors.Control;
            }
        }

        private void txtNewPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void txtConfirmPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
      
        }

        private void txtConfirmPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtNewPassword.Text != txtConfirmPassword.Text) 
            {
                txtNewPassword.BackColor = Color.FromArgb(247, 122, 111);
            }
            else
            {
                txtNewPassword.BackColor = SystemColors.Control;
            }
        }
    }
}
